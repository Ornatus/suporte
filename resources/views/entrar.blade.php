@extends('layouts.entrar')

@section('content')
<script
    src="https://code.jquery.com/jquery-2.2.4.min.js"
    integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
    crossorigin="anonymous"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading">Acessar formulário</div>

            <div class="panel-body">
              <form>
                <div class="form-group">
                  <label for="loja">Loja</label>
                  <select class="form-control" name="loja" id="loja">
                    <option value=""></option>
                    <?php foreach($listaLojas as $loja){ ?>

                      <option value="<?php echo $loja->codigo; ?>"><?php echo $loja->nome; ?></option>

                    <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="email">E-mail</label>
                  <input type="email" class="form-control" id="email" aria-describedby="" placeholder="">
                </div>
                <a type="button" id="btn-entrar" class="btn btn-primary">Entrar</a>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<script>

$(document).ready(function() {

$('#btn-entrar').click(function(){

  $btn = $(this);
  $loja = $("#loja option:selected").val();
  $email = $("#email").val();


  if($loja.length < 1 || $email.length < 1)
  {
    swal('','Selecione a loja e informe seu email.','info');

    return false;
  }

  $btn.attr('disabled',true);

  $url = "{{ URL::to('/formulario/login/') }}";

  $.ajax({
    url: $url,
    type: 'POST',
    data: {"_token":"{{ csrf_token() }}",
            loja:$loja,
            email:$email
          },
    datatype : "application/json",
    success: function(dataReturn, textStatus, xhr)
    {

      if(xhr.status == 203)
      {
        swal({
          title: "",
          text: "Cadastro não encontrado.",
          type: "info",
          showCancelButton: false,
          confirmButtonColor: "#0073e6",
          confirmButtonText: "Sim",
          closeOnConfirm: false,
          closeOnClickOutside: false,
          closeOnEsc: false

        })

          $btn.attr('disabled',false);
          return false;
      }
      else
      {
        window.location.href = "{{ URL::to('/formulario/loja/') }}"+'/'+$loja;
      }
    },
    error: function(xhr, status, error) {
      swal('Erro.'+error.text);
      $btn.attr('disabled',false);
      return false;
    }
  }).done(function() {
    $btn.attr('disabled',false);
  });

})


});



  </script>


@endsection
