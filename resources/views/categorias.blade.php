@extends('layouts.menu')

@section('content')

<div class="row" style="margin:10px !important">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">

        <form id="filtro" name="filtro" method="GET">
          {!! csrf_field() !!}
          <table width="auto">
            <td style="padding:5px"> Filtrar categoria:<br>
              <input type="text" id="filtro_categoria" name="filtro_categoria" value="<?php echo $filtro_categoria; ?>" style="margin-bottom:10px"  class="form-control"/>
            </td>
            <td style="padding:5px"><br>
              <a type="button" id="btn-atualizar" class="btn btn-primary" style="margin-bottom:10px">Filtrar</a>
            </td>
            <td style="padding:5px"><br>
              <a type="button" id="btn-adicionar" class="btn btn-primary" style="margin-bottom:10px">Adicionar</a>
            </td>
          </tr>
        </table>
      </form>
    </div>
    <div  width="100%"  style="padding-left:43%;">
      <div style="">
        {{ $categorias->render()}}
      </div>
    </div>
    <div class="panel-body">
      <table class="table table-striped table-responsive" style="font-size: 10pt; font-family: Verdana;">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Categoria</th>
            <th scope="col">Departamento</th>
            <th scope="col" style="width:10%">Cor</th>
            <th scope="col" style="width:10%">Editar</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($categorias as $categoria){ ?>
            <tr>
              <td scope="row"><b><?php echo $categoria->categoria_id; ?></b> </td>
              <td><?php echo $categoria->descricao; ?></td>
              <td><?php echo $categoria->departamento; ?></td>
              <td style="padding: 5px;">
                <div class="rounded" style="width:50% !important;height: 100% !important;background-color:<?php echo $categoria->cor; ?>;color:<?php echo $categoria->cor; ?>;">
                  <img style="<?php echo $categoria->cor; ?>" class="rounded-circle">
                  <h6><?php echo $categoria->cor; ?></h6>
                </div>
              </td>
              <td>
                <a type="button" title="Editar categoria" href="{{ URL::to('/categoria/'.$categoria->categoria_id) }}" class="btn btn-default">
                  <i class="fa fa-bars"></i></a>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
    <div  width="100%"  style="padding-left:43%;">
      <div style="">
        {{ $categorias->render()}}
      </div>
    </div>
  </div>
</div>


<script>
$(document).ready(function() {

  $('#btn-atualizar').click(function(){
    swal({
      title: 'Aguarde',
      html: 'Aguarde.',
      showCancelButton: false,
      showConfirmButton: false
    });

    $("#filtro").submit();

  });

  $("#btn-adicionar").click(function(){

    window.location = "{{ URL::to('/adiciona-categoria/') }}";
  })

});

</script>

<style>

.btn-group
{
  margin-bottom:0px !important;
}

</style>

@endsection
