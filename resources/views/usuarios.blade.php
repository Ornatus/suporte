@extends('layouts.menu')

@section('content')

<div class="row" style="margin:10px !important">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">

        <form id="filtro" name="filtro" method="GET">
          {!! csrf_field() !!}
          <table width="auto">
            <td style="padding:5px"> Filtrar usuário:<br>
              <input type="text" id="filtro_usuario" name="filtro_usuario" value="<?php echo $filtro_usuario; ?>" style="margin-bottom:10px"  class="form-control"/>
            </td>
            <td style="padding:5px"><br>
              <a type="button" id="btn-atualizar" class="btn btn-primary" style="margin-bottom:10px">Filtrar</a>
            </td>
            <td style="padding:5px"><br>
              <a type="button" id="btn-adicionar" class="btn btn-primary" style="margin-bottom:10px">Adicionar</a>
            </td>
          </tr>
        </table>
      </form>
    </div>
    <div  width="100%"  style="padding-left:43%;">
      <div style="">
        {{ $usuarios->render()}}
      </div>
    </div>
    <div class="panel-body">
      <table class="table table-striped table-responsive" style="font-size: 10pt; font-family: Verdana;">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Usuario</th>
            <th scope="col">Nome</th>
            <th scope="col">Sobrenome</th>
            <th scope="col">email</th>
            <th scope="col">Grupo</th>
            <th scope="col" style="width:10%">Editar</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($usuarios as $user){ ?>
            <tr>
              <td scope="row"><b><?php echo $user->user_id; ?></b> </td>
              <td><?php echo $user->username; ?></td>
              <td><?php echo $user->firstname; ?></td>
              <td><?php echo $user->lastname; ?></td>
              <td><?php echo $user->email; ?></td>
              <td><?php echo $user->descricao; ?></td>
                <td>
                <a type="button" title="Editar usuario" href="{{ URL::to('/usuario/'.$user->user_id) }}" class="btn btn-default">
                  <i class="fa fa-bars"></i></a>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
    <div  width="100%"  style="padding-left:43%;">
      <div style="">
        {{ $usuarios->render()}}
      </div>
    </div>
  </div>
</div>


<script>
$(document).ready(function() {

  $('#btn-atualizar').click(function(){
    swal({
      title: 'Aguarde',
      html: 'Aguarde.',
      showCancelButton: false,
      showConfirmButton: false
    });

    $("#filtro").submit();

  });

  $("#btn-adicionar").click(function(){

    window.location = "{{ URL::to('/adiciona-usuario/') }}";
  })

});

</script>

<style>

.btn-group
{
  margin-bottom:0px !important;
}

</style>

@endsection
