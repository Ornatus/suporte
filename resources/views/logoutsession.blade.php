@extends('layouts.app')
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style>
.navbar_bottom {
  overflow: hidden;
  background-color:  #FFFFFF;
  position: fixed;
  bottom: 0;
  border:1px solid #d3e0e9;
  width: 100%;
  padding:10px !important;
}

.navbar_bottom a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.navbar_bottom a:hover {
  background: #ddd;
  color: black;
}
</style>
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <form id="shopping_list" name="shopping_list" method="POST">
          <div class="panel-body">
            <button type="button" class="btn btn-warning btn-lg btn-block user-login" style="text-align:left !important">

              <h5>
                <i class="fa fa-user"></i> Sua sessão expirou.</h5>
                <h5>Clique aqui para entrar novamente na plataforma.</h5>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script>

  $(document).ready(function() {


    $('.user-login').click(function()
    {
      window.location = "{{ URL::to('logar') }}";

      return true;

    });

  });

</script>

@endsection
