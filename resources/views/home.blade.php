@extends('layouts.menu')

@section('content')

<div class="row" style="margin:10px !important">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <form id="filtro" name="filtro" method="GET">
          {!! csrf_field() !!}
          <table width="auto">
            <tr>
              <td style="padding:5px">
                Quais chamados?<br>
                <select name="filtro_chamado" id="filtro_chamado" class="form-control" style="width:auto; margin-bottom:10px">
                  <option <?php echo ($filtro_chamado == 'grupo') ? 'selected' : ''; ?> value="grupo">Chamados do meu grupo</option>
                  <option <?php echo ($filtro_chamado == 'usuario') ? 'selected' : ''; ?> value="usuario">Meus Chamados</option>
                </select>
              </td>
              <td style="padding:5px">
                Qual Status?<br>
                <select name="filtro_status" id="filtro_status" class="form-control" style="width:auto; margin-bottom:10px">
                  <option value="">Todos</option>
                  <option <?php echo ($filtro_status == 'AA') ? 'selected' : ''; ?> value="AA">Aguardando + Atendimento</option>
                  <?php foreach($status as $sta){ ?>
                    <option <?php echo ($filtro_status == $sta->status_id) ? 'selected' : ''; ?> value="<?php echo $sta->status_id; ?>"><?php echo $sta->descricao; ?></option>
                  <?php } ?>
                </select>
              </td>
              <td style="padding:5px">
                Busca por:<br>
                <input type="text" id="filtro_livre" name="filtro_livre" value="<?php echo $filtro_livre; ?>" style="margin-bottom:10px; width:400px;"  class="form-control"/>
              </td>
              <td style="padding:5px">
                Ordenação<br>
                <select name="filtro_coluna" id="filtro_coluna" class="form-control" style="width:auto; margin-bottom:10px">
                  <option <?php echo ($filtro_coluna == 'data_chamado') ? 'selected' : ''; ?> value="data_chamado">Data</option>
                  <option <?php echo ($filtro_coluna == 'brazil_store_name') ? 'selected' : ''; ?> value="brazil_store_name">Loja</option>
                  <option <?php echo ($filtro_coluna == 'categoria') ? 'selected' : ''; ?> value="categoria">Categoria</option>
                  <option <?php echo ($filtro_coluna == 'atendente_nome') ? 'selected' : ''; ?> value="atendente_nome">Usuário</option>
                  <option <?php echo ($filtro_coluna == 'tempo_aberto') ? 'selected' : ''; ?> value="tempo_aberto">Tempo aberto</option>
                  <option <?php echo ($filtro_coluna == 'status') ? 'selected' : ''; ?> value="status">Status</option>
                </select>
              </td>
              <td style="padding:5px">
                ASC ou DESC?<br>
                <select name="filtro_ordem" id="filtro_ordem" class="form-control" style="width:auto; margin-bottom:10px">
                  <option <?php echo ($filtro_ordem == 'asc') ? 'selected' : ''; ?> value="asc">ASC</option>
                  <option <?php echo ($filtro_ordem == 'desc') ? 'selected' : ''; ?> value="desc">DESC</option>
                </select>
              </td>
              <td style="padding:5px"><br>
                <a type="button" id="btn-atualizar" class="btn btn-primary" style="margin-bottom:10px">Atualizar</a>
              </td>
            </tr>
          </table>
        </form>

      </div>

      <div>
        <div width="100%"  style="padding-left:43%;">
          <div style="">
            {{ $chamados->render()}}
          </div>
        </div>
        <table class="table table-striped table-responsive" style="font-size: 9pt; font-family: Verdana;">
          <thead>
            <tr>
              <th scope="col">#ID</th>
              <th scope="col">Data Chamado</th>
              <th scope="col">Loja</th>
              <th scope="col">Titulo</th>
              <th scope="col">Categoria</th>
              <th scope="col">Descrição</th>
              <th scope="col">Solicitante</th>
              <th scope="col">Atendido Por <br> Tempo Atendimento</th>
              <th scope="col">Prioridade</th>
              <th scope="col">Status</th>
              <th scope="col">Visualizar</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($chamados as $chamado){ ?>

              <tr <?php echo($chamado->retorno == 1) ? "style='background-color:#ffe6e6'" : '' ; ?>>

                <td scope="row">
                  <b><?php echo $chamado->chamado_id; ?></b>
                  
                  <?php echo $chamado->data_chamado; ?>
                  <?php if ($chamado->is_semaphore == 1){?>
                    <?php if ($chamado->status_id == 1 ){ ?>
                      <?php if ( $chamado->dif_time > '01:00:00' ){ ?>
                        <div id="box-inside" style="">
                          <div id="circulo3" class="color3"></div>
                        </div>
                      <?php }else{ ?>
                        <div id="box-inside">
                          <div id="circulo1" class="color1"></div>
                        </div>
                      <?php } ?>
                    <?php } ?>

                    <?php if ($chamado->status_id == 2 ){ ?>
                      <?php if ( $chamado->tempo_aberto > '48:00:00' ){ ?>
                        <div id="box-inside" style="">
                          <div id="circulo3" class="color3"></div>
                        </div>
                      <?php }else{ ?>
                        <div id="box-inside" style="">
                          <div id="circulo1" class="color1"></div>
                        </div>
                      <?php } ?>
                    <?php } ?>
                  <?php } ?>
                </td>

                <td>
                  <b><?php echo $chamado->brazil_store_name; ?></b>
                </td>
                <td>
                  <span class="label label-default"  style="padding: 5px; color:white; background-color:<?php echo $chamado->cortitulo; ?>; border-radius: 5px;">
                    <b><?php echo wordwrap($chamado->titulo,20,"<br><br>"); ?></b>
                  </span>
                </td>
                <td><b><span class="label label-default"  style="padding: 5px; color:white; background-color:<?php echo $chamado->cor_categoria; ?>"><?php echo $chamado->categoria; ?></badge></b></td>
                  <td><span title="<?php echo $chamado->descricao; ?>"><?php echo substr($chamado->descricao,0,20); ?></span></td>
                  <td>
                    <?php echo wordwrap($chamado->solicitante,10,"<br>"); ?>
                    <?php echo wordwrap($chamado->telefone,11,"<br>"); ?>
                  </td>
                  <td><?php echo $chamado->atendente_nome; ?><br>
                    <b><?php echo $chamado->tempo_aberto; ?></b>
                  </td>

                  <td><span class="label label-default"  style="padding: 5px; color:white; background-color:<?php echo $chamado->corprioridade; ?>; border-radius: 5px;"><b><?php echo $chamado->prioridade; ?></b></span></td>
                  <td><span class="label label-default"  style="padding: 5px; color:white; background-color:<?php echo $chamado->status_cor; ?>"><b><?php echo $chamado->status; ?></b></span></td>
                  <td>
                    <a type="button" title="Visualizar chamado" href="{{ URL::to('/chamado/'.$chamado->chamado_id) }}" class="btn btn-default">
                      <i class="fa fa-bars"></i></a>
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
        <div  width="100%"  style="padding-left:43%;">
          <div style="">
            {{ $chamados->render()}}
          </div>
        </div>
      </div>
    </div>


    <script>
    $(document).ready(function() {

      $('#btn-atualizar').click(function(){
        swal({
          title: 'Aguarde',
          html: 'Aguarde.',
          showCancelButton: false,
          showConfirmButton: false
        });

        $("#filtro").submit();

      });

    });

    </script>

    <style>

    .btn-group
    {
      margin-bottom:0px !important;
    }

    </style>

    @endsection


    <style>

    #box-inside{
      background-color:black;
      border-radius: 10px;
      width:25px; height:25px;
      border: solid 1px;
      position:relative;
      padding: 4px 4px 4px 4px;
      transition:width 2s, height 2s;
      -moz-transition:width 2s, height 2s, -moz-transform 2s;
      -webkit-transition:width 2s, height 2s, -webkit-transform 2s;
      //margin: 20px auto auto 20px;
      /*-webkit-transform: rotate(90deg);
      -moz-transform: rotate(90deg);
      -o-transform: rotate(90deg);
      -ms-transform: rotate(90deg);
      transform: rotate(90deg); */
    }

    .color1{
      background: rgb(118,217,65);
      background: -moz-radial-gradient(center, ellipse cover, rgba(118,217,65,1) 0%, rgba(61,152,16,1) 50%, rgba(118,217,65,1) 100%);
      background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,rgba(118,217,65,1)), color-stop(50%,rgba(61,152,16,1)), color-stop(100%,rgba(118,217,65,1)));
      background: -webkit-radial-gradient(center, ellipse cover, rgba(118,217,65,1) 0%,rgba(61,152,16,1) 50%,rgba(118,217,65,1) 100%);
      background: -o-radial-gradient(center, ellipse cover, rgba(118,217,65,1) 0%,rgba(61,152,16,1) 50%,rgba(118,217,65,1) 100%);
      background: -ms-radial-gradient(center, ellipse cover, rgba(118,217,65,1) 0%,rgba(61,152,16,1) 50%,rgba(118,217,65,1) 100%);
      background: radial-gradient(ellipse at center, rgba(118,217,65,1) 0%,rgba(61,152,16,1) 50%,rgba(118,217,65,1) 100%);
      filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#76d941', endColorstr='#76d941',GradientType=1 );
    }

    #circulo1{

      border-radius: 200px;
      width:15px; height: 15px;
      position:relative;
      top: 0; left: 0;

    }
    #circulo1:hover{

      background: rgb(180,227,145);
      background: -moz-radial-gradient(center, ellipse cover, rgba(180,227,145,1) 0%, rgba(97,196,25,1) 50%, rgba(180,227,145,1) 100%); /* FF3.6+ */
      background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,rgba(180,227,145,1)), color-stop(50%,rgba(97,196,25,1)), color-stop(100%,rgba(180,227,145,1))); /* Chrome,Safari4+ */
      background: -webkit-radial-gradient(center, ellipse cover, rgba(180,227,145,1) 0%,rgba(97,196,25,1) 50%,rgba(180,227,145,1) 100%); /* Chrome10+,Safari5.1+ */
      background: -o-radial-gradient(center, ellipse cover, rgba(180,227,145,1) 0%,rgba(97,196,25,1) 50%,rgba(180,227,145,1) 100%); /* Opera 12+ */
      background: -ms-radial-gradient(center, ellipse cover, rgba(180,227,145,1) 0%,rgba(97,196,25,1) 50%,rgba(180,227,145,1) 100%); /* IE10+ */
      background: radial-gradient(ellipse at center, rgba(180,227,145,1) 0%,rgba(97,196,25,1) 50%,rgba(180,227,145,1) 100%); /* W3C */
      filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b4e391', endColorstr='#b4e391',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
      border-radius: 100px;
      width:15px; height: 15px;
      position:relative;
      top: 0; left: 0;


    }

    .color3{
      background: rgb(216,82,82);
      background: -moz-radial-gradient(center, ellipse cover, rgba(216,82,82,1) 0%, rgba(217,117,118,1) 32%, rgba(209,49,55,1) 49%, rgba(205,59,64,1) 71%, rgba(200,70,78,1) 100%);
      background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,rgba(216,82,82,1)), color-stop(32%,rgba(217,117,118,1)), color-stop(49%,rgba(209,49,55,1)), color-stop(71%,rgba(205,59,64,1)), color-stop(100%,rgba(200,70,78,1)));
      background: -webkit-radial-gradient(center, ellipse cover, rgba(216,82,82,1) 0%,rgba(217,117,118,1) 32%,rgba(209,49,55,1) 49%,rgba(205,59,64,1) 71%,rgba(200,70,78,1) 100%);
      background: -o-radial-gradient(center, ellipse cover, rgba(216,82,82,1) 0%,rgba(217,117,118,1) 32%,rgba(209,49,55,1) 49%,rgba(205,59,64,1) 71%,rgba(200,70,78,1) 100%);
      background: -ms-radial-gradient(center, ellipse cover, rgba(216,82,82,1) 0%,rgba(217,117,118,1) 32%,rgba(209,49,55,1) 49%,rgba(205,59,64,1) 71%,rgba(200,70,78,1) 100%);
      background: radial-gradient(ellipse at center, rgba(216,82,82,1) 0%,rgba(217,117,118,1) 32%,rgba(209,49,55,1) 49%,rgba(205,59,64,1) 71%,rgba(200,70,78,1) 100%);
      filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d85252', endColorstr='#c8464e',GradientType=1 );
      -webkit-animation: color3 .5s linear infinite;
      -moz-animation: color3 .5s linear infinite;
      -ms-animation: color3 .5s linear infinite;
      -o-animation: color3 .5s linear infinite;
      animation: color3 .5s linear infinite;
    }

    @keyframes color3 {
      0% { opacity: 1; }
      50% { opacity: 0.5; }
      100% { opacity: 0; }
    }

    #circulo3{
      border-radius: 200px;
      width:15px; height: 15px;
      position:relative;
      top: 0; left: 0;

    }
    #circulo3:hover{

      background: rgb(243,197,189);
      background: -moz-radial-gradient(center, ellipse cover, rgba(243,197,189,1) 0%, rgba(232,108,87,1) 50%, rgba(234,40,3,1) 51%, rgba(255,102,0,1) 75%, rgba(199,34,0,1) 100%); /* FF3.6+ */
      background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,rgba(243,197,189,1)), color-stop(50%,rgba(232,108,87,1)), color-stop(51%,rgba(234,40,3,1)), color-stop(75%,rgba(255,102,0,1)), color-stop(100%,rgba(199,34,0,1))); /* Chrome,Safari4+ */
      background: -webkit-radial-gradient(center, ellipse cover, rgba(243,197,189,1) 0%,rgba(232,108,87,1) 50%,rgba(234,40,3,1) 51%,rgba(255,102,0,1) 75%,rgba(199,34,0,1) 100%); /* Chrome10+,Safari5.1+ */
      background: -o-radial-gradient(center, ellipse cover, rgba(243,197,189,1) 0%,rgba(232,108,87,1) 50%,rgba(234,40,3,1) 51%,rgba(255,102,0,1) 75%,rgba(199,34,0,1) 100%); /* Opera 12+ */
      background: -ms-radial-gradient(center, ellipse cover, rgba(243,197,189,1) 0%,rgba(232,108,87,1) 50%,rgba(234,40,3,1) 51%,rgba(255,102,0,1) 75%,rgba(199,34,0,1) 100%); /* IE10+ */
      background: radial-gradient(ellipse at center, rgba(243,197,189,1) 0%,rgba(232,108,87,1) 50%,rgba(234,40,3,1) 51%,rgba(255,102,0,1) 75%,rgba(199,34,0,1) 100%); /* W3C */
      filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f3c5bd', endColorstr='#c72200',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
      border-radius: 100px;
      width:15px; height: 15px;
      position:relative;
      top: 0; left: 0;

    }
    </style>
