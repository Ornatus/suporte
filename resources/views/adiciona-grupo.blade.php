@extends('layouts.menu')

@section('content')

<div class="row" style="margin:10px !important">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">

      </div>
      <div class="panel-body">
        <table class="table table-striped table-responsive" style="font-size: 10pt; font-family: Verdana;">
          <thead>
            <tr>
              <th scope="col">
                Descrição:<br> <input class="form-control" type="text" name ="descricao" id="descricao" value=""></input>
              </th>
            </tr>
            <tr>
              <th scope="col">
                Cor:<br> <input type="text" class="form-control" name ="cor"  id="cor" value=""></input>
              </th>
            </tr>

          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
<br><br>

<div class="navbar2">
  <table style="float:right">
    <tr>
      <td style="padding:5px">
        <a type="button" id="btn-voltar" class="btn btn-info btn-voltar">Voltar</a>
      </td>

      <td style="padding:5px">
        <a type="button" id="btn-cadastrar" class="btn btn-success">Cadastrar</a>
      </td>
    </tr>
  </table>
</div>

<!-- Modal da finalização do chamado -->
<!-- Modal -->
<div class="modal fade" id="modal-finaliza" tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="TituloModalCentralizado">Resolução do chamado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="md-form">
          <i class="fas fa-pencil-alt prefix"></i>
          <textarea id="resolucao" name="resolucao" class="md-textarea form-control" rows="3"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary btn-modalfinalizar">Finalizar</button>
      </div>
    </div>
  </div>
</div>


<script>
$(document).ready(function() {

  $(".btn-voltar").click(function(){

    window.location = "{{ URL::to('/grupos/') }}";
  })

  $("#btn-cadastrar").click(function(){

    $btn = $(this);
    $btn.attr('disabled',false);

    url = "{{ URL::to('/cadastra-grupo/') }}";

    var cor = $('#cor');
    var descricao = $('#descricao');

    if(cor.val() == '' || descricao.val() == ''){
      swal("Erro!", "Por favor preencha os campos (Cor e Descrição).", "error");
      return false;
    }

    $.ajax({
      url: url,
      type: 'POST',
      data: {
        descricao: descricao.val(),
        cor: cor.val(),
        "_token":"{{ csrf_token() }}"
      },
      error: function(jq,status,message) {
        swal("Erro!", "Erro ao adicionar o grupo.", "error");
        $btn.attr('disabled',false);
        return false;
      }
    })
    .done(function( msg ) {

      if(msg==1)
      {
        $('#mensagem').val('');

        swal({
          title: "",
          text: "Grupo Acicionado.",
          type: "success",
          showCancelButton: false,
          cancelButtonText: "Não",
          confirmButtonColor: "#1f90bb",
          confirmButtonText: "OK",
          closeOnConfirm: true
        } ,
        function(){
          swal({
            title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
            text: 'Aguarde...',
            html: true,
            showCancelButton: false,
            showConfirmButton: false,
            closeOnConfirm: false,
            closeOnCancel: false
          });

          window.location = "{{ URL::to('/grupos') }}";
        })
        .fail(function( jqXHR, textStatus ) {
          swal("Erro!", "Erro ao finalizar o grupo.", "error");
          $btn.attr('disabled',false);
        })
      }
    });

  })
/*
  $("#btn-atualizar").click(function(){
    $('#modal-atualizar').modal('show');
  })
*/
});


</script>


@endsection

<style>

.navbar2 {
  overflow: hidden;
  background-color: #333;
  position: fixed;
  bottom: 0;
  padding:10px;
  width: 100%;
}

.navbar2 a:hover {
  background: #ddd;
  color: black;
}

</style>
