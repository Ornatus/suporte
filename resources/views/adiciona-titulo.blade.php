@extends('layouts.menu')

@section('content')

<div class="row" style="margin:10px !important">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">

      </div>
      <div class="panel-body">
        <table class="table table-striped table-responsive" style="font-size: 10pt; font-family: Verdana;">
          <thead>
            <tr style="padding:5px">
              <b>Categoria:</b><br>
              <select name="categoria" id="categoria" class="form-control" style="width:auto; margin-bottom:10px">
                <option value="0">Selecionar</option>
                <?php foreach($categorias as $categoria){ ?>
                  <option value="<?php echo $categoria->categoria_id; ?>"><?php echo $categoria->descricao; ?></option>
                <?php } ?>
              </select>
            </tr>
            <tr>
              <th scope="col">
                Titulo:<br> <input class="form-control" type="text" name="descricao" id="descricao" value=""></input>
              </th>
            </tr>
            <tr>
              <th scope="col">
                Cor:<br> <input type="text" class="form-control" name="cor" id="cor" value=""></input>
              </th>
            </tr>
            <tr>
              <th scope="col">
                Link:<br> <input type="text" class="form-control" name="link" id="link" value=""></input>
              </th>
            </tr>

          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
<br><br>

<div class="navbar2">
  <table style="float:right">
    <tr>
      <td style="padding:5px">
        <a type="button" id="btn-voltar" class="btn btn-info btn-voltar">Voltar</a>
      </td>

      <td style="padding:5px">
        <a type="button" id="btn-cadastrar" class="btn btn-success">Cadastrar</a>
      </td>
    </tr>
  </table>
</div>

<script>
$(document).ready(function() {

  $(".btn-voltar").click(function(){

    window.location = "{{ URL::to('/titulos/') }}";
  })

  $("#btn-cadastrar").click(function(){

    $btn = $(this);
    $btn.attr('disabled',false);

    url = "{{ URL::to('/cadastra-titulo/') }}";

    var cor = $('#cor');
    var descricao = $('#descricao');
    var link = $('#link');
    var categoria = $('#categoria');

    if(cor.val() == '' || descricao.val() == '' || categoria.val() == 0){
      swal("Erro!", "Por favor preencha os campos (Cor, Titulo e Categoria).", "error");
      return false;
    }

    $.ajax({
      url: url,
      type: 'POST',
      data: {
        descricao: descricao.val(),
        categoria: categoria.val(),
        link: link.val(),
        cor: cor.val(),
        "_token":"{{ csrf_token() }}"
      },
      error: function(jq,status,message) {
        swal("Erro!", "Erro ao adicionar a titulo.", "error");
        $btn.attr('disabled',false);
        return false;
      }
    })
    .done(function( msg ) {

      if(msg==1)
      {
        $('#mensagem').val('');

        swal({
          title: "",
          text: "Titulo Acicionada.",
          type: "success",
          showCancelButton: false,
          cancelButtonText: "Não",
          confirmButtonColor: "#1f90bb",
          confirmButtonText: "OK",
          closeOnConfirm: true
        } ,
        function(){
          swal({
            title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
            text: 'Aguarde...',
            html: true,
            showCancelButton: false,
            showConfirmButton: false,
            closeOnConfirm: false,
            closeOnCancel: false
          });

          window.location = "{{ URL::to('/titulos') }}";
        })
        .fail(function( jqXHR, textStatus ) {
          swal("Erro!", "Erro ao finalizar o titulo.", "error");
          $btn.attr('disabled',false);
        })
      }
    });

  })
/*
  $("#btn-atualizar").click(function(){
    $('#modal-atualizar').modal('show');
  })
*/
});


</script>


@endsection

<style>

.navbar2 {
  overflow: hidden;
  background-color: #333;
  position: fixed;
  bottom: 0;
  padding:10px;
  width: 100%;
}

.navbar2 a:hover {
  background: #ddd;
  color: black;
}

</style>
