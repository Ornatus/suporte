@extends('layouts.menu')

@section('content')

<div class="row" style="margin:10px !important">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">

      </div>
      <div class="panel-body">
        <table class="table table-striped table-responsive" style="font-size: 10pt; font-family: Verdana;" >
          <thead>

            <tr>
              <th scope="col">
                Chamado:<br>
                <?php echo $chamado[0]->chamado_id; ?>
              </th>
              <th scope="col">
                Loja:<br>
                <?php echo $chamado[0]->brazil_store_name; ?>
              </th>
              <th scope="col">
                Data Abertura:<br>
                <?php echo $chamado[0]->data_chamado; ?>
              </th>
              <th scope="col">
                Solicitante:<br>
                <?php echo $chamado[0]->solicitante; ?><br>
                <?php echo $chamado[0]->telefone; ?>
              </th>
              <th scope="col" colspan="4">
                Categoria:<br>
                <span class="label label-default" style="background-color:<?php echo $chamado[0]->cor_categoria; ?>"><?php echo $chamado[0]->categoria; ?></span>
              </th>
            </tr>
            <tr>
              <td  scope="col" colspan="4"><b>Descrição:</b><br>
                <?php echo wordwrap($chamado[0]->descricao,200,"<br>"); ?>
              </td>
            </tr>

            <td style="padding:5px">
              <b>Titulo:</b><br>
              <select name="titulo" id="titulo" class="form-control" style="width:200px; margin-bottom:10px">
                <option value="0">Selecionar</option>
                <!-- mostrar o titulo do chamando independente da categoria -->
                <?php if($chamado[0]->titulo > 0) {?>
                  <option selected value="<?php echo $chamado[0]->titulo ?>"> <?php echo  $chamado[0]->titulo_descricao ?> </option>
                <?php } ?>
                <!-- gambiarra para o suporte mudar a categoria mantendo os titulos -->
                <!-- se no looping existir o nome ja printado a cima, não ira mostra-lo -->
                <?php foreach($titulos as $titulo){ ?>
                  <?php if($chamado[0]->titulo_id != $titulo->titulo_id) {?>
                    <option <?php echo ($chamado[0]->titulo_id == $titulo->titulo_id) ? 'selected' : ''; ?>  value="<?php echo $titulo->titulo_id; ?>"><?php echo $titulo->descricao; ?></option>
                  <?php } ?>
                <?php } ?>
              </select>
            </td>

            <td style="padding:5px">
              <b>Prioridade:</b><br>
              <select name="prioridade" id="prioridade" class="form-control" style="width:auto; margin-bottom:10px">
                <option value="0">Selecionar</option>
                <?php foreach($prioridades as $prioridade){ ?>
                  <option <?php echo ($chamado[0]->prioridade_id == $prioridade->prioridade_id) ? 'selected' : ''; ?>  value="<?php echo $prioridade->prioridade_id; ?>"><?php echo $prioridade->descricao; ?></option>
                <?php } ?>
              </select>
            </td>

            <td style="padding:5px">
              <b>Tipo:</b><br>
              <select name="tipo" id="tipo" class="form-control" style="width:auto; margin-bottom:10px">
                <option value="0">Selecionar</option>
                <?php foreach($tipos as $tipo){ ?>
                  <option <?php echo ($chamado[0]->tipo_id == $tipo->tipo_id) ? 'selected' : ''; ?>  value="<?php echo $tipo->tipo_id; ?>"><?php echo $tipo->descricao; ?></option>
                <?php } ?>
              </select>
            </td>

            <td style="padding:5px" colspan="2">
              <b>Categoria:</b><br>
              <?php if($chamado[0]->atualiza_categoria == 0) { ?>
                <select name="categoria" id="categoria" class="form-control" style="width:auto; margin-bottom:10px">
                  <option selected value="<?php echo $chamado[0]->categoria_id; ?>"><?php echo $chamado[0]->categoria; ?></option>
                </select>
              <?php }else{ ?>
                <select name="categoria" id="categoria" class="form-control" style="width:auto; margin-bottom:10px">
                  <option value="0">Selecionar</option>
                  <?php foreach($categorias as $categoria){ ?>
                    <option <?php echo ($chamado[0]->categoria_id == $categoria->categoria_id) ? 'selected' : ''; ?>  value="<?php echo $categoria->categoria_id; ?>"><?php echo $categoria->descricao; ?></option>
                  <?php } ?>
                </select>
              <?php } ?>
            </td>

            <td style="padding:27px" colspan="2">
              <?php if( $chamado[0]->status_id == 2 && $chamado[0]->atendente_id == $usuario->user_id ){ ?>
                <button style=" background-color:red;color:white;" type="button" id="btn-bolsao" class="btn" data-dismiss="modal">Retornar ao Bolsão</button>
              <?php }?>
            </td>

            <tr style="border:0px !important">
              <td colspan="2" style="border:0px !important"><b>Status:</b><br>
                <span class="label label-default"  style="font-size: 15pt; padding: 1px; color:white; background-color:<?php echo $chamado[0]->status_cor; ?>"><b><?php echo $chamado[0]->status; ?></b></span>
              </td>
            </tr>

            <?php if( $chamado[0]->status_id > 1 ){ ?>
              <tr>
                <td colspan="6" scope="col"><b>Atendido por:</b><br>
                  <?php echo $chamado[0]->atendente_nome; ?>
                </td>
              </tr>
              <tr>
                <td  scope="col"><b>Início atendimento:</b><br>
                  <?php echo $chamado[0]->data_atribuido; ?>
                </td>

              </tr>
            <?php } ?>
            <?php if( $chamado[0]->status_id == 3 ){ ?>
              <tr>
                <td  scope="col" colspan="4"><b>Finalizado em:</b><br>
                  <?php echo $chamado[0]->data_resolvido; ?>
                </td>
              </tr>
              <tr>
                <td  scope="col" colspan="4"><b>Resolução:</b><br>
                  <?php echo $chamado[0]->resolucao; ?>
                </td>
              </tr>
            <?php } ?>

          </thead>
        </table>

        <?php if( $chamado[0]->status_id == 2 && $chamado[0]->atendente_id == $usuario->user_id  ){ ?>
          <div class="md-form">
            <i class="fas fa-pencil-alt prefix"></i>
            <label for="lbl-mensagem">Nova Mensagem:</label>
            <textarea id="mensagem" name="mensagem" class="md-textarea form-control" rows="3"></textarea>
          </div>

          <label for="exampleInputEmail1"><b>Enviar arquivos (Enviar arquivos de até <b>5Mb</b>):</b></label>
          <div id="product_image" style="width:30% !important; height:2%;">
          </div>

          <table border=0 width="100%">
            <tr>

              <td style="text-align:left !important;" width="14%">
                <div id="img1">
                </div>
                <label class="fileContainer addFile1" style="margin:0px;">
                  Selecionar arquivo
                  <input name="image" class="image inputFile1" type="file" accept="image/*" capture="camera" data-id="1">
                </label>
                <label class="fileContainer deleteFile1 hide fa fa-trash-o" style="margin-top:3px; background-color:black !important; width:130px; text-align:center !important;" data-id="1" onclick="deleteFile(1);">
                </label>
                <input class="textimg1 hide" name="image[]" data-id="1" type="text" value="" />
              </td>

              <td  width="85%" style=" text-align:right !important;">
                <div class="pull-right" style="margin:5px">
                  <a class="btn btn-primary btn-enviar">Enviar Mensagem</a>
                </div>
              </td>
            </tr>
          </table>

        <?php } ?>
        <br>
        <br>
        <h5><i class="fa fa-file-o" style="font-size:12pt" aria-hidden="true"></i> Arquivos </h5>
        <hr></hr>
        <div id="div-arquivos">

        </div>
        <br>
        <br>
        <h5><i class="fa fa-envelope" style="font-size:12pt" aria-hidden="true"></i> Mensagens </h5>
        <hr></hr>
        <div id="div-mensagens">

        </div>
      </div>
    </div>
  </div>
</div>
<br><br>

<div class="navbar2">
  <table style="float:right">
    <tr>
      <td style="padding:5px">
        <a type="button" id="btn-voltar" class="btn btn-info btn-voltar">Voltar</a>
      </td>
      <?php if($chamado[0]->atendente_id == $usuario->user_id || $usuario->username == 'livia.matos' ){ ?>
        <td style="padding:5px">
          <a type="button" id="btn-atualizar" class="btn btn-primary btn-atualizar">Atualizar</a>
        </td>
      <?php } ?>
      <?php if( $chamado[0]->status_id == 1 ){ ?>
        <td style="padding:5px">
          <a type="button" id="btn-atender" class="btn btn-success">Atender este chamado</a>
        </td>
      <?php }else{ ?>
        <?php if($chamado[0]->finaliza_chamado == 1){ ?>
          <?php if( $chamado[0]->status_id == 2 && $chamado[0]->atendente_id == $usuario->user_id  ){ ?>
            <td style="padding:5px">
              <a type="button" id="btn-finalizar" class="btn btn-success">Finalizar chamado</a>
            </td>
          <?php } ?>
        <?php }elseif($chamado[0]->finaliza_chamado == 0 && $chamado[0]->status_id == 2  && $chamado[0]->atendente_id == $usuario->user_id) { ?>
          <!--td style="padding:5px">
          <a type="button" id="btn-solicitar-analise" class="btn btn-warning">Solicitar Análise</a>
        </td-->
      <?php }?>
    <?php }?>

    <?php if( isset($usuario_configuracao->aprovar_chamado) && $usuario_configuracao->aprovar_chamado == 1 && $chamado[0]->status_id == 4 ){ ?>
      <!--td style="padding:5px">
      <a type="button" id="btn-negar-analise" class="btn btn-danger">Negar Análise</a>
    </td>
    <td style="padding:5px">
    <a type="button" id="btn-finalizar" class="btn btn-success">Finalizar chamado</a>
  </td-->
<?php }?>



</tr>
</table>
</div>

<!-- Modal da finalização do chamado -->
<!-- Modal -->
<div class="modal fade" id="modal-finaliza" tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="TituloModalCentralizado">Resolução do chamado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="md-form">
          <i class="fas fa-pencil-alt prefix"></i>
          <textarea id="resolucao" name="resolucao" class="md-textarea form-control" rows="3"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary btn-modalfinalizar">Finalizar</button>
      </div>
    </div>
  </div>
</div>
<!-- modal Analise chamado -->
<div class="modal fade" id="modal-finaliza-analise" tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="TituloModalCentralizado">Resposta para análise do chamado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="md-form">
          <i class="fas fa-pencil-alt prefix"></i>
          <textarea id="resolucao-analise" name="resolucao-analise" class="md-textarea form-control" rows="3"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary btn-modalfinalizar-analise">Enviar Análise</button>
      </div>
    </div>
  </div>
</div>
<!-- fim modal -->

<script>


function deleteFile(id){

  $('.textimg'+id).val('');
  $('.addFile'+id).removeClass('hide');
  $('.addFile'+id).addClass('show');
  $('#img'+id).html('');
  $('.deleteFile'+id).addClass('hide');
  $('.deleteFile'+id).removeClass('show');
  $('.inputFile'+id).val('');

}



$(document).ready(function() {

  getMensagensByChamadoId();
  getFilesByChamadoId();

  //botão voltar
  $(".btn-voltar").click(function(){

    window.location = "{{ URL::to('/chamados/') }}";
  })

  //botao retorno bolsao

  //botão atualizar
  $("#btn-bolsao").click(function(){

    $btn = $(this);
    $btn.attr('disabled',false);

    url = "{{ URL::to('/retorna_bolsao') }}";

    $.ajax({
      url: url,
      type: 'POST',
      data: {
        chamado_id: '<?php echo $chamado[0]->chamado_id; ?>',
        "_token":"{{ csrf_token() }}"
      },
      error: function(jq,status,message) {
        swal("Erro!", "Erro ao retornar o chamado.", "error");
        $btn.attr('disabled',false);
        return false;
      }
    })
    .done(function( msg ) {

      if(msg==1)
      {
        swal({
          title: "",
          text: "Chamado retornado ao bolsão com sucesso.",
          type: "success",
          showCancelButton: false,
          cancelButtonText: "Não",
          confirmButtonColor: "#1f90bb",
          confirmButtonText: "OK",
          closeOnConfirm: true
        } ,
        function(){
          swal({
            title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
            text: 'Aguarde...',
            html: true,
            showCancelButton: false,
            showConfirmButton: false,
            closeOnConfirm: false,
            closeOnCancel: false
          });

          window.location = "{{ URL::to('/chamado/') }}/"+<?php echo $chamado[0]->chamado_id; ?>;
        })
        .fail(function( jqXHR, textStatus ) {
          swal("Erro!", "Erro ao atualizar o chamado.", "error");
          $btn.attr('disabled',false);
        })
      }
    });

  })


  //botão atualizar
  $(".btn-atualizar").click(function(){

    $btn = $(this);
    $btn.attr('disabled',false);

    var tipo = $('#tipo');
    var prioridade = $('#prioridade');
    var acao = $('#prioridade');
    var titulo = $('#titulo');
    var categoria = $('#categoria');
    var atualiza_categoria = '<?php echo $chamado[0]->atualiza_categoria; ?>';

    if(tipo.val() == 0 || prioridade.val() == 0 || categoria.val() == 0 ){
      swal("Erro!", "Por favor preencha todos os campos.", "error");
      return false;
    }
    //rota
    url = "{{ URL::to('/atualiza-chamado') }}";

    $.ajax({
      url: url,
      type: 'POST',
      data: {
        chamado_id: '<?php echo $chamado[0]->chamado_id; ?>',
        tipo_id: tipo.val(),
        titulo_id: titulo.val(),
        prioridade_id: prioridade.val(),
        categoria_id: categoria.val(),
        atualiza_categoria: atualiza_categoria,
        "_token":"{{ csrf_token() }}"
      },
      error: function(jq,status,message) {
        swal("Erro!", "Erro ao atualizar o chamado.", "error");
        $btn.attr('disabled',false);
        return false;
      }
    })
    .done(function( msg ) {

      if(msg==1)
      {
        swal({
          title: "",
          text: "Chamado atualizado.",
          type: "success",
          showCancelButton: false,
          cancelButtonText: "Não",
          confirmButtonColor: "#1f90bb",
          confirmButtonText: "OK",
          closeOnConfirm: true
        } ,
        function(){
          swal({
            title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
            text: 'Aguarde...',
            html: true,
            showCancelButton: false,
            showConfirmButton: false,
            closeOnConfirm: false,
            closeOnCancel: false
          });

          window.location = "{{ URL::to('/chamado/') }}/"+<?php echo $chamado[0]->chamado_id; ?>;
        })
        .fail(function( jqXHR, textStatus ) {
          swal("Erro!", "Erro ao atualizar o chamado.", "error");
          $btn.attr('disabled',false);
        })
      }
    });

  })

  // botão finalizar
  $(".btn-modalfinalizar").click(function(){

    $btn = $(this);
    $btn.attr('disabled',true);

    $resolucao = $("#resolucao").val();

    url = "{{ URL::to('/finaliza-chamado/') }}";

    $.ajax({
      url: url,
      type: 'POST',
      data: {
        resolucao:$resolucao,
        chamado_id: '<?php echo $chamado[0]->chamado_id; ?>',
        "_token":"{{ csrf_token() }}"
      },
      error: function(jq,status,message) {
        swal("Erro!", "Erro ao finalizar o chamado.", "error");
        $btn.attr('disabled',false);
        return false;
      }
    })
    .done(function( msg ) {

      if(msg==1)
      {
        $('#mensagem').val('');

        swal({
          title: "",
          text: "Chamado finalizado.",
          type: "success",
          showCancelButton: false,
          cancelButtonText: "Não",
          confirmButtonColor: "#1f90bb",
          confirmButtonText: "OK",
          closeOnConfirm: true
        } ,
        function(){
          swal({
            title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
            text: 'Aguarde...',
            html: true,
            showCancelButton: false,
            showConfirmButton: false,
            closeOnConfirm: false,
            closeOnCancel: false
          });

          window.location = "{{ URL::to('/chamado/') }}/"+<?php echo $chamado[0]->chamado_id; ?>;
        })
        .fail(function( jqXHR, textStatus ) {
          swal("Erro!", "Erro ao finalizar o chamado.", "error");
          $btn.attr('disabled',false);
        })
      }
    });

  })

  $("#btn-finalizar").click(function(){
    $("#resolucao").val('');
    $('#modal-finaliza').modal('show');
  })

  $(".btn-enviar").click(function(){

    swal({
      title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
      text: 'Aguarde...',
      html: true,
      showCancelButton: false,
      showConfirmButton: false,
      closeOnConfirm: false,
      closeOnCancel: false
    });

    url = "{{ URL::to('/envia-mensagem/') }}";

    var arquivos = [];
    var lista_arquivos = document.getElementsByName("image[]");

    for(var i =0; i< lista_arquivos.length;i++){
      arquivos.push(lista_arquivos[i].value);
    }

    var $mensagem = $('#mensagem').val();

    if( $mensagem.length < 2)
    {
      swal("Atenção", "A mensagem deve ter pelo menos 3 caracteres.", "info");
      return false;
    }

    $.ajax({
      url: url,
      type: 'POST',
      data: {
        mensagem:$mensagem,
        chamado_id: '<?php echo $chamado[0]->chamado_id; ?>',
        arquivos:arquivos,
        "_token":"{{ csrf_token() }}"
      },
      error: function(jq,status,message) {
        swal("Erro!", "Erro ao enviar a mensagem.", "error");
        return false;
      }
    })
    .done(function( msg ) {

      if(msg==1)
      {
        $('#mensagem').val('');

        deleteFile(1);

        swal({
          title: "",
          text: "Mensagem enviada com sucesso.",
          type: "success",
          showCancelButton: false,
          cancelButtonText: "Não",
          confirmButtonColor: "#1f90bb",
          confirmButtonText: "OK",
          closeOnConfirm: true
        }
        ,
        function(){
          getMensagensByChamadoId();
        })
        .fail(function( jqXHR, textStatus ) {
          swal("Erro!", "Erro ao enviar os dados.", "error");
        })
      }
    });
  })


  $("#btn-atender").click(function(){
    var tipo = $('#tipo');
    var titulo = $('#titulo');
    var prioridade = $('#prioridade');
    var categoria = $('#categoria');

    if(tipo.val() == 0 || prioridade.val() == 0 || categoria.val() == 0 || titulo.val() == 0){
      swal("Erro!", "Por favor preencha todos os campos.", "error");
      return false;
    }

    url = "{{ URL::to('/atender-chamado/') }}";

    swal({
      title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
      text: 'Aguarde...',
      html: true,
      showCancelButton: false,
      showConfirmButton: false,
      closeOnConfirm: false,
      closeOnCancel: false
    });

    $.ajax({
      url: url,
      type: 'POST',
      data: {
        chamado_id: '<?php echo $chamado[0]->chamado_id; ?>',
        tipo_id: tipo.val(),
        titulo_id: titulo.val(),
        prioridade_id: prioridade.val(),
        categoria_id: categoria.val(),
        "_token":"{{ csrf_token() }}"
      },
      error: function(jq,status,message) {
        swal("Erro!", "Erro ao atribuir o chamado", "error");
        return false;
      }
    })
    .done(function( msg ) {

      if(msg.response==true)
      {
        window.location = "{{ URL::to('/chamado/') }}/"+<?php echo $chamado[0]->chamado_id; ?>;
      }
      else
      {
        swal({
          title: "",
          text: "Este chamado foi atribuído a outro atendente.("+msg.chamado[0].atendente_nome+")",
          type: "info",
          showCancelButton: false,
          cancelButtonText: "Não",
          confirmButtonColor: "#1f90bb",
          confirmButtonText: "OK",
          closeOnConfirm: true
        }
        ,
        function(){
          window.location = "{{ URL::to('/chamado/') }}/"+<?php echo $chamado[0]->chamado_id; ?>;
        })
      }
    });
  })


  $('.image').change(function (event) {
    var btn = $(this);
    var id_button_file = $(this).data('id');

    $("#base_image").val('');
    form = new FormData();

    form.append('image', event.target.files[0]);
    form.append('_token',"{{ csrf_token() }}");

    var url = "{{ URL::to('/uploadFile/') }}";

    $.ajax({
      url: url,
      cache:false,
      contentType:false,
      processData: false,
      data: form,
      type: 'POST',
      success: function (data) {

        if(data == 'false'){
          swal("Atenção", "Enviar arquivos somente de até 5Mbs.", "info");
          return false;
        }

        var obj = data;

        if(obj.file.extension.toLowerCase() == 'jpg' ||  obj.file.extension.toLowerCase() == 'png' || obj.file.extension.toLowerCase() == 'jpeg' || obj.file.extension.toLowerCase() == 'gif'){
          $('#img'+ id_button_file).html(' <img class="img_chamado" width=130px heigth=130px onclick="openModal(this);" src="data:image/'+ obj.file.extension + ';base64,' + obj.file.base64 + '" ></img>' );
        }else{
          $('#img'+ id_button_file).html('<div class="fileContainer fa fa-newspaper-o"  style="width:130px; heigth:130px; background-color:black !important;"> <br>'+ obj.file.name + '  </div>' );
        }

        $('.textimg'+id_button_file).val(obj.file.arquivo_id);
        $('.addFile'+id_button_file).addClass('hide');
        $('.addFile'+id_button_file).removeClass('show');
        $('.deleteFile'+id_button_file).removeClass('hide');
        $('.deleteFile'+id_button_file).addClass('show');

      }
    });

  });

  $('.deleteFile').click(function (event) {
    var btn = $(this);
    var id_button_file = $(this).data('id');

    $("#base_image").val('');
    form = new FormData();

    form.append('image', event.target.files[0]);
    form.append('_token',"{{ csrf_token() }}");

    var url = "{{ URL::to('/uploadFile/') }}";

    $.ajax({
      url: url,
      data: form,
      processData: false,
      contentType: false,
      type: 'POST',
      success: function (data) {
        var obj = data;

        $('#img'+ id_button_file).html(' <img class="img_chamado" width=130px heigth=130px onclick="openModal(this);" src="data:image/'+ obj.file.extension + ';base64,' + obj.file.base64 + '" ></img>' );
        $('.textimg'+id_button_file).val(obj.file.arquivo_id);

        $('.addFile'+id_button_file).addClass('hide');
      }
    });

  });



  $("#btn-solicitar-analise").click(function(){

    url = "{{ URL::to('/solicita-analise') }}";

    swal({
      title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
      text: 'Aguarde...',
      html: true,
      showCancelButton: false,
      showConfirmButton: false,
      closeOnConfirm: false,
      closeOnCancel: false
    });

    $.ajax({
      url: url,
      type: 'POST',
      data: {
        chamado_id: '<?php echo $chamado[0]->chamado_id; ?>',
        "_token":"{{ csrf_token() }}"
      },
      error: function(jq,status,message) {
        swal("Erro!", "Erro ao atualizar chamado", "error");
        return false;
      },
      success: function(dataReturn, textStatus, xhr)
      {

        if(xhr.status == 200)
        {
          swal({
            title: "Atenção",
            text: dataReturn.mensagem,
            html: true,

          },  function () {
            location.reload(true);
            tr.hide();
            window.location = "{{ URL::to('/chamado/') }}/"+<?php echo $chamado[0]->chamado_id; ?>;
          });

        }

      }
    });

  });


  $("#btn-negar-analise").click(function(){
    $("#resolucao-analise").val('');
    $('#modal-finaliza-analise').modal('show');
  })

  $(".btn-modalfinalizar-analise").click(function(){

    $btn = $(this);
    $btn.attr('disabled',true);

    $resolucao_analise = $("#resolucao-analise").val();

    url = "{{ URL::to('/negar-analise/') }}";

    $.ajax({
      url: url,
      type: 'POST',
      data: {
        mensagem:$resolucao_analise,
        chamado_id: '<?php echo $chamado[0]->chamado_id; ?>',
        "_token":"{{ csrf_token() }}"
      },
      error: function(jq,status,message) {
        swal("Erro!", "Erro ao negar análise.", "error");
        $btn.attr('disabled',false);
        return false;
      }
    })
    .done(function( msg ) {

      if(msg==1)
      {
        $('#mensagem').val('');

        swal({
          title: "",
          text: "Análise negada.",
          type: "success",
          showCancelButton: false,
          cancelButtonText: "Não",
          confirmButtonColor: "#1f90bb",
          confirmButtonText: "OK",
          closeOnConfirm: true
        } ,
        function(){
          swal({
            title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
            text: 'Aguarde...',
            html: true,
            showCancelButton: false,
            showConfirmButton: false,
            closeOnConfirm: false,
            closeOnCancel: false
          });

          window.location = "{{ URL::to('/chamado/') }}/"+<?php echo $chamado[0]->chamado_id; ?>;
        })
        .fail(function( jqXHR, textStatus ) {
          swal("Erro!", "Erro ao finalizar análise.", "error");
          $btn.attr('disabled',false);
        })
      }
    });

  })


});



function getMensagensByChamadoId()
{

  $("#div-mensagens").html('<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>');

  url = "{{ URL::to('/chamado-mensagem/') }}";

  $.ajax({
    url: url,
    type: 'POST',
    data: {
      chamado_id: '<?php echo $chamado[0]->chamado_id; ?>',
      "_token":"{{ csrf_token() }}"
    }
  })
  .done(function( msg ) {

    $("#div-mensagens").html(msg.html);

  });
}

function getFilesByChamadoId()
{

  $("#div-arquivos").html('<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>');

  url = "{{ URL::to('/chamado-arquivo/') }}";

  $.ajax({
    url: url,
    type: 'POST',
    data: {
      chamado_id: '<?php echo $chamado[0]->chamado_id; ?>',
      "_token":"{{ csrf_token() }}"
    }
  })
  .done(function( msg ) {

    $("#div-arquivos").html(msg.html);

  });
}


</script>


@endsection

<style>

.navbar2 {
  overflow: hidden;
  background-color: #333;
  position: fixed;
  bottom: 0;
  padding:10px;
  width: 100%;
}

.navbar2 a:hover {
  background: #ddd;
  color: black;
}

.fileContainer {
  overflow: hidden;
  position: relative;
  background-color:red !important;
  color:white;
  padding:10px;
}

.fileContainer [type=file] {
  cursor: inherit;
  display: block;
  font-size: 999px;
  filter: alpha(opacity=0);
  min-height: 100%;
  min-width: 100%;
  opacity: 0;
  position: absolute;
  right: 0;
  text-align: right;
  top: 0;

}

</style>
