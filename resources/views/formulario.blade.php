@extends('layouts.entrar')

@section('content')


<script
src="https://code.jquery.com/jquery-2.2.4.min.js"
integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
crossorigin="anonymous"></script>
<!-- script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.0.0/sweetalert.min.js"></script-->
<script src="{{ asset('js/sweetalert/dist/sweetalert.min.js') }}"></script>
<script src="{{ asset('js/jquery.mask.js') }}"></script>

<link href="{{ asset('js/sweetalert/dist/sweetalert.css') }}" rel="stylesheet">


<script>
$( document ).ready(function() {

  $('#EspacoDisco').mask('00000');
  $('#MemRam').mask('000');
  $('#PdwMysql').mask('AAAA');

});


function isNumber(evt) {
  var iKeyCode = (evt.which) ? evt.which : evt.keyCode
  if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
  return false;

  return true;
}


function contar(input, tamanho_limite)
{

  valor = $(input).val();
  tamanho = $(input).val().length;
  valor_limite = valor.substring(0,tamanho_limite);

  console.log(valor, valor_limite);

  if(tamanho > tamanho_limite)
  {
    $(input).val('');
    $(input).val(valor_limite);
  }
}

</script>
<style>

.navbar2 {
  overflow: hidden;
  background-color: #333;
  position: fixed;
  bottom: 0;
  padding:10px;
  width: 100%;
}

.navbar2 a:hover {
  background: #ddd;
  color: black;
}


</style>

<div class="container">

  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading">Informe os dados abaixo <br> <?php echo (isset($cadastro[0]->DT_ULTIMA_ATUALIZACAO) ) ? 'Última atualização ['.$cadastro[0]->DT_ULTIMA_ATUALIZACAO.']' : ''; ?></div>

            <div class="panel-body">
              <form id="formulario" name="formulario">
                <div class="form-group">
                  <label for="loja">Loja</label><br>
                  <input id="CodArea" name="CodArea" type="hidden" value="<?php echo $loja[0]->codigo; ?>" />
                  <?php echo $loja[0]->nome; ?>
                </div>

                <div class="form-group IndProcIntel_DIV">
                  <label for="data_instalacao">Indicador se o processador do PC é Intel</label>
                  <select class="form-control" name="IndProcIntel" id="IndProcIntel">
                    <option <?php echo (isset($cadastro[0]->IndProcIntel) && $cadastro[0]->IndProcIntel == 0) ? 'selected=selected' : ''; ?> value="0"></option>
                    <option <?php echo (isset($cadastro[0]->IndProcIntel) && $cadastro[0]->IndProcIntel == 1) ? 'selected=selected' : ''; ?> value="1">Sim</option>
                    <option <?php echo (isset($cadastro[0]->IndProcIntel) && $cadastro[0]->IndProcIntel == 2) ? 'selected=selected' : ''; ?> value="2">Não</option>
                  </select>
                </div>

                <div class="form-group EspacoDisco_DIV">
                  <label for="label_campo">Espaço livre em disco (GigaBytes)</label>
                  <input type="text" class="form-control" onkeypress="javascript:return isNumber(event)" onkeyup="javascript:return contar(this,5)" name="EspacoDisco" id="EspacoDisco" value="<?php echo (isset($cadastro[0]->EspacoDisco)) ? $cadastro[0]->EspacoDisco : ''; ?>">
                </div>

                <div class="form-group MemRam_DIV">
                  <label for="label_campo">Memória RAM (GigaBytes)</label>
                  <spam style="font-size: 12pt; background-color:red; color:white"><br>
                    <spam>( Para instalar o sistema é necessário no mínimo 4 GB de memória )</spam>
                  </label>
                  <input type="text" onkeypress="javascript:return isNumber(event)" onkeyup="javascript:return contar(this,3)" class="form-control" name="MemRam" id="MemRam" value="<?php echo (isset($cadastro[0]->MemRam)) ? $cadastro[0]->MemRam : ''; ?>">
                </div>

                <div class="form-group IndSO_DIV">
                  <label for="data_instalacao">Indicar o sistema operacional</label><br>
                  <spam style="font-size: 12pt; background-color:red; color:white">( Para instalar o sistema é obrigatório o uso de Windows 10 64 Bits )</spam>
                </label>
                  <select class="form-control" name="IndSO" id="IndSO">
                    <option <?php echo (isset($cadastro[0]->IndSO) && $cadastro[0]->IndSO == 0) ? 'selected=selected' : ''; ?> value="0"></option>
                    <option <?php echo (isset($cadastro[0]->IndSO) && $cadastro[0]->IndSO == 1) ? 'selected=selected' : ''; ?> value="1">Inferior Windows 7</option>
                    <option <?php echo (isset($cadastro[0]->IndSO) && $cadastro[0]->IndSO == 2) ? 'selected=selected' : ''; ?> value="2">Windows 7 32 Bits</option>
                    <option <?php echo (isset($cadastro[0]->IndSO) && $cadastro[0]->IndSO == 3) ? 'selected=selected' : ''; ?> value="3">Windows 7 64 Bits</option>
                    <option <?php echo (isset($cadastro[0]->IndSO) && $cadastro[0]->IndSO == 4) ? 'selected=selected' : ''; ?> value="4">Windows 8 32 Bits</option>
                    <option <?php echo (isset($cadastro[0]->IndSO) && $cadastro[0]->IndSO == 5) ? 'selected=selected' : ''; ?> value="5">Windows 8 64 Bits</option>
                    <option <?php echo (isset($cadastro[0]->IndSO) && $cadastro[0]->IndSO == 6) ? 'selected=selected' : ''; ?> value="6">Windows 8.1 32 Bits</option>
                    <option <?php echo (isset($cadastro[0]->IndSO) && $cadastro[0]->IndSO == 7) ? 'selected=selected' : ''; ?> value="7">Windows 8.1 64 Bits</option>
                    <option <?php echo (isset($cadastro[0]->IndSO) && $cadastro[0]->IndSO == 8) ? 'selected=selected' : ''; ?> value="8">Windows 10 32 Bits</option>
                    <option <?php echo (isset($cadastro[0]->IndSO) && $cadastro[0]->IndSO == 9) ? 'selected=selected' : ''; ?> value="9">Windows 10 64 Bits</option>
                  </select>
                </div>

                <div class="form-group IndMySql_DIV">
                  <label for="data_instalacao">Indicador se possui MYSQL</label>
                  <select class="form-control" name="IndMySql" id="IndMySql">
                    <option <?php echo (isset($cadastro[0]->IndMySql) && $cadastro[0]->IndMySql == 0) ? 'selected=selected' : ''; ?> value="0"></option>
                    <option <?php echo (isset($cadastro[0]->IndMySql) && $cadastro[0]->IndMySql == 1) ? 'selected=selected' : ''; ?> value="1">Sim</option>
                    <option <?php echo (isset($cadastro[0]->IndMySql) && $cadastro[0]->IndMySql == 2) ? 'selected=selected' : ''; ?> value="2">Não</option>
                  </select>
                </div>

                <div class="form-group PwdMySql_DIV">
                  <label for="label_campo">Senha do usuário ROOT no Mysql</label>
                  <input type="text" class="form-control" onkeyup="javascript:return contar(this,20)" name="PwdMySql" id="PwdMySql" value="<?php echo (isset($cadastro[0]->PwdMySql)) ? $cadastro[0]->PwdMySql : ''; ?>">
                </div>

                <div class="form-group TamBanda_DIV">
                  <label for="label_campo">Tamanho da velocidade de banda da internet (MegaBytes)</label>
                  <input type="text" class="form-control"  onkeypress="javascript:return isNumber(event)"  onkeyup="javascript:return contar(this,3)" name="TamBanda" id="TamBanda" value="<?php echo (isset($cadastro[0]->TamBanda)) ? $cadastro[0]->TamBanda : ''; ?>">
                </div>
                <div class="form-group TvwVersao_DIV">
                  <label for="label_campo">Número da Versão do Team Viewer Instalada. Digitar zero(0) se não instalada</label>
                  <input type="text" onkeypress="javascript:return isNumber(event)"  onkeyup="javascript:return contar(this,2)" class="form-control" name="TvwVersao" id="TvwVersao" value="<?php echo (isset($cadastro[0]->TvwVersao)) ? $cadastro[0]->TvwVersao : ''; ?>">
                </div>

                <div class="form-group TvwId_DIV">
                  <label for="label_campo">Número do ID do parceiro</label>
                  <input type="text" onkeypress="javascript:return isNumber(event)"  onkeyup="javascript:return contar(this,10)"  class="form-control" name="TvwId" id="TvwId" value="<?php echo (isset($cadastro[0]->TvwId)) ? $cadastro[0]->TvwId : ''; ?>">
                </div>
                <div class="form-group TvwPwd_DIV">
                  <label for="label_campo">Senha de acesso ao Team Viewer</label>
                  <input type="text" class="form-control" onkeyup="javascript:return contar(this,10)" name="TvwPwd" id="TvwPwd" value="<?php echo (isset($cadastro[0]->TvwPwd)) ? $cadastro[0]->TvwPwd : ''; ?>">
                </div>
                <div class="form-group CNPJ_DIV">
                  <label for="label_campo">CNPJ (Apenas números)</label>
                  <input type="text" class="form-control" onkeypress="javascript:return isNumber(event);"  onkeyup="javascript:return contar(this,14)" name="CNPJ" id="CNPJ" value="<?php echo (isset($cadastro[0]->CNPJ)) ? $cadastro[0]->CNPJ : ''; ?>">
                </div>

                <div class="form-group INSCESC_DIV">
                  <label for="label_campo">Número da Inscrição Estadual</label>
                  <input type="text" class="form-control"  onkeypress="javascript:return isNumber(event)"  onkeyup="javascript:return contar(this,20)" name="INSCESC" id="INSCESC" value="<?php echo (isset($cadastro[0]->INSCESC)) ? $cadastro[0]->INSCESC : ''; ?>">
                </div>

                <div class="form-group INSCMUN_DIV">
                  <label for="label_campo">Número da Inscrição Munícipal</label>
                  <input type="text" class="form-control" onkeypress="javascript:return isNumber(event)"  onkeyup="javascript:return contar(this,20)" name="INSCMUN" id="INSCMUN" value="<?php echo (isset($cadastro[0]->INSCMUN)) ? $cadastro[0]->INSCMUN : ''; ?>">
                </div>

                <div class="form-group RAZAO_DIV">
                  <label for="label_campo">Razão Social</label>
                  <input type="text" class="form-control" onkeyup="javascript:return contar(this,50)" name="RAZAO" id="RAZAO" value="<?php echo (isset($cadastro[0]->RAZAO)) ? $cadastro[0]->RAZAO : ''; ?>">
                </div>

                <div class="form-group FANTASIA_DIV">
                  <label for="label_campo">Nome Fantasia</label>
                  <input type="text" class="form-control" onkeyup="javascript:return contar(this,40)" name="FANTASIA" id="FANTASIA" value="<?php echo (isset($cadastro[0]->FANTASIA)) ? $cadastro[0]->FANTASIA : ''; ?>">
                </div>

                <div class="form-group ENDERECO_DIV">
                  <label for="label_campo">Nome do logradouro</label>
                  <input type="text" class="form-control" onkeyup="javascript:return contar(this,50)" name="ENDERECO" id="ENDERECO" value="<?php echo (isset($cadastro[0]->ENDERECO)) ? $cadastro[0]->ENDERECO : ''; ?>">
                </div>

                <div class="form-group NUMERO_DIV">
                  <label for="label_campo">Número</label>
                  <input type="text" class="form-control" onkeyup="javascript:return contar(this,5)" name="NUMERO" id="NUMERO" value="<?php echo (isset($cadastro[0]->NUMERO)) ? $cadastro[0]->NUMERO : ''; ?>">
                </div>

                <div class="form-group COMPLEM_DIV">
                  <label for="label_campo">Complemento</label>
                  <input type="text" class="form-control" onkeyup="javascript:return contar(this,20)" name="COMPLEM" id="COMPLEM" value="<?php echo (isset($cadastro[0]->COMPLEM)) ? $cadastro[0]->COMPLEM : ''; ?>">
                </div>

                <div class="form-group BAIRRO_DIV">
                  <label for="label_campo">Bairro</label>
                  <input type="text" class="form-control" onkeyup="javascript:return contar(this,30)" name="BAIRRO" id="BAIRRO" value="<?php echo (isset($cadastro[0]->BAIRRO)) ? $cadastro[0]->BAIRRO : ''; ?>">
                </div>

                <div class="form-group CIDADE_DIV">
                  <label for="label_campo">Cidade</label>
                  <input type="text" class="form-control" onkeyup="javascript:return contar(this,30)" name="CIDADE" id="CIDADE" value="<?php echo (isset($cadastro[0]->CIDADE)) ? $cadastro[0]->CIDADE : ''; ?>">
                </div>

                <div class="form-group CEP_DIV">
                  <label for="label_campo">Cep</label>
                  <input type="text" class="form-control" onkeyup="javascript:return contar(this,8)" name="CEP" id="CEP" value="<?php echo (isset($cadastro[0]->CEP)) ? $cadastro[0]->CEP : ''; ?>">
                </div>

                <div class="form-group ESTADO_DIV">
                  <label for="label_campo">Estado</label>
                  <select  class="form-control" id="ESTADO" name="ESTADO">
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'AC') ? 'selected=selected' : ''; ?> value="AC">Acre</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'AL') ? 'selected=selected' : ''; ?>  value="AL">Alagoas</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'AP') ? 'selected=selected' : ''; ?>  value="AP">Amapá</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'AM') ? 'selected=selected' : ''; ?>  value="AM">Amazonas</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'BA') ? 'selected=selected' : ''; ?>  value="BA">Bahia</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'CE') ? 'selected=selected' : ''; ?>  value="CE">Ceará</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'DF') ? 'selected=selected' : ''; ?>  value="DF">Distrito Federal</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'ES') ? 'selected=selected' : ''; ?>  value="ES">Espírito Santo</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'GO') ? 'selected=selected' : ''; ?>  value="GO">Goiás</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'MA') ? 'selected=selected' : ''; ?>  value="MA">Maranhão</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'MT') ? 'selected=selected' : ''; ?>  value="MT">Mato Grosso</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'MS') ? 'selected=selected' : ''; ?> value="MS">Mato Grosso do Sul</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'MG') ? 'selected=selected' : ''; ?>  value="MG">Minas Gerais</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'PA') ? 'selected=selected' : ''; ?>  value="PA">Pará</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'PB') ? 'selected=selected' : ''; ?>  value="PB">Paraíba</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'PR') ? 'selected=selected' : ''; ?>  value="PR">Paraná</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'PE') ? 'selected=selected' : ''; ?>  value="PE">Pernambuco</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'PI') ? 'selected=selected' : ''; ?>  value="PI">Piauí</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'RJ') ? 'selected=selected' : ''; ?>  value="RJ">Rio de Janeiro</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'RN') ? 'selected=selected' : ''; ?>  value="RN">Rio Grande do Norte</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'RS') ? 'selected=selected' : ''; ?>  value="RS">Rio Grande do Sul</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'RO') ? 'selected=selected' : ''; ?>  value="RO">Rondônia</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'RR') ? 'selected=selected' : ''; ?>  value="RR">Roraima</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'SC') ? 'selected=selected' : ''; ?>  value="SC">Santa Catarina</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'SP') ? 'selected=selected' : ''; ?>  value="SP">São Paulo</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'SE') ? 'selected=selected' : ''; ?>  value="SE">Sergipe</option>
                    <option <?php echo (isset($cadastro[0]->ESTADO) && $cadastro[0]->ESTADO == 'TO') ? 'selected=selected' : ''; ?>  value="TO">Tocantins</option>
                  </select>
                </div>

                <div class="form-group DDD_DIV">
                  <label for="label_campo">DDD</label>
                  <input type="text" onkeypress="javascript:return isNumber(event)" onkeyup="javascript:return contar(this,2)" class="form-control" name="DDD" id="DDD" value="<?php echo (isset($cadastro[0]->DDD)) ? $cadastro[0]->DDD : ''; ?>">
                </div>

                <div class="form-group FONE1_DIV">
                  <label for="label_campo">Telefone de Contato</label>
                  <input type="text" onkeypress="javascript:return isNumber(event)" onkeyup="javascript:return contar(this,15)"  class="form-control" name="FONE1" id="FONE1" value="<?php echo (isset($cadastro[0]->FONE1)) ? $cadastro[0]->FONE1 : ''; ?>">
                </div>

                <div class="form-group FONE2_DIV">
                  <label for="label_campo">Telefone de Contato 2</label>
                  <input type="text" onkeypress="javascript:return isNumber(event)" onkeyup="javascript:return contar(this,15)"  class="form-control" name="FONE2" id="FONE2" value="<?php echo (isset($cadastro[0]->FONE2)) ? $cadastro[0]->FONE2 : ''; ?>">
                </div>

                <div class="form-group EMAIL_DIV">
                  <label for="label_campo">E-mail</label>
                  <input type="text" class="form-control" onkeyup="javascript:return contar(this,50)"  name="EMAIL" id="EMAIL" value="<?php echo (isset($cadastro[0]->EMAIL)) ? $cadastro[0]->EMAIL : ''; ?>">
                </div>

                <div class="form-group NOMERESP_DIV">
                  <label for="label_campo">Nome do responsável</label>
                  <input type="text" class="form-control" onkeyup="javascript:return contar(this,50)" name="NOMERESP" id="NOMERESP" value="<?php echo (isset($cadastro[0]->NOMERESP)) ? $cadastro[0]->NOMERESP : ''; ?>">
                </div>

                <div class="form-group CPFRESP_DIV">
                  <label for="label_campo">CPF do responsável</label>
                  <input type="text" class="form-control"  onkeypress="javascript:return isNumber(event)" onkeyup="javascript:return contar(this,11)"  name="CPFRESP" id="CPFRESP" value="<?php echo (isset($cadastro[0]->CPFRESP)) ? $cadastro[0]->CPFRESP : ''; ?>">
                </div>

                <div class="form-group CONTATO_DIV">
                  <label for="label_campo">Nome do contato na Loja</label>
                  <input type="text" class="form-control"  onkeyup="javascript:return contar(this,20)"  name="CONTATO" id="CONTATO" value="<?php echo (isset($cadastro[0]->CONTATO)) ? $cadastro[0]->CONTATO : ''; ?>">
                </div>

                <div class="form-group NOMECONTADOR_DIV">
                  <label for="label_campo">Nome do contador responsável da loja</label>
                  <input type="text" class="form-control"  onkeyup="javascript:return contar(this,50)" name="NOMECONTADOR" id="NOMECONTADOR" value="<?php echo (isset($cadastro[0]->NOMECONTADOR)) ? $cadastro[0]->NOMECONTADOR : ''; ?>">
                </div>

                <div class="form-group FONECONTADOR_DIV">
                  <label for="label_campo">Telefone do contador responsável da loja</label>
                  <input type="text" class="form-control"  onkeypress="javascript:return isNumber(event)" onkeyup="javascript:return contar(this,20)" name="FONECONTADOR" id="FONECONTADOR" value="<?php echo (isset($cadastro[0]->FONECONTADOR)) ? $cadastro[0]->FONECONTADOR : ''; ?>">
                </div>

                <div class="form-group EMAILCONTADOR_DIV">
                  <label for="label_campo">E-mail do contador responsável da loja</label>
                  <input type="text" class="form-control" onkeyup="javascript:return contar(this,50)" name="EMAILCONTADOR" id="EMAILCONTADOR" value="<?php echo (isset($cadastro[0]->EMAILCONTADOR)) ? $cadastro[0]->EMAILCONTADOR : ''; ?>">
                </div>

                <div class="form-group DTESTOQUE_DIV">
                  <label for="label_campo">Data da contagem do estoque</label>
                  <input type="date" class="form-control" onkeyup="javascript:return contar(this,8)"  name="DTESTOQUE" id="DTESTOQUE" value="<?php echo (isset($cadastro[0]->DTESTOQUE)) ? $cadastro[0]->DTESTOQUE : ''; ?>">
                </div>

                <h3> Campos para preenchimento do contador </h3>

                <div class="form-group CRTDIGINST_DIV">
                  <label for="label_campo">Certificado digital está istalado?</label>
                  <select class="form-control" name="CRTDIGINST" id="CRTDIGINST">
                    <option  <?php echo (isset($cadastro[0]->CRTDIGINST) && $cadastro[0]->CRTDIGINST == 0) ? 'selected=selected' : ''; ?> value="0"></option>
                    <option  <?php echo (isset($cadastro[0]->CRTDIGINST) && $cadastro[0]->CRTDIGINST == 1) ? 'selected=selected' : ''; ?>  value="1">Sim</option>
                    <option  <?php echo (isset($cadastro[0]->CRTDIGINST) && $cadastro[0]->CRTDIGINST == 2) ? 'selected=selected' : ''; ?>  value="2">Não</option>
                  </select>
                </div>

                <div class="form-group CRTDIGTIPO_DIV">
                  <label for="label_campo">Tipo do certificado digital</label>
                  <select class="form-control" name="CRTDIGTIPO" id="CRTDIGTIPO">
                    <option <?php echo (isset($cadastro[0]->CRTDIGTIPO) && $cadastro[0]->CRTDIGTIPO == 0) ? 'selected=selected' : ''; ?> value="0"></option>
                    <option <?php echo (isset($cadastro[0]->CRTDIGTIPO) && $cadastro[0]->CRTDIGTIPO == 1) ? 'selected=selected' : ''; ?> value="1">A1</option>
                    <option <?php echo (isset($cadastro[0]->CRTDIGTIPO) && $cadastro[0]->CRTDIGTIPO == 2) ? 'selected=selected' : ''; ?> value="2">A3</option>
                  </select>
                </div>

                <div class="form-group DTHABNFE_DIV">
                  <label for="label_campo">Data em que o estabelecimento foi habilitado no SEFAZ</label>
                  <input type="date" class="form-control" onkeyup="javascript:return contar(this,8)"  name="DTHABNFE" id="DTHABNFE" value="<?php echo (isset($cadastro[0]->DTHABNFE)) ? $cadastro[0]->DTHABNFE : ''; ?>">
                </div>

                <div class="form-group REGTRIB_DIV">
                  <label for="label_campo">Regime Tributário da Loja</label>
                  <select class="form-control" name="REGTRIB" id="REGTRIB">
                    <option <?php echo (isset($cadastro[0]->REGTRIB) && $cadastro[0]->REGTRIB == 0) ? 'selected=selected' : ''; ?> value="0"></option>
                    <option <?php echo (isset($cadastro[0]->REGTRIB) && $cadastro[0]->REGTRIB == 1) ? 'selected=selected' : ''; ?> value="1">Simples Nacional</option>
                    <option <?php echo (isset($cadastro[0]->REGTRIB) && $cadastro[0]->REGTRIB == 2) ? 'selected=selected' : ''; ?> value="2">Simples Nacional Excesso de Sub-Limite</option>
                    <option <?php echo (isset($cadastro[0]->REGTRIB) && $cadastro[0]->REGTRIB == 3) ? 'selected=selected' : ''; ?> value="3">Regime Normal</option>
                  </select>
                </div>

                <div class="form-group ALIQSN_DIV">
                  <label for="label_campo">Alíquota de ICMS (Formato decimal, exemplo 18.0)</label>
                  <input type="text" class="form-control" onkeypress="return(moeda(this,'.','.',event))" onkeyup="javascript:return contar(this,5)"  name="ALIQSN" id="ALIQSN" value="<?php echo (isset($cadastro[0]->ALIQSN)) ? $cadastro[0]->ALIQSN : ''; ?>">
                </div>

                <div class="form-group INDPDVDOC_DIV">
                  <label for="label_campo">Tipo de documento fiscal</label>
                  <select class="form-control" name="INDPDVDOC" id="INDPDVDOC">
                    <option <?php echo (isset($cadastro[0]->INDPDVDOC) && $cadastro[0]->INDPDVDOC == 0) ? 'selected=selected' : ''; ?> value="0"></option>
                    <option <?php echo (isset($cadastro[0]->INDPDVDOC) && $cadastro[0]->INDPDVDOC == 1) ? 'selected=selected' : ''; ?> value="1">Cupom Fiscal( Impressora Fiscal )</option>
                    <option <?php echo (isset($cadastro[0]->INDPDVDOC) && $cadastro[0]->INDPDVDOC == 2) ? 'selected=selected' : ''; ?> value="2">NFC-e</option>
                    <option <?php echo (isset($cadastro[0]->INDPDVDOC) && $cadastro[0]->INDPDVDOC == 3) ? 'selected=selected' : ''; ?> value="3">CF-e (S@T-SP)</option>
                    <option <?php echo (isset($cadastro[0]->INDPDVDOC) && $cadastro[0]->INDPDVDOC == 4) ? 'selected=selected' : ''; ?> value="4">CF-e (MFE-CE)</option>
                  </select>
                </div>

                <?php
                $show_imp = 'hide';
                if( isset($cadastro[0]->INDPDVDOC) && in_array($cadastro[0]->INDPDVDOC,array(2,3,4)) ){

                  $show_imp = 'show';

                } ?>

                <div class="form-group NOMEIMPNFISC_DIV <?php echo $show_imp; ?>">
                  <label for="label_campo">Nome da Impressora NÃO fiscal instalada no Windows.</label>
                  <input type="text" class="form-control" onkeyup="javascript:return contar(this,30)" name="NOMEIMPNFISC" id="NOMEIMPNFISC" value="<?php echo (isset($cadastro[0]->NOMEIMPNFISC)) ? $cadastro[0]->NOMEIMPNFISC : ''; ?>">
                </div>


                <?php
                $show_MARCAEF = 'hide';
                if( isset($cadastro[0]->INDPDVDOC) && in_array($cadastro[0]->INDPDVDOC,array(1)) ){

                  $show_MARCAEF = 'show';

                } ?>


                <div class="form-group MARCAECF_DIV <?php echo $show_MARCAEF; ?>">
                  <label for="label_campo">Marca do ECF</label>
                  <select class="form-control" name="MARCAECF" id="MARCAECF">
                    <option <?php echo (isset($cadastro[0]->MARCAECF) && $cadastro[0]->MARCAECF == 0) ? 'selected=selected' : ''; ?> value="0"></option>
                    <option <?php echo (isset($cadastro[0]->MARCAECF) && $cadastro[0]->MARCAECF == 1) ? 'selected=selected' : ''; ?> value="1">Bematech</option>
                    <option <?php echo (isset($cadastro[0]->MARCAECF) && $cadastro[0]->MARCAECF == 2) ? 'selected=selected' : ''; ?> value="2">Daruma</option>
                    <option <?php echo (isset($cadastro[0]->MARCAECF) && $cadastro[0]->MARCAECF == 3) ? 'selected=selected' : ''; ?> value="3">Sweda</option>
                    <option <?php echo (isset($cadastro[0]->MARCAECF) && $cadastro[0]->MARCAECF == 4) ? 'selected=selected' : ''; ?> value="4">Epson</option>
                  </select>
                </div>


                <?php
                $show_MODELOECF = 'hide';
                if( isset($cadastro[0]->INDPDVDOC) && in_array($cadastro[0]->INDPDVDOC,array(1)) ){

                  $show_MODELOECF = 'show';

                } ?>


                <div class="form-group MODELOECF_DIV <?php echo $show_MODELOECF; ?>">
                  <label for="label_campo">Descrição do modelo do ECF.</label>
                  <input type="text" class="form-control" onkeyup="javascript:return contar(this,15)" name="MODELOECF" id="MODELOECF" value="<?php echo (isset($cadastro[0]->MODELOECF)) ? $cadastro[0]->MODELOECF : ''; ?>">
                </div>

                <div class="form-group hide">
                  <label for="label_campo">Código Nacional do ECF</label>
                  <input type="text" class="form-control" onkeyup="javascript:return contar(this,6)" name="CODNACECF" id="CODNACECF" value="<?php echo (isset($cadastro[0]->CODNACECF)) ? $cadastro[0]->CODNACECF : ''; ?>">
                </div>


                <?php
                $show_DTHABNFCE = 'hide';
                if( isset($cadastro[0]->INDPDVDOC) && in_array($cadastro[0]->INDPDVDOC,array(2)) ){

                  $show_DTHABNFCE = 'show';

                } ?>

                <div class="form-group DTHABNFCE_DIV <?php echo $show_DTHABNFCE; ?>">
                  <label for="label_campo">Data da habilitação no SEFAZ para emissão de Nota Fiscal de Consumidor Eletrônica</label>
                  <input type="date" class="form-control" name="DTHABNFCE" id="DTHABNFCE" value="<?php echo (isset($cadastro[0]->DTHABNFCE)) ? $cadastro[0]->DTHABNFCE : ''; ?>">
                </div>

                <div class="form-group NFESERIE_DIV">
                  <label for="label_campo">Série para emissão da NF-e</label>
                  <input type="text" class="form-control"  onkeypress="javascript:return isNumber(event)" onkeyup="javascript:return contar(this,3)" name="NFESERIE" id="NFESERIE" value="<?php echo (isset($cadastro[0]->NFESERIE)) ? $cadastro[0]->NFESERIE : ''; ?>">
                </div>

                <div class="form-group NFENRO_DIV">
                  <label for="label_campo">Número da última NF-e emitida.</label>
                  <input type="text" class="form-control"  onkeypress="javascript:return isNumber(event)" onkeyup="javascript:return contar(this,8)" name="NFENRO" id="NFENRO" value="<?php echo (isset($cadastro[0]->NFENRO)) ? $cadastro[0]->NFENRO : ''; ?>">
                </div>


                <?php
                $show_SAT = 'hide';
                if(isset($cadastro[0]->INDPDVDOC) && in_array($cadastro[0]->INDPDVDOC,array(3,4)) ){

                  $show_SAT = 'show';

                } ?>

                <div class="form-group CODATIVACAOSAT_DIV  <?php echo $show_SAT; ?>">
                  <label for="label_campo">Código de ativação do SAT</label>
                  <input type="text" class="form-control" onkeyup="javascript:return contar(this,32)" name="CODATIVACAOSAT" id="CODATIVACAOSAT" value="<?php echo (isset($cadastro[0]->CODATIVACAOSAT)) ? $cadastro[0]->CODATIVACAOSAT : ''; ?>">
                </div>

                <div class="form-group hide">
                  <label for="label_campo">Chave Vinculada</label>
                  <input type="text" class="form-control" onkeyup="javascript:return contar(this,344)" name="CHAVEVINCSH" id="CHAVEVINCSH" value="<?php echo (isset($cadastro[0]->CHAVEVINCSH)) ? $cadastro[0]->CHAVEVINCSH : ''; ?>">
                </div>

                <?php
                $show_NFCESERIE = 'hide';
                $show_NFCENRO =  'hide';
                $show_IDTOKEN = 'hide';
                $show_CSC = 'hide';
                if( isset($cadastro[0]->INDPDVDOC) && in_array($cadastro[0]->INDPDVDOC,array(2)) ){

                  $show_NFCESERIE = 'show';
                  $show_NFCENRO =  'show';
                  $show_IDTOKEN = 'show';
                  $show_CSC = 'show';

                } ?>

                <div class="form-group NFCESERIE_DIV  <?php echo $show_NFCESERIE; ?>">
                  <label for="label_campo">Série para emissão da NFC-e</label>
                  <input type="text" class="form-control"  onkeypress="javascript:return isNumber(event)" onkeyup="javascript:return contar(this,3)" name="NFCESERIE" id="NFCESERIE" value="<?php echo (isset($cadastro[0]->NFCESERIE)) ? $cadastro[0]->NFCESERIE : ''; ?>">
                </div>

                <div class="form-group NFCENRO_DIV  <?php echo $show_NFCENRO; ?>">
                  <label for="label_campo">Número da última NFC-e emitida.</label>
                  <input type="text" class="form-control"  onkeypress="javascript:return isNumber(event)" onkeyup="javascript:return contar(this,8)" name="NFCENRO" id="NFCENRO" value="<?php echo (isset($cadastro[0]->NFCENRO)) ? $cadastro[0]->NFCENRO : ''; ?>">
                </div>

                <div class="form-group IDTOKEN_DIV  <?php echo $show_IDTOKEN; ?>">
                  <label for="label_campo">Token fornecido pela SEFAZ do Estado para emissão de NFC-e</label>
                  <input type="text" class="form-control" onkeyup="javascript:return contar(this,6)" name="IDTOKEN" id="IDTOKEN" value="<?php echo (isset($cadastro[0]->IDTOKEN)) ? $cadastro[0]->IDTOKEN : ''; ?>">
                </div>

                <div class="form-group CSC_DIV  <?php echo $show_CSC; ?>">
                  <label for="label_campo">Código CSC fornecido pelo SEFAZ do Estado para emissão de NFC-e</label>
                  <input type="text" class="form-control" onkeyup="javascript:return contar(this,36)" name="CSC" id="CSC" value="<?php echo (isset($cadastro[0]->CSC)) ? $cadastro[0]->CSC : ''; ?>">
                </div>

                <?php

                $show_NROAUTUSOECF = 'hide';
                if( isset($cadastro[0]->ESTADO) && in_array($cadastro[0]->ESTADO,array('SC')) ){

                  $show_NROAUTUSOECF = 'show';

                } ?>

                <div class="form-group NROAUTUSOECF_DIV <?php echo $show_NROAUTUSOECF; ?>">
                  <label for="label_campo">Número de autorização do uso de ECF</label>
                  <input type="text" class="form-control" onkeyup="javascript:return contar(this,15)" name="NROAUTUSOECF" id="NROAUTUSOECF" value="<?php echo (isset($cadastro[0]->NROAUTUSOECF)) ? $cadastro[0]->NROAUTUSOECF : ''; ?>">
                </div>

                <div class="form-group TIPOEMINFDEV_DIV">
                  <label for="label_campo">Tipo de emissão de NF de devolução(Trocas)</label>
                  <select class="form-control" name="TIPOEMINFDEV" id="TIPOEMINFDEV">
                    <option <?php echo (isset($cadastro[0]->TIPOEMINFDEV) && $cadastro[0]->TIPOEMINFDEV == 0) ? 'selected=selected' : ''; ?> value="0"></option>
                    <option <?php echo (isset($cadastro[0]->TIPOEMINFDEV) && $cadastro[0]->TIPOEMINFDEV == 1) ? 'selected=selected' : ''; ?> value="1">Emissão contra a loja</option>
                    <option <?php echo (isset($cadastro[0]->TIPOEMINFDEV) && $cadastro[0]->TIPOEMINFDEV == 2) ? 'selected=selected' : ''; ?> value="2">Emissão contra o cliente</option>
                  </select>
                </div>

                <div class="form-group TIPOEMINFEST_DIV">
                  <label for="label_campo">Tipo de emissão de NF de estorno(Vendas canceladas fora do prezo)</label>
                  <select class="form-control" name="TIPOEMINFEST" id="TIPOEMINFEST">
                    <option <?php echo (isset($cadastro[0]->TIPOEMINFEST) && $cadastro[0]->TIPOEMINFEST == 0) ? 'selected=selected' : ''; ?> value="0"></option>
                    <option <?php echo (isset($cadastro[0]->TIPOEMINFEST) && $cadastro[0]->TIPOEMINFEST == 1) ? 'selected=selected' : ''; ?> value="1">Emissão contra a loja</option>
                    <option <?php echo (isset($cadastro[0]->TIPOEMINFEST) && $cadastro[0]->TIPOEMINFEST == 2) ? 'selected=selected' : ''; ?> value="2">Emissão contra o cliente</option>
                  </select>
                </div>

                <?php
                $show_ALIQICMSECF = 'hide';

                if(isset($cadastro[0]->INDPDVDOC) && in_array($cadastro[0]->INDPDVDOC,array(1)) ){

                  $show_ALIQICMSECF = 'show';


                } ?>

                <div class="form-group ALIQICMSECF_DIV  <?php echo $show_ALIQICMSECF; ?>">
                  <label for="label_campo">Alíquota de ICMS para ser praticada no ECF (Formato decimal, exemplo 18.0)</label>
                  <input type="text" class="form-control" onkeypress="return(moeda(this,'.','.',event))"  onkeyup="javascript:return contar(this,5)" name="ALIQICMSECF" id="ALIQICMSECF" value="<?php echo (isset($cadastro[0]->ALIQICMSECF)) ? $cadastro[0]->ALIQICMSECF : ''; ?>">
                </div>


                <?php
                $show_MFE_CHAVACESVLD = 'hide';
                $show_CNPJADQ = 'hide';
                $show_CHAVEREQ = 'hide';
                $show_CODESTADM = 'hide';
                $show_SERIALPOS = 'hide';

                if(isset($cadastro[0]->INDPDVDOC) && in_array($cadastro[0]->INDPDVDOC,array(4)) ){

                  $show_MFE_CHAVACESVLD = 'show';
                  $show_CNPJADQ = 'show';
                  $show_CHAVEREQ = 'show';
                  $show_CODESTADM = 'show';
                  $show_SERIALPOS = 'show';
                } ?>

                <div class="form-group MFE_CHAVACESVLD_DIV <?php echo $show_MFE_CHAVACESVLD; ?>">
                  <label for="label_campo">Chave de acesso do validor fiscal</label>
                  <input type="text" class="form-control"  onkeyup="javascript:return contar(this,36)" name="MFE_CHAVACESVLD" id="MFE_CHAVACESVLD" value="<?php echo (isset($cadastro[0]->MFE_CHAVACESVLD)) ? $cadastro[0]->MFE_CHAVACESVLD : ''; ?>">
                </div>

                <div class="form-group CNPJADQ_DIV <?php echo $show_CNPJADQ; ?>">
                  <label for="label_campo">CNPJ da empresa Adquirente para transações de cartão de crédito e débito</label>
                  <input type="text" class="form-control"  onkeypress="javascript:return isNumber(event)"   onkeyup="javascript:return contar(this,14)" name="CNPJADQ" id="CNPJADQ" value="<?php echo (isset($cadastro[0]->CNPJADQ)) ? $cadastro[0]->CNPJADQ : ''; ?>">
                </div>

                <div class="form-group CHAVEREQ_DIV <?php echo $show_CHAVEREQ; ?>">
                  <label for="label_campo">Chave de requisição ao validador Fiscal</label>
                  <input type="text" class="form-control"  onkeyup="javascript:return contar(this,36)" name="CHAVEREQ" id="CHAVEREQ" value="<?php echo (isset($cadastro[0]->CHAVEREQ)) ? $cadastro[0]->CHAVEREQ : ''; ?>">
                </div>

                <div class="form-group CODESTADM_DIV <?php echo $show_CODESTADM; ?>">
                  <label for="label_campo">Código do estabelecimento adquirente</label>
                  <input type="text" class="form-control"  onkeyup="javascript:return contar(this,20)" name="CODESTADM" id="CODESTADM" value="<?php echo (isset($cadastro[0]->CODESTADM)) ? $cadastro[0]->CODESTADM : ''; ?>">
                </div>

                <div class="form-group SERIALPOS_DIV <?php echo $show_SERIALPOS; ?>">
                  <label for="label_campo">Número Serial do POS</label>
                  <input type="text" class="form-control"  onkeyup="javascript:return contar(this,10)" name="SERIALPOS" id="SERIALPOS" value="<?php echo (isset($cadastro[0]->SERIALPOS)) ? $cadastro[0]->SERIALPOS : ''; ?>">
                </div>

                <div class="form-group">
                  <label for="label_campo">Mensagens adicionais impressas no cupom de Venda, até 8 linhas com 40 caracteres cada.</label>
                  <input type="text" style="margin:3px" class="form-control"  onkeyup="javascript:return contar(this,320)" name="MSGCUPOMVENDA" id="MSGCUPOMVENDA" value="<?php echo (isset($cadastro[0]->MSGCUPOMVENDA)) ? $cadastro[0]->MSGCUPOMVENDA : ''; ?>">
                </div>

                <div class="form-group INDTEF_DIV">
                  <label for="label_campo">Utiliza TEF</label>
                  <select class="form-control" name="INDTEF" id="INDTEF">
                    <option <?php echo (isset($cadastro[0]->INDTEF) && $cadastro[0]->INDTEF == 0) ? 'selected=selected' : ''; ?> value="0"></option>
                    <option <?php echo (isset($cadastro[0]->INDTEF) && $cadastro[0]->INDTEF == 1) ? 'selected=selected' : ''; ?> value="1">Não utiliza TEF</option>
                    <option <?php echo (isset($cadastro[0]->INDTEF) && $cadastro[0]->INDTEF == 2) ? 'selected=selected' : ''; ?> value="2">TEF Pay Go</option>
                    <option <?php echo (isset($cadastro[0]->INDTEF) && $cadastro[0]->INDTEF == 3) ? 'selected=selected' : ''; ?> value="3">Sitef</option>
                  </select>
                </div>

                <div class="form-group NFE_XMLEM_FMENV_DIV">
                  <label for="label_campo">Forma de envio de e-mail quando emitida NF-e</label>
                  <select class="form-control" name="NFE_XMLEM_FMENV" id="NFE_XMLEM_FMENV">
                    <option <?php echo (isset($cadastro[0]->NFE_XMLEM_FMENV) && $cadastro[0]->NFE_XMLEM_FMENV == 0) ? 'selected=selected' : ''; ?> value="0"></option>
                    <option <?php echo (isset($cadastro[0]->NFE_XMLEM_FMENV) && $cadastro[0]->NFE_XMLEM_FMENV == 1) ? 'selected=selected' : ''; ?> value="1">Servidor de e-mail externo (exemplo: Gmail).</option>
                    <option <?php echo (isset($cadastro[0]->NFE_XMLEM_FMENV) && $cadastro[0]->NFE_XMLEM_FMENV == 2) ? 'selected=selected' : ''; ?> value="2">Por aplicativo local de e-mail (exemplo: Outlook)</option>
                  </select>
                </div>

                <?php
                $show_NFE_XMLEM_SRVEML = 'hide';
                $show_NFE_XMLEM_SRVPRT = 'hide';


                if(isset($cadastro[0]->NFE_XMLEM_FMENV) && in_array($cadastro[0]->NFE_XMLEM_FMENV,array(1)) ){

                  $show_NFE_XMLEM_SRVEML = 'show';
                  $show_NFE_XMLEM_SRVPRT = 'show';
                } ?>

                <div class="form-group  NFE_XMLEM_SRVEML_DIV <?php echo $show_NFE_XMLEM_SRVEML; ?>">
                  <label for="label_campo">Servidor de e-mail</label>
                  <input type="text" class="form-control"  onkeyup="javascript:return contar(this,50)" name="NFE_XMLEM_SRVEML" id="NFE_XMLEM_SRVEML" value="<?php echo (isset($cadastro[0]->NFE_XMLEM_SRVEML)) ? $cadastro[0]->NFE_XMLEM_SRVEML : ''; ?>">
                </div>

                <div class="form-group NFE_XMLEM_SRVPRT_DIV <?php echo $show_NFE_XMLEM_SRVPRT; ?>">
                  <label for="label_campo">Porta do servidor de e-mail</label>
                  <input type="text" class="form-control"  onkeypress="javascript:return isNumber(event)"  onkeyup="javascript:return contar(this,6)" name="NFE_XMLEM_SRVPRT" id="NFE_XMLEM_SRVPRT" value="<?php echo (isset($cadastro[0]->NFE_XMLEM_SRVPRT)) ? $cadastro[0]->NFE_XMLEM_SRVPRT : ''; ?>">
                </div>

                <div class="form-group NFE_XMLEM_SRVUSR_DIV">
                  <label for="label_campo">Nome do usuário da conta de e-mail</label>
                  <input type="text" class="form-control"  onkeyup="javascript:return contar(this,50)" name="NFE_XMLEM_SRVUSR" id="NFE_XMLEM_SRVUSR" value="<?php echo (isset($cadastro[0]->NFE_XMLEM_SRVUSR)) ? $cadastro[0]->NFE_XMLEM_SRVUSR : ''; ?>">
                </div>

                <div class="form-group NFE_XMLEM_SRVPWD_DIV">
                  <label for="label_campo">Senha da conta de e-mail</label>
                  <input type="text" class="form-control"  onkeyup="javascript:return contar(this,50)" name="NFE_XMLEM_SRVPWD" id="NFE_XMLEM_SRVPWD" value="<?php echo (isset($cadastro[0]->NFE_XMLEM_SRVPWD)) ? $cadastro[0]->NFE_XMLEM_SRVPWD : ''; ?>">
                </div>


              </form>

            </div>
          </div>
        </div>
      </div>

    </div>

  </div>
</div>
<br>
<br>
<br>
<br>
<br>

<div class="navbar2">
  <table style="float:right">
    <tr>
      <td style="color:white !important">
        <?php if( isset($cadastro[0]->STATUS) && $cadastro[0]->STATUS == 'C'  ) {
          echo (isset($cadastro[0]->DT_ULTIMA_ATUALIZACAO) ) ? 'Finalizado em:  ['.$cadastro[0]->DT_ULTIMA_ATUALIZACAO.']' : '';
        }
        else
        {
          echo (isset($cadastro[0]->DT_ULTIMA_ATUALIZACAO) ) ? 'Última atualização ['.$cadastro[0]->DT_ULTIMA_ATUALIZACAO.']' : '';
        }
        ?>
        <td>
          <?php if(!isset($cadastro[0]->STATUS) || $cadastro[0]->STATUS == 'I'  ) { ?>
            <td style="padding:10px">
              <a type="button" id="btn-atualizar" class="btn btn-primary">Atualizar</a>
            </td>
            <td>
              <a type="button" id="btn-finalizar" class="btn btn-success">Finalizar</a>
            </td>
          <?php } ?>
        </tr>
      </table>
    </div>


    <script>

    $(document).ready(function() {



      $( "#INDPDVDOC" ).change(function() {
        $value = $(this).val();

        console.log($value);


        if($value == 0 || $value == 1)
        {
          $(".NOMEIMPNFISC_DIV").addClass('hide');
          $(".NOMEIMPNFISC_DIV").removeClass('show');

          $(".MARCAECF_DIV").addClass('show');
          $(".MARCAECF_DIV").removeClass('hide');

          $(".MODELOECF_DIV").addClass('show');
          $(".MODELOECF_DIV").removeClass('hide');

          $(".ALIQICMSECF_DIV").addClass('show');
          $(".ALIQICMSECF_DIV").removeClass('hide');


          $("#NOMEIMPNFISC").val('');

        }
        else
        {
          $(".NOMEIMPNFISC_DIV").addClass('show');
          $(".NOMEIMPNFISC_DIV").removeClass('hide');

          $(".MARCAECF_DIV").addClass('hide');
          $(".MARCAECF_DIV").removeClass('show');
          $("#MARCAECF").val(0);

          $(".MODELOECF_DIV").addClass('hide');
          $(".MODELOECF_DIV").removeClass('show');
          $("#MODELOECF").val('');

          $(".ALIQICMSECF_DIV").addClass('hide');
          $(".ALIQICMSECF_DIV").removeClass('show');
          $("#ALIQICMSECF").val('');

        }

        if( $value == 2 )
        {
          $(".DTHABNFCE_DIV").addClass('show');
          $(".DTHABNFCE_DIV").removeClass('hide');

          $(".NFCESERIE_DIV").addClass('show');
          $(".NFCESERIE_DIV").removeClass('hide');

          $(".NFCENRO_DIV").addClass('show');
          $(".NFCENRO_DIV").removeClass('hide');

          $(".IDTOKEN_DIV").addClass('show');
          $(".IDTOKEN_DIV").removeClass('hide');

          $(".CSC_DIV").addClass('show');
          $(".CSC_DIV").removeClass('hide');

        }
        else
        {
          $(".DTHABNFCE_DIV").addClass('hide');
          $(".DTHABNFCE_DIV").removeClass('show');
          $("#DTHABNFCE").val('');

          $(".NFCESERIE_DIV").addClass('hide');
          $(".NFCESERIE_DIV").removeClass('show');
          $("#NFCESERIE").val('');

          $(".NFCENRO_DIV").addClass('hide');
          $(".NFCENRO_DIV").removeClass('show');
          $("#NFCENRO").val('');

          $(".IDTOKEN_DIV").addClass('hide');
          $(".IDTOKEN_DIV").removeClass('show');
          $("#IDTOKEN").val('');

          $(".CSC_DIV").addClass('hide');
          $(".CSC_DIV").removeClass('show');
          $("#CSC").removeClass('show');

        }

        if( $value == 4 )
        {
          $(".MFE_CHAVACESVLD_DIV").addClass('show');
          $(".MFE_CHAVACESVLD_DIV").removeClass('hide');

          $(".CNPJADQ_DIV").addClass('show');
          $(".CNPJADQ_DIV").removeClass('hide');

          $(".CHAVEREQ_DIV").addClass('show');
          $(".CHAVEREQ_DIV").removeClass('hide');

          $(".CODESTADM_DIV").addClass('show');
          $(".CODESTADM_DIV").removeClass('hide');

          $(".SERIALPOS_DIV").addClass('show');
          $(".SERIALPOS_DIV").removeClass('hide');

        }
        else
        {
          $(".MFE_CHAVACESVLD_DIV").addClass('hide');
          $(".MFE_CHAVACESVLD_DIV").removeClass('show');
          $("#MFE_CHAVACESVLD").val('');

          $(".CNPJADQ_DIV").addClass('hide');
          $(".CNPJADQ_DIV").removeClass('show');
          $("#CNPJADQ").val('');

          $(".CHAVEREQ_DIV").addClass('hide');
          $(".CHAVEREQ_DIV").removeClass('show');
          $("#CHAVEREQ").val('');

          $(".CODESTADM_DIV").addClass('hide');
          $(".CODESTADM_DIV").removeClass('show');
          $("#CODESTADM").val('');

          $(".SERIALPOS_DIV").addClass('hide');
          $(".SERIALPOS_DIV").removeClass('show');
          $("#SERIALPOS").val('');

        }

        if( $value == 3 || $value == 4 )
        {
          $(".CODATIVACAOSAT_DIV").addClass('show');
          $(".CODATIVACAOSAT_DIV").removeClass('hide');

        }
        else
        {
          $(".CODATIVACAOSAT_DIV").addClass('hide');
          $(".CODATIVACAOSAT_DIV").removeClass('show');
          $("#CODATIVACAOSAT").val('');
        }

      });

      $( "#ESTADO" ).change(function() {

        $value = $(this).val();

        if($value == 'SC')
        {
          $(".NROAUTUSOECF_DIV").addClass('show');
          $(".NROAUTUSOECF_DIV").removeClass('hide');
        }
        else
        {
          $(".NROAUTUSOECF_DIV").addClass('hide');
          $(".NROAUTUSOECF_DIV").removeClass('show');
          $("#NROAUTUSOECF").val('');
        }

      })

      $( "#NFE_XMLEM_FMENV" ).change(function() {

        $value = $(this).val();

        if($value == 1)
        {
          $(".NFE_XMLEM_SRVEML_DIV").addClass('show');
          $(".NFE_XMLEM_SRVEML_DIV").removeClass('hide');

          $(".NFE_XMLEM_SRVPRT_DIV").addClass('show');
          $(".NFE_XMLEM_SRVPRT_DIV").removeClass('hide');
        }
        else
        {

          $(".NFE_XMLEM_SRVEML_DIV").addClass('hide');
          $(".NFE_XMLEM_SRVEML_DIV").removeClass('show');
          $("#NFE_XMLEM_SRVEML").val('');

          $(".NFE_XMLEM_SRVPRT_DIV").addClass('hide');
          $(".NFE_XMLEM_SRVPRT_DIV").removeClass('show');
          $("#NFE_XMLEM_SRVPRT").val('');
        }

      })


      $('#btn-atualizar').click(function(){

        //todas as divs de campos com fundo branco
        $('.form-group').each(function() {
          $(this).css("background-color", "white");
        });

        swal({
          title: 'Aguarde',
          html: 'Aguarde.',
          showCancelButton: false,
          showConfirmButton: false
        });

        $btn = $(this);
        $area = $("#CodArea").val();

        $form = $('#formulario').serialize();

        $btn.attr('disabled',true);

        $url = "{{ URL::to('/formulario/atualizar/') }}";

        $.ajax({
          url: $url,
          type: 'POST',
          data: {"_token":"{{ csrf_token() }}",
          form:$form
        },
        datatype : "application/json",
        success: function(dataReturn, textStatus, xhr)
        {

          if(xhr.status == 200)
          {
            window.location.href = "{{ URL::to('/formulario/loja/') }}"+'/'+$area;
            location.reload();
          }

          if(xhr.status == 202)
          {
            $ret = JSON.parse(JSON.stringify(dataReturn));

            Object.keys($ret).forEach(function(key, value){

              swal({
                title: "Campos obrigatórios",
                text: 'Informe os campos destacados de amarelo.',
                html: true
              });

              $("."+key+"_DIV").css("background-color", "yellow");

            });


            return false;

          }

        },
        error: function(xhr, status, error) {

          console.log(xhr);

          swal({
            title: "Erro",
            text: xhr.responseText,
            html: true,
            customClass: 'swal-wide'
          });

          $btn.attr('disabled',false);
          return false;
        }
      }).done(function() {
        $btn.attr('disabled',false);
      });

    })



    $('#btn-finalizar').click(function(){

      //todas as divs de campos com fundo branco
      $('.form-group').each(function() {
        $(this).css("background-color", "white");
      });

      swal({
        title: 'Aguarde',
        html: 'Aguarde.',
        showCancelButton: false,
        showConfirmButton: false
      });

      $btn = $(this);
      $area = $("#CodArea").val();

      $form = $('#formulario').serialize();

      $btn.attr('disabled',true);

      $url = "{{ URL::to('/formulario/finalizar/') }}";

      $.ajax({
        url: $url,
        type: 'POST',
        data: {"_token":"{{ csrf_token() }}",
        form:$form
      },
      datatype : "application/json",
      success: function(dataReturn, textStatus, xhr)
      {

        if(xhr.status == 200)
        {
          window.location.href = "{{ URL::to('/formulario/loja/') }}"+'/'+$area;
          location.reload();
        }

        if(xhr.status == 202)
        {
          $ret = JSON.parse(JSON.stringify(dataReturn));

          Object.keys($ret).forEach(function(key, value){

            swal({
              title: "Campos obrigatórios",
              text: 'Informe os campos destacados de amarelo.',
              html: true
            });

            $("."+key+"_DIV").css("background-color", "yellow");

          });

          console.log(dataReturn);

          return false;

        }

      },
      error: function(xhr, status, error) {

        console.log(xhr);

        swal({
          title: "Erro",
          text: xhr.responseText,
          html: true,
          customClass: 'swal-wide'
        });

        $btn.attr('disabled',false);
        return false;
      }
    }).done(function() {
      $btn.attr('disabled',false);
    });

  })

});


function moeda(a, e, r, t) {
        let n = ""
        , h = j = 0
        , u = tamanho2 = 0
        , l = ajd2 = ""
        , o = window.Event ? t.which : t.keyCode;
        if (13 == o || 8 == o)
        return !0;
        if (n = String.fromCharCode(o),
        -1 == "0123456789".indexOf(n))
        return !1;
        for (u = a.value.length,
        h = 0; h < u && ("0" == a.value.charAt(h) || a.value.charAt(h) == r); h++)
        ;
        for (l = ""; h < u; h++)
        -1 != "0123456789".indexOf(a.value.charAt(h)) && (l += a.value.charAt(h));
        if (l += n,
        0 == (u = l.length) && (a.value = ""),
        1 == u && (a.value = "0" + r + "0" + l),
        2 == u && (a.value = "0" + r + l),
        u > 2) {
        for (ajd2 = "",
        j = 0,
        h = u - 3; h >= 0; h--)
        3 == j && (ajd2 += e,
        j = 0),
        ajd2 += l.charAt(h),
        j++;
        for (a.value = "",
        tamanho2 = ajd2.length,
        h = tamanho2 - 1; h >= 0; h--)
        a.value += ajd2.charAt(h);
        a.value += r + l.substr(u - 2, u)
        }
        return !1
}



</script>

<style>
.swal-wide{
  width:100% !important;
  left:0px !important,
}
</style>
@endsection
