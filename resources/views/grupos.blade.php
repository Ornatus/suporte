@extends('layouts.menu')

@section('content')

<div class="row" style="margin:10px !important">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">

        <form id="filtro" name="filtro" method="GET">
          {!! csrf_field() !!}
          <table width="auto">
            <td style="padding:5px"> Filtrar grupo:<br>
              <input type="text" id="filtro_grupo" name="filtro_grupo" value="<?php echo $filtro_grupo; ?>" style="margin-bottom:10px"  class="form-control"/>
            </td>
            <td style="padding:5px"><br>
              <a type="button" id="btn-atualizar" class="btn btn-primary" style="margin-bottom:10px">Atualizar</a>
            </td>
            <td style="padding:5px"><br>
              <a type="button" id="btn-adicionar" class="btn btn-primary" style="margin-bottom:10px">Adicionar</a>
            </td>
          </tr>
        </table>
      </form>
    </div>
    <div  width="100%"  style="padding-left:43%;">
      <div style="">
        {{ $grupos->render()}}
      </div>
    </div>
    <div class="panel-body">
      <table class="table table-striped table-responsive" style="font-size: 10pt; font-family: Verdana;">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Grupo</th>
            <th scope="col" style="width:10%">Cor</th>
            <th scope="col" style="width:10%">Editar</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($grupos as $grupo){ ?>
            <tr>
              <td scope="row"><b><?php echo $grupo->grupo_id; ?></b> </td>
              <td><?php echo $grupo->descricao; ?></td>
              <td style="padding: 5px;">
                <div class="rounded" style="width:50% !important;height: 100% !important;background-color:<?php echo $grupo->cor; ?>;color:<?php echo $grupo->cor; ?>;">
                  <img style="<?php echo $grupo->cor; ?>" class="rounded-circle">
                  <h6><?php echo $grupo->cor; ?></h6>
                </div>
              </td>
              <td>
                <a type="button" title="Editar grupo" href="{{ URL::to('/grupo/'.$grupo->grupo_id) }}" class="btn btn-default">
                  <i class="fa fa-bars"></i></a>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
    <div  width="100%"  style="padding-left:43%;">
      <div style="">
        {{ $grupos->render()}}
      </div>
    </div>
  </div>
</div>


<script>
$(document).ready(function() {

  $('#btn-atualizar').click(function(){
    swal({
      title: 'Aguarde',
      html: 'Aguarde.',
      showCancelButton: false,
      showConfirmButton: false
    });

    $("#filtro").submit();

  });

  $("#btn-adicionar").click(function(){

    window.location = "{{ URL::to('/adiciona-grupo/') }}";
  })

});

</script>

<style>

.btn-group
{
  margin-bottom:0px !important;
}

</style>

@endsection
