@extends('layouts.app')

@section('content')

<script
    src="https://code.jquery.com/jquery-2.2.4.min.js"
    integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
    crossorigin="anonymous"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading">Cadastro</div>

            <div class="panel-body">
              <form>
                <div class="form-group">
                  <label for="loja">Loja</label><br>
                  <input id="id" name="id" type="hidden" value="<?php echo $data[0]->id; ?>" />
                  <?php echo $data[0]->nome; ?>
                </div>
                <div class="form-group">
                  <label for="data_instalacao">Data da instalação</label>
                  <input type="date" class="form-control" id="data_instalacao" aria-describedby="" placeholder="" value="<?php echo $data[0]->data_instalacao; ?>">
                </div>
                <div class="form-group">
                  <label for="data_limite">Data limite de cadastro</label>
                  <input type="date" class="form-control" id="data_limite" aria-describedby="" placeholder="" value="<?php echo $data[0]->data_limite; ?>">
                </div>
                <div class="form-group">
                  <label for="email_responsavel">E-mail do responsável</label>
                  <input type="email" class="form-control" id="email_responsavel" aria-describedby="" placeholder="" value="<?php echo $data[0]->email_responsavel; ?>">
                </div>
                <div class="form-group">
                  <label for="email_contador">E-mail do contador</label>
                  <input type="email" class="form-control" id="email_contador" aria-describedby="" placeholder="" value="<?php echo $data[0]->email_contador; ?>">
                </div>
                <a type="button" id="btn-atualizar" class="btn btn-primary">Atualizar</a>
                <a type="button"  href="{{ URL::to('/home/') }}" class="btn btn-danger">Cancelar</a>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<script>

$(document).ready(function() {

$('#btn-atualizar').click(function(){

  $btn = $(this);
  $data_instalacao = $("#data_instalacao").val();
  $data_limite = $("#data_limite").val();
  $email_responsavel = $("#email_responsavel").val();
  $email_contador = $("#email_contador").val();
  $id = $("#id").val();


  if( $data_instalacao.length < 1 || $data_limite.length < 1 || $email_responsavel.length < 1 || $email_contador.length < 1)
  {
    swal('Todos os campos são obrigatórios.');

    return false;
  }

  $btn.attr('disabled',true);

  $url = "{{ URL::to('/cadastro/atualizar/') }}";

  $.ajax({
    url: $url,
    type: 'POST',
    data: {"_token":"{{ csrf_token() }}",
            data_instalacao:$data_instalacao,
            data_limite:$data_limite,
            email_responsavel:$email_responsavel,
            email_contador:$email_contador,
            id:$id
          },
    datatype : "application/json",
    success: function(dataReturn, textStatus, xhr)
    {

      if(xhr.status == 200)
      {
        swal({
          title: "",
          text: "Cadastro atualizado com sucesso.",
          type: "info",
          showCancelButton: false,
          confirmButtonColor: "#0073e6",
          confirmButtonText: "Sim",
          closeOnConfirm: false,
          closeOnClickOutside: false,
          closeOnEsc: false

        }).then((value) => {
              window.location.href = "{{ URL::to('/home/') }}";
            });
      }
    },
    error: function(xhr, status, error) {
      swal('Erro.'+error.text);
      $btn.attr('disabled',false);
      return false;
    }
  }).done(function() {
    $btn.attr('disabled',false);
  });

})


});



  </script>


@endsection
