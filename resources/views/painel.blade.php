@extends('layouts.painel')

@section('content')
<script
src="https://code.jquery.com/jquery-2.2.4.min.js"
integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

<div class="container-fluid info">

  <input type="hidden" id="ultimo_chamado" value="<?php echo $ultimo_chamado[0]->ultimo_chamado; ?>">
  <div class="row" id="cards-chamados" style="font-family: Arial, Helvetica, sans-serif !important;">

    <?php foreach($chamados as $chamado){ ?>

      <div class="col-md-3 col-sm-4 info">
        <div class="wrimagecard wrimagecard-topimage" style="height:300px;">
          <a href="#">
            <div class="wrimagecard-topimage_header info" id="card_<?php echo $chamado->chamado_id; ?>" style="background-color:rgba(187, 120, 36, 0.1) ">
              <left><i class="fa fa-list-alt info" style="font-size:20pt; color:#BB7824"> <?php echo $chamado->categoria; ?> </i></left>
            </div>
            <div class="wrimagecard-topimage_title">
              <i class="fa fa-shopping-bag info" style="font-size:12pt" aria-hidden="true"> <?php echo $chamado->brazil_store_name; ?> </i>
              <br>
              <i class="fa fa-calendar info" style="font-size:12pt; margin-bottom:10px" aria-hidden="true"> <?php echo $chamado->data_chamado; ?> </i>
              <p><?php echo substr($chamado->descricao,0,100); ?></p>
            </div>
          </a>
        </div>
      </div>
    <?php } ?>
  </div>
</div>

<div class="load-painel">
  <table style="float:center" width="100%" border=0>
    <tr>
      <td style="color:white !important; text-align:center">
        <i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>
      </td>
    </tr>
  </table>
</div>

<style>
.wrimagecard{
  margin-top: 0;
  margin-bottom: 1.5rem;
  text-align: left;
  position: relative;
  background: #fff;
  box-shadow: 12px 15px 20px 0px rgba(46,61,73,0.15);
  border-radius: 4px;
  transition: all 0.3s ease;
}
.wrimagecard .fa{
  position: relative;
  font-size: 70px;
}
.wrimagecard-topimage_header{
  padding: 20px;
}
a.wrimagecard:hover, .wrimagecard-topimage:hover {
  box-shadow: 2px 4px 8px 0px rgba(46,61,73,0.2);
}
.wrimagecard-topimage a {
  width: 100%;
  height: 100%;
  display: block;
}
.wrimagecard-topimage_title {
  padding: 20px 24px;
  height: 80px;
  padding-bottom: 0.75rem;
  position: relative;
}
.wrimagecard-topimage a {
  border-bottom: none;
  text-decoration: none;
  color: #525c65;
  transition: color 0.3s ease;
}

.load-painel {
  overflow: hidden;
  background-color: #333;
  position: fixed;
  bottom: 0;
  padding:10px;
  width: 100%;
}

.load-painel a:hover {
  background: #ddd;
  color: black;
}
.info{
  font-family: Arial !important;
}




</style>

<script>

$(document).ready(function() {

  atualizarPainel();

  window.onload = setInterval("atualizarPainel()", 20000);

});

function atualizarPainel(){

  $url = "{{ URL::to('/atualiza-painel/') }}";

  $(".load-painel").show();

  $.ajax({
    url: $url,
    type: 'GET',
    data: {"_token":"{{ csrf_token() }}"
  },
  datatype : "application/json",
  success: function(dataReturn, textStatus, xhr)
  {
    $('#cards-chamados').html(dataReturn.html);

    return true;
  },
  error: function(xhr, status, error) {
      return false;
  }
}).done(function(dataReturn) {

  $ultimo_chamado =  $("#ultimo_chamado").val();

  $('.cards').each(function(i,e) {

    $novo_chamado = false;

    $card_chamado = $(this).data('chamado');

    if($card_chamado > $ultimo_chamado)
    {
      $(this).delay(5000).css("background-color"," #99ff99");
      $novo_chamado = true;

    }

    if($novo_chamado == true)
    {
      var audio = new Audio('bell.mp3');
      audio.play();
    }

  });

  $("#ultimo_chamado").val(dataReturn.ultimo_chamado[0].ultimo_chamado);

  // $(".load-painel").hide();

});

}


</script>

@endsection
