<?php foreach($chamados as $chamado){ ?>

  <div class="col-md-3 col-sm-4" >
    <div class="wrimagecard wrimagecard-topimage" style="height:300px; font-family: Arial, Helvetica, sans-serif !important;">
      <a href="#">
        <div class="wrimagecard-topimage_header cards" data-chamado="<?php echo $chamado->chamado_id; ?>" id="card_<?php echo $chamado->chamado_id; ?>" style="background-color:rgba(187, 120, 36, 0.1) ">
          <left><i class="fa fa-list-alt" style="font-size:15pt;"><span style="font-family: Arial, Helvetica, sans-serif !important;"> <?php echo $chamado->departamento; ?> - <?php echo $chamado->categoria; ?> </span></i></left>
        </div>
        <div class="wrimagecard-topimage_title">
          <i class="fa fa-shopping-bag" style="font-size:12pt" aria-hidden="true"><span style="font-family: sans-serif !important;"> <?php echo $chamado->brazil_store_name; ?> </span> </i>
          <br>
          <i class="fa fa-calendar" style="font-size:12pt; margin-bottom:10px" aria-hidden="true"><span style="font-family: sans-serif !important;"> <?php echo $chamado->data_chamado; ?> </span> </i>
          <p><?php echo substr($chamado->descricao,0,100); ?></p>
        </div>
      </a>
    </div>
  </div>

<?php } ?>
