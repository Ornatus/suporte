@extends('layouts.menu')

@section('content')

<div class="row" style="margin:10px !important">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">

      </div>
      <div class="panel-body">
        <table class="table table-striped table-responsive" style="font-size: 10pt; font-family: Verdana;">
          <thead>
            <tr style="padding:5px">
              <b>Grupo:</b><br>
              <select name="grupo" id="grupo" class="form-control" style="width:100%; margin-bottom:10px">
                <option value="0">Selecionar</option>
                <?php foreach($grupos as $grupo){ ?>
                  <option <?php echo ($user[0]->grupo_id ==  $grupo->grupo_id) ? 'selected' : ''; ?> value="<?php echo $grupo->grupo_id; ?>"><?php echo $grupo->descricao; ?></option>
                <?php } ?>
              </select>
            </tr>
            <tr style="padding:5px">
              <b>Aprova Chamado :</b><br>
              <select name="aprova_chamado" id="aprova_chamado" class="form-control" style="width:100%; margin-bottom:10px">
                <option value="0">Selecionar</option>
                <option <?php echo ($user[0]->aprovar_chamado == 0 ) ? 'selected' : '' ;?> value="0">Não</option>
                <option <?php echo ($user[0]->aprovar_chamado == 1 ) ? 'selected' : '' ;?> value="1">Sim</option>
              </select>
            </tr>
            <tr>
              <th scope="col">
                ID:<br>[<?php echo $user[0]->user_id; ?>]
              </th>
              <th scope="col">
                Usuario:<br><?php echo $user[0]->username; ?>
              </th>
              <th scope="col">
                Nome:<br><?php echo $user[0]->firstname; ?>
              </th>
              <th scope="col">
                Sobrenome:<br><?php echo $user[0]->lastname; ?>
              </th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
<br><br>

<div class="navbar2">
  <table style="float:right">
    <tr>
      <td style="padding:5px">
        <a type="button" id="btn-voltar" class="btn btn-info btn-voltar">Voltar</a>
      </td>

      <td style="padding:5px">
        <a type="button" id="btn-atualizar" class="btn btn-success">Atualizar</a>
      </td>
    </tr>
  </table>
</div>



<script>
$(document).ready(function() {

  $(".btn-voltar").click(function(){

    window.location = "{{ URL::to('/usuarios/') }}";
  })

  $("#btn-atualizar").click(function(){

    $btn = $(this);
    $btn.attr('disabled',false);

    url = "{{ URL::to('/atualiza-usuario') }}";

    var grupo = $('#grupo');
    var aprovar_chamado = $( "#aprova_chamado option:selected" ).val();

    if(grupo.val() == 0){
      swal("Erro!", "Por favor selecione o gurpo para atualizar.", "error");
      return false;
    }

    $.ajax({
      url: url,
      type: 'POST',
      data: {
        grupo: grupo.val(),
        usuario_id: '<?php echo $user[0]->user_id; ?>',
        aprovar_chamado: aprovar_chamado,
        "_token":"{{ csrf_token() }}"
      },
      error: function(jq,status,message) {
        swal("Erro!", "Erro ao atualizar o usuario.", "error");
        $btn.attr('disabled',false);
        return false;
      }
    })
    .done(function( msg ) {

      if(msg==1)
      {
        $('#mensagem').val('');

        swal({
          title: "",
          text: "Usuario Atualizado.",
          type: "success",
          showCancelButton: false,
          cancelButtonText: "Não",
          confirmButtonColor: "#1f90bb",
          confirmButtonText: "OK",
          closeOnConfirm: true
        } ,
        function(){
          swal({
            title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
            text: 'Aguarde...',
            html: true,
            showCancelButton: false,
            showConfirmButton: false,
            closeOnConfirm: false,
            closeOnCancel: false
          });

          window.location = "{{ URL::to('/usuarios') }}";
        })
        .fail(function( jqXHR, textStatus ) {
          swal("Erro!", "Erro ao finalizar o usuario.", "error");
          $btn.attr('disabled',false);
        })
      }
    });

  })
  
});


</script>


@endsection

<style>

.navbar2 {
  overflow: hidden;
  background-color: #333;
  position: fixed;
  bottom: 0;
  padding:10px;
  width: 100%;
}

.navbar2 a:hover {
  background: #ddd;
  color: black;
}

</style>
