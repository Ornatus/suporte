@extends('layouts.menu')

@section('content')

<div class="row" style="margin:10px !important">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">

      </div>
      <div class="panel-body">
        <table class="table table-striped table-responsive" style="font-size: 10pt; font-family: Verdana;">
          <thead>
            <tr>
              <th scope="col">
                Categoria: <?php echo $categoria[0]->descricao; ?>[<?php echo $categoria[0]->categoria_id; ?>]
              </th>
            </tr>
            <tr>
              <th scope="col">
                Departamento:<br> <input class="form-control" type="text" name ="departamento" id="departamento" value="<?php echo $categoria[0]->departamento; ?>"></input>
              </th>
            </tr>
            <tr>
              <th scope="col">
                Descrição:<br> <input class="form-control" type="text" name ="descricao" id="descricao" value="<?php echo $categoria[0]->descricao; ?>"></input>
              </th>
            </tr>
            <tr>
              <th scope="col">
                Cor: (Em inglês ou hexadecimal)<br> <input type="text" class="form-control" name ="cor"  id="cor" value="<?php echo $categoria[0]->cor; ?>"></input>
              </th>
            </tr>
            <tr>
              <th scope="col">
                Categoria finaliza chamado:
                <select name="finaliza_chamado" id="finaliza_chamado" class="form-control" style="width:auto; margin-bottom:10px">

                  <option <?php echo ($categoria[0]->finaliza_chamado == 0 ) ? 'selected' : '' ;?> value="0">Não</option>
                  <option <?php echo ($categoria[0]->finaliza_chamado == 1 ) ? 'selected' : '' ;?> value="1">Sim</option>
                </select>
              </th>
            </tr>
            <tr>
              <th scope="col">
                Semáforo:
                <select name="is_semaphore" id="is_semaphore" class="form-control" style="width:auto; margin-bottom:10px">
                  <option <?php echo ($categoria[0]->is_semaphore == 0 ) ? 'selected' : '' ;?> value="0">Não</option>
                  <option <?php echo ($categoria[0]->is_semaphore == 1 ) ? 'selected' : '' ;?> value="1">Sim</option>
                </select>
              </th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
<br><br>

<div class="navbar2">
  <table style="float:right">
    <tr>
      <td style="padding:5px">
        <a type="button" id="btn-voltar" class="btn btn-info btn-voltar">Voltar</a>
      </td>

      <td style="padding:5px">
        <a type="button" id="btn-atualizar" class="btn btn-success">Atualizar</a>
      </td>
    </tr>
  </table>
</div>

<!-- Modal da finalização do chamado -->
<!-- Modal -->
<div class="modal fade" id="modal-finaliza" tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="TituloModalCentralizado">Resolução do chamado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="md-form">
          <i class="fas fa-pencil-alt prefix"></i>
          <textarea id="resolucao" name="resolucao" class="md-textarea form-control" rows="3"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary btn-modalfinalizar">Finalizar</button>
      </div>
    </div>
  </div>
</div>


<script>
$(document).ready(function() {

  $(".btn-voltar").click(function(){

    window.location = "{{ URL::to('/categorias/') }}";
  })

  $("#btn-atualizar").click(function(){

    $btn = $(this);
    $btn.attr('disabled',false);

    url = "{{ URL::to('/atualiza-categoria') }}";

    var cor = $('#cor');
    var departamento = $('#departamento');
    var descricao = $('#descricao');
    var finaliza_chamado = $( "#finaliza_chamado option:selected" ).val();
    var is_semaphore = $( "#is_semaphore option:selected" ).val();

    if(cor.val() == '' || descricao.val() == '' || departamento.val() == ''){
      swal("Erro!", "Por favor preencha os campos (Cor e Descrição).", "error");
      return false;
    }

    $.ajax({
      url: url,
      type: 'POST',
      data: {
        descricao: descricao.val(),
        cor: cor.val(),
        departamento: departamento.val(),
        finaliza_chamado: finaliza_chamado,
        categoria_id: '<?php echo $categoria[0]->categoria_id; ?>',
        is_semaphore:is_semaphore,
        "_token":"{{ csrf_token() }}"
      },
      error: function(jq,status,message) {
        swal("Erro!", "Erro ao atualizar o categoria.", "error");
        $btn.attr('disabled',false);
        return false;
      }
    })
    .done(function( msg ) {

      if(msg==1)
      {
        $('#mensagem').val('');

        swal({
          title: "",
          text: "Categoria Atualizado.",
          type: "success",
          showCancelButton: false,
          cancelButtonText: "Não",
          confirmButtonColor: "#1f90bb",
          confirmButtonText: "OK",
          closeOnConfirm: true
        } ,
        function(){
          swal({
            title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
            text: 'Aguarde...',
            html: true,
            showCancelButton: false,
            showConfirmButton: false,
            closeOnConfirm: false,
            closeOnCancel: false
          });

          window.location = "{{ URL::to('/categorias') }}";
        })
        .fail(function( jqXHR, textStatus ) {
          swal("Erro!", "Erro ao finalizar o categoria.", "error");
          $btn.attr('disabled',false);
        })
      }
    });

  })
  /*
  $("#btn-atualizar").click(function(){
  $('#modal-atualizar').modal('show');
})
*/
});


</script>


@endsection

<style>

.navbar2 {
  overflow: hidden;
  background-color: #333;
  position: fixed;
  bottom: 0;
  padding:10px;
  width: 100%;
}

.navbar2 a:hover {
  background: #ddd;
  color: black;
}

</style>
