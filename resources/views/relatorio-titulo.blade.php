@extends('layouts.menu')

@section('content')

<div class="row" style="margin:10px !important">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <form id="filtro" name="filtro" method="GET">
          {!! csrf_field() !!}
          <table width="auto" border=0>
            <h4 class ="text-center"><b>Relatorio de Títulos</b></h4>
            <td style="width:250px; padding:15px;" >
              <span class="lbl-date">Selecione a Categoria:<br></span>
              <select name="select" id="categorias" style="height:35px; width:200px; border-radius:10px;">
                <option value="0"> Todos </option>
                <?php foreach($categorias as $categoria){ ?>
                  <option value="<?php echo $categoria->categoria_id; ?>"><?php echo $categoria->descricao?></option>
                <?php } ?>
              </select>
            </td>
            <td style="width:250px; padding:15px;">
              <span class="lbl-date">Data Min:<br></span>
              <input type="date" id="data_min" name="data_min" value="" style=" width:200px; height:35px; border-radius:10px;"  />
            </td>
            <td style="width:250px; padding:15px;">
              <span class="lbl-date">Data Max:<br></span>
              <input type="date" id="data_max" name="data_max" value="" style=" width:200px; height:35px; border-radius:10px;"  />
            </td>
            <td style="width:250px; padding:15px;">
              <span class="lbl-date">Filtrar por:<br></span>
              <select class="" name="filter_order" id="filter_order" style="height:35px; width:200px; border-radius:10px;">
                <option value="0">Selecionar</option>
                <option value="1">Titulo</option>
                <option value="2">Quantidade Chamado</option>
              </select>
            </td>
            <td  style="width:250px; padding:15px;">
              <span class="lbl-date">Ordernar por:<br></span>
              <select class="" name="filter_order_direction" id="filter_order_direction" style="height:35px; width:200px; border-radius:10px;">
                <option value="1">ASC</option>
                <option value="2">DESC</option>
              </select>
            </td>
          </tr>
          <tr>
            <td style="width:200px; padding:15px;">
              <a type="button" id="btn-relatorio" class="btn btn-primary" style="">Gerar Relatório</a>
            </td>
            <td style="width:200px; padding:15px;">
              <a  id="export-relatorio-titulo" type="button" class="btn btn-success">Exportar para Excel</a>
            </td>
          </tr>
        </table>
      </form>

    </div>
    <div class="panel-body">
      <div id="lista-relatorio"></div>
    </div>
  </div>
</div>
</div>


<script>



$(document).ready(function() {



  $('#btn-relatorio').click(function(){

    swal({
      title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
      text: 'Aguarde...',
      html: true,
      showCancelButton: false,
      showConfirmButton: false,
      closeOnConfirm: false,
      closeOnCancel: false
    });

    $("#lista-relatorio").html('');

    var categoria = $('#categorias');
    var filter_order_direction = $('#filter_order_direction');
    var filter_order = $('#filter_order');
    var data_min = $('#data_min');
    var data_max = $('#data_max');


    var url = "{{ URL::to('/lista-relatorio-titulo/') }}";

    $.ajax({
      url: url,
      type: 'GET',
      data: {
        categoria: categoria.val(),
        filter_order_direction: filter_order_direction.val(),
        filter_order: filter_order.val(),
        data_min: data_min.val(),
        data_max: data_max.val()
      },
      success: function(data) {
        $("#lista-relatorio").html(data);
          swal.close()
      }
    });


  });

  $('#export-relatorio-titulo').click(function(){

    var url = "{{ URL::to('/lista-relatorio-titulo/') }}";

    var categoria = $('#categorias');
    var filter_order_direction = $('#filter_order_direction');
    var filter_order = $('#filter_order');
    var data_min = $('#data_min');
    var data_max = $('#data_max');


    $.ajax({
      url: url,
      type: 'GET',
      data: {
        categoria: categoria.val(),
        filter_order_direction: filter_order_direction.val(),
        filter_order: filter_order.val(),
        data_min: data_min.val(),
        data_max: data_max.val()
      },  success: function(data) {
          $("#lista-relatorio").html(data);
          var a = document.createElement('a');
          var data_type = 'data:application/vnd.ms-excel';
          var table_div = document.getElementById('lista-relatorio');
          var table_html = table_div.outerHTML.replace(/ /g, '%20');
          a.href = data_type + ', ' + table_html;
          a.download = 'Relatorio_por_Titulo.xls';
          a.click();
        }

    });
  });


});

</script>



@endsection
