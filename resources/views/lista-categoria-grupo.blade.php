<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Departamento</th>
      <th scope="col">Categoria</th>
      <th scope="col"> </th>
    </tr>
  </thead>
  <tbody>

    <?php foreach($categorias as $categoria){ ?>
      <tr>
        <th scope="row"><?php echo $categoria->categoria_id; ?></th>
        <td><?php echo $categoria->departamento; ?></td>
        <td><?php echo $categoria->descricao; ?></td>
        <td>    <a data-id="<?php echo $categoria->grupo_categoria_id; ?>" style="text-align:left !important" type="button" class="btn btn-danger btn-remover-categoria" name ="Remover"  id="Remover" >Remover</a></td>
      </tr>

    <?php } ?>


  </tbody>
</table>

<script>
$(document).ready(function() {

  $(".btn-remover-categoria").click(function(){

    $btn = $(this);
    $btn.attr('disabled',false);

    url = "{{ URL::to('/remover-categoria') }}";

    var grupo_categoria_id = $(this).data('id');

    $.ajax({
      url: url,
      type: 'POST',
      data: {
        grupo_categoria_id: grupo_categoria_id,
        "_token":"{{ csrf_token() }}"
      },
      error: function(jq,status,message) {
        swal("Erro!", "Erro ao remover a categoria.", "error");
        $btn.attr('disabled',false);
        return false;
      }
    })
    .done(function( msg , status,xhr) {
      if(xhr.status==201)
      {
        swal("Erro!", "Não foi possivel remover a categoria do grupo.", "error");
        $btn.attr('disabled',false);
        return false;
      }
      else {

        atualizarCategoria();

        $('#mensagem').val('');

        swal({
          title: "",
          text: "Categoria removida.",
          type: "success",
          showCancelButton: false,
          cancelButtonText: "Não",
          confirmButtonColor: "#1f90bb",
          confirmButtonText: "OK",
          closeOnConfirm: true
        } ,
        function(){
          swal({
            title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
            text: 'Aguarde...',
            html: true,
            showCancelButton: false,
            showConfirmButton: false,
            closeOnConfirm: false,
            closeOnCancel: false
          });
        });
      }
    });
  })


})

</script>
