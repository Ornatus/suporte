<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
  <script src="{{ asset('js/sweetalert/dist/sweetalert.min.js') }}"></script>
  <script src="{{ asset('js/jquery.mask.js') }}"></script>

  <script src="{{ asset('js/select/js/bootstrap-select.min.js') }}"></script>

  <link href="{{ asset('js/sweetalert/dist/sweetalert.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

  <link rel="stylesheet" href="{{ asset('js/select/css/bootstrap-select.min.css') }}">

  <title>Suporte</title>

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
  <div id="app">
    <nav class="navbar navbar-default navbar-static-top">
      <div class="container" style=" width:99% !important; margin:2px !important">
        <div class="navbar-header">

          <!-- Collapsed Hamburger -->
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
            <span class="sr-only">Toggle Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

          <!-- Branding Image -->
          <a class="navbar-brand" style="padding:0px">
            <img width="105px" height="45px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAR0AAABzCAMAAACSPSbXAAAAM1BMVEX///96enrf39+/v7+ZmZmEhIT39/etra3MzMzv7++UlJTn5+eMjIzX19e3t7elpaXFxcWEeKRPAAAMGElEQVR4nO1d2Zbjqg61mbEx+P+/9iJGMSSV83A7VSRa1asTDwS2NSPwtn3pL5AgF41EiHh3Z34VXZTxHRNnlLy7U7+ChHL7nJz6dB46JX+ATWAhqd/dwTcSYU+gicQ+VcJ0iw1zXiMf8KE97j6Sf2gFwEiVIABgLi9wCkscfW9H30DnUZULEh5WwbgqQMf5nk6+i+7KGRIft/5AAUvcJiOo/n0X30dyf4AOwIPt+M2nV61MIksVvaKaaU62bCKyemIf4vxkcECbuJEpWGejSBKv4yPgyeAEWMQ+jNle/Q3yc+DJ4CQBMnd/AR0NeNLhx/+9c2+nyAg8M4jlPUdM0NnUh6jmqGR5cWD0bvsr+gNACZ6B0dYiEkeJvDsZjZZymWVcwyFXEkE13LgeCY51TjoEsuXljaUDrJEst+/RhkV4zMqa2U3iptsboxOizfSdN6dZ8Z2jwpqJ3SIU5Yo1h4QHgPkTR3JzNHAWUVmEaEAnnIvGbt2MRhgfb7w9bjfNPTxJfKiXIKJMBUEc3P8L7HTyAduVSE3szr4HdyZp4oMLt0MkasoFwnp5jMJ2D0prJTKTZ++55OQsY+ajrr2HgJ0lFgsNmG1JUjO9wfebO9DWAQ+vmwe3D3gpCWPUW32ksQYdM7XBdu7HDuEF6GGzg27hjfDhm9iymuecmpykc7xmBm3trfbRzWSd2LOOcrdiotn2gWRw7EiSFG+QfAyuOou2gTxS5AGaVX0e0xscJkXgnfhNgT9IR4tkG3a7F9XLUbBwIGBBZJxnp+js+IFb7ob7TIOOXjTaCsF5M3gFnuGxsyuZczckuCwFUM/tOgogx5qhOhs8OQH2x/8dyQr5ELUNMj2jaK+tArCZge4B5CVoYm1iXMmyjb6zwkUx1u1t2Y2FKQgo/1ed/ld0TtSpDoHTkdABqxV4hGZG8bjx8Id9nCVtupr5cSlsiMe9gEE8CgKXTHadT3dV4tiKgTodEzueLl4C0xAvQL7iKiimqSzuMBh22tAfJzm3NcImQSGBY7wedrSgE1jLdHwyh/mP00OB4CGUED7CAunxIMqAU6j7mgFBVjRaD9GJrGITDJ5dDMQWF1wLvDNM6JAVA9GH6EAi/roKDMm0GQ9WSNEPMrQuOh0f6KCHPPNwjwMVIl+nwMLR8HGcg1gXnY530lxVzDYLGhIVNigVEDQ557aPQYfGfEVwCiVMTWx5DoIF5ZzkqnX9PgidkPkSJpyK4cKV0dn3nEAlTUi+JDohX9xlhGmMmFJazIQglSB0UnbZNqCGqGu1/NfMiaMBsJj42SkL52lFJ+URhdE/NfTn6UbMkEkFLshlcq6iE+b1cq6H8k1bVm6dMeGfJ4K4IRMk+lgIT00UpStwjQ6HPDjCKgjuIbao4X1IwK6WHBRh+N1BFl0dz1RxpuoMgUUAx5t3z0EkTFZgWZq28/fpmEhEqubJrh+vOQ2PC4Rem05yV9SwmvDgAiQnimdzBQsVY6qc0rmDKbOggAxzt26bWc1k5XREd1CYjE7QJy4zk4siRPkQjvMVlXJWGH1kEKtO4HBgilTZbkSpM+1i9IDxcmnlLUnRWLud0SkaJ2GYZOx6pZEFKJZg9PlyumeLVMEJE+t7OYFIzxBbgxrLvGniSad5CdBHdUmS3gRNMubtOburcMmZ8lqDKNIjefEDl9FsKbTuxm6qXSxaFLOe8dMipIvYbM0CrYDSWdf6WdOdK9IoJ3p6GYrckaKAs139yQnd5yQLGmRh1sm13MXRJbJBoV96bZknR6saF8fKrIMNVCRxyUdrrvkYaIY80IL1F4WiRhmSxWLYzmCyFis6ROuFWJVIp2YLtUI20y3Jq14td9EQfcQZGJ5+chgoLXpbVSUnOh7Bg9CZ5NQTOKtl23tKa5AGeDTWyONdEZyl1x8FSvrj6HRP4x32gdRpHtmx5SjlcHirXBr/uAvCU5XPJ4BTFjE2GlbtDY1zNJ8Cjuee5BbXzT9E5ygj5snbiXwKOEX3+Gghz1ntHWUo8kr93XwMOGgnDB4WQZw9OMmm6Zzm+ZhdMBLZjAOX5zYJtiTsCVa+Le4EjkSqlZpujMbq+WO1AtwXqGRHfyD+cYwTScufsUHJr48jbZ/zD6er1fz/N3q8VSVsVvnu3v0CEkqaARkjP34fz0qaUMgj77AXI7OUfLZAfelLX/rSl770pS+9gwRR8MIC9b4XFujw1oRf6AoKeuwGqh7AqefsDduFw27Kh/Uusw8j6O+KIig3Oa4RF8SF/7wHF+epBE4o86uSOuJoc5OE/3N0LE7kQK7912SSPThdruBM6AhKAt0EJS8vBYcK81/+JFEwfXnOrg5EqIPo8n4or7Kb4ZNlMvmE5sl1q3YR26aar+GnL1zbI0tB/GWpIoiU73o3hvAjV5qCFXfobD7pxtL6XCYjbtBCtzz2vb75AiZu0d4L2uWSHHjmRt62udqLreHW9+Z2/NHrIdQwRc5KvT80f8ibmTSBkS9oJ7j03ebm4wahaXg9bXGaDCnXOoZN+fGSy/L0nUyqzepTSD/qha12h7YVSqrURpC8rBxdrY+yyxmgZycSI/iwZxNUHZDSaPh0N8UJrJ//M402UHU7G5eHGfsmLp7GgFmN5LoplQ6LlOtnT4vH8yM5UaFwhw4p6Ih8NSnVJCfHo1APinfGsgtZKnEzOmjXxi2uXmpaYg06x1DXTApz0Wdj4Pk2FU6fz4vHS5uyXvcQnebqwLei3ZmqHWEizzrj84EZQJ2HlZowyJbCW5MaoBt0wvxhK8UVndDsNR8DetIiDfWZeahvv6iC/QI6Kl0i+4okNtYNqun2nCYfrehIVH/AmGzhadCxsAauhZy0iok8QqdlY/bcuSltkjqGF9AhcS5cD4x5jRVccroHhENSSsrv1m6zTXaKCN3s5ePonvpr6Jzd6i/+vI4Tjzc/uJfQCc/AjozJh6UkE3ba8vYhW4tO1SUenXYfNYyO8kO8u+rd19AJGwWg4z9UuSLJKhi8gA6NH45xzQcb2GlQEflHenQkGh+gA8UKBR6MDoPdQTu9/CI6oLGQJ9qHDcRKHwrKbHmRnn1k0SfoCB57PSmEpEPRwMvoGPQcAZ0ITz5Qh6HDQdc+hhfRiWswy437wPxQu3bX09miV835IzrEHHmfzhfQMY/Qke04buwbBHQCPEkQEDo2dP9ql02+ik6sLsrSNXI6KLTyGSLmyzvd+DVET9HRPszOs5ivocOmJSgU+07g6cvG/WNpk8ICj619NlGztRruZXTiQuYkXXZco8tadHzUcTRG9wW9k35nEu3TwX7b6VZVMleAwabv3n6bfYZOWHgrY7v51OVjDiDT9OZ1dGJtuUxtDZ5qi05o0+Ei/JfRmRgouLmrcrumK2eKQk/jELx1b/JvnnHnMIQOCqyQXv4v6ATlEyXKzFi9fM7RiUEjuFphvOv9fVNudPTYaMbMxB3U2JUg6X/EYqzRExKho0tPG44f0cFfr853VPnH1PDoJuiApasrq9qW0b6/PTrX4C+IiZKhk8WetHQKx1nIotcuAjyqonOXgSqs9rpO634MXa9kxuDodecMnQ275rxRFKxKzzD0o9f5Y7Jim7Gvl6NsNqsMYE7E7k2Ap6Bjyi8KLNgdOv4y/NxcL+4kYxBYsw3oMDpZZ8vavMRegK7Y6mGUZ8eYgs+sN7hgrWyhdFhFJ2//HvuI74eQdM+XVz7EFqdHx2KVKPJNZ708NxMax75Eg04ZexVjjVWAq+Pte7D1OQvh5u+DUB08OFeIAryz5o3ahEXwUfKt1QRr9Gz6vgmOJEZmfitusq1XC6ickSVfxSs6AqEjKjyqcJuQreYb0oyK11c26oM/yJZA/XvJ1WmGm1FoWDUJ1/mwZ+6zbmxRTTGOfbtKhlPUEIWlQyfHBpK4UHAFboLDdZ4K1+ADiGkIl9mNJZBhNGi8nlvNkDuG4kljvUd5uf3xi4dDVbuD9LBibYLV4cAHuAcSqH0sDS5c6NfRoANLd1LtoRz6Rrwd9mMg1tRn5i2q78MleZek00qG93Abht6cd8dXlteUcv0Gl3PZ1PalF5wPWlffznHf7vNiN6HcER5Rk5wXqdF6FS3U8KGASxij3lWvfVJUel9W1WbaHuQxICV6Mw8Qk9+SxR/pf242S4iJ9U4vAAAAAElFTkSuQmCC">
          </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
          <!-- Left Side Of Navbar -->
          <ul class="nav navbar-nav">
            &nbsp;
          </ul>

          <!-- Right Side Of Navbar -->
          <ul class="nav navbar-nav navbar-right">

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Cadastros <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li>
                  <a href="{{ route('chamados') }}">
                    Chamados
                  </a>
                  <a href="{{ route('categorias') }}">
                    Categorias
                  </a>
                  <a href="{{ route('grupos') }}">
                    Grupos
                  </a>
                  <a href="{{ route('titulos') }}">
                    Titulos
                  </a>
                  <a href="{{ route('usuarios') }}">
                    Usuarios
                  </a>
                </li>
              </ul>
            </li>


            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                <i class="fa fa-bar-chart" aria-hidden="true"></i> Relatórios <span class="caret"></span>
              </a>

              <ul class="dropdown-menu" role="menu">
                <li>
                  <a href="{{ route('relatorio-area') }}">
                    Relatório Área
                  </a>
                  <a href="{{ route('relatorio-backup') }}">
                    Relatório Backups
                  </a>
                  <a href="{{ route('relatorio-chamado') }}">
                    Relatório de Chamados
                  </a>
                  <a href="{{ route('relatorio-titulo') }}">
                    Relatório de Títulos
                  </a>
                  <a href="{{ route('relatorio-usuario') }}">
                    Relatório Usuários
                  </a>

                </li>
              </ul>
            </li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                <i class="fa fa-user"></i> <?php echo $usuario->firstname; ?> ( <?php echo $usuario_configuracao->grupo; ?> ) <span class="caret"></span>
              </a>

              <ul class="dropdown-menu" role="menu">
                <li>
                  <a href="{{ route('logout') }}">
                    Logout
                  </a>

                </li>
              </ul>
            </li>

          </ul>
        </div>
      </div>
    </nav>

    @yield('content')
  </div>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}"></script>



</body>
</html>
