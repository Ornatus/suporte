<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Suporte</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('formulario/entrar') }}">
                        Suporte
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>
                    <ul class="nav navbar-nav navbar-right">

                            <li >
                                <a href="{{ url('formulario/entrar') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Logout
                                </a>

                            </li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->



</body>
</html>
