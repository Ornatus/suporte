<table class="table">
  <thead>
    <tr>

      <th scope="col">Usuário(a)</th>
      <th scope="col">Quantidade de Chamados</th>
      <th scope="col">Tempo médio por chamado</th>

    </tr>
  </thead>
  <tbody>

    <?php foreach($results as $result){ ?>
      <tr>

        <td><?php echo $result->usuario; ?></td>
        <td><?php echo $result->quantidade_chamado; ?></td>
        <td><?php echo round($result->tempo_medio,2); ?> Minutos</td>

      </tr>

    <?php } ?>


  </tbody>
</table>
