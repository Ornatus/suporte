@extends('layouts.menu')

@section('content')

<div class="row" style="margin:10px !important">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class ="text-center"><b>Relatorio de Backups</b></h4>
        <form id="filtro" name="filtro" method="GET" >
          {!! csrf_field() !!}
          <table width="auto" border=0>

            <td style="width:250px; padding:15px;">
              <span class="lbl-date">Data Min:<br></span>
              <input type="date" id="data_min" name="data_min" value="<?php echo isset($data_min)? $data_min: '' ;?>" style=" width:180px; height:35px;  border: 1px solid gray;"  />
            </td>
            <td style="width:250px; padding:15px;">
              <span class="lbl-date">Data Max:<br></span>
              <input type="date" id="data_max" name="data_max" value="<?php echo isset($data_max)? $data_max: '' ;?>" style=" width:180px; height:35px;  border: 1px solid gray;"  />
            </td>


            <td  style="width:250px; padding:15px;">
              <span class="lbl-date">Loja:<br></span>
              <select class="" name="filter_customer" id="filter_customer" style="height:35px; width:150px; ">
                <option <?php echo $filter_customer == 0 ? 'selected':'' ;?> value="0">Selecionar</option>
                <?php foreach ($customers as $customer) { ?>
                <option <?php echo $filter_customer == $customer->brazil_store_id ? 'selected':'' ;?> value="<?php echo $customer->brazil_store_id;?>"><?php echo $customer->brazil_store_name;?></option>
              <?php } ?>
              </select>
            </td>
            <input type="number" class="hidden" id="first_access" name="first_access" value="1" />

            <td style="padding-top:20px;">
              <a type="button" id="btn-relatorio" type="submit" class="btn btn-primary" style="">Buscar</a>
            </td>
            <td style="padding-top:20px; padding-left:15px;">
              <a  id="export-relatorio-chamado" type="button" class="btn btn-success">Exportar para Excel</a>
            </td>
          </tr>
        </table>
      </form>
    </div>
    <div class="panel-body">
      <div width="100%"  style="padding-left:43%;">

      </div>
      <table class="table">
        <thead>
          <tr>
            <th scope="col">Nome Arquivo</th>
            <th scope="col">Data Criação</th>
            <th scope="col">Tamanho</th>
          </tr>
        </thead>
        <tbody>

          <?php foreach($backups as $backup){ ?>
            <tr>
              <td><?php echo $backup['name'];?></td>
              <td><?php echo $backup['date'];?></td>
              <td><?php echo $backup['size'];?></td>
            </tr>

          <?php } ?>


        </tbody>
      </table>

      <div width="100%"  style="padding-left:43%;">

      </div>

    </div>
  </div>
</div>
</div>

<div id="lista-relatorio" class="hidden">
</div>
<script>




$(document).ready(function() {

  $('#btn-relatorio').click(function(){


    var filter_customer = $('#filter_customer').val();
    var data_min = $('#data_min').val();
    var data_max = $('#data_max').val();

    var date1 = new Date(data_min);
    var date2 = new Date(data_max);
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));


    if(data_min.length < 1 || data_max.length < 1){
      swal("Erro!", "Selecione uma data.", "error");
      return false;
    }

    if(diffDays > 31){
      swal("Erro!", "Datas devem respeitar limite de 1 mês.", "error");
      return false;
    }

    if(data_min > data_max){
      swal("Erro!", "Data mínima deve ser menor que data máxima.", "error");
      return false;
    }

    swal({
      title: 'Aguarde',
      html: 'Aguarde.',
      showCancelButton: false,
      showConfirmButton: false
    });

    $("#filtro").submit();

  });

  $('#export-relatorio-chamado').click(function(){

    var first_access = $('#first_access').val();
    var filter_customer = $('#filter_customer').val();
    var data_min = $('#data_min').val();
    var data_max = $('#data_max').val();

    var date1 = new Date(data_min);
    var date2 = new Date(data_max);
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));


    if(data_min.length < 1 || data_max.length < 1){
      swal("Erro!", "Selecione uma data.", "error");
      return false;
    }

    if(diffDays > 31){
      swal("Erro!", "Datas devem respeitar limite de 1 mês.", "error");
      return false;
    }

    if(data_min > data_max){
      swal("Erro!", "Data mínima deve ser menor que data máxima.", "error");
      return false;
    }


    var url = "{{ URL::to('/excel-relatorio-backup/') }}";

    $.ajax({
      url: url,
      type: 'GET',
      data: {
        filter_customer: filter_customer,
        data_min: data_min,
        data_max: data_max,
        first_access:first_access
      },  success: function(data) {
        $("#lista-relatorio").html(data);
        var a = document.createElement('a');
        var data_type = 'data:application/vnd.ms-excel';
        var table_div = document.getElementById('lista-relatorio');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');
        a.href = data_type + ', ' + table_html;
        a.download = 'Relatorio_backup.xls';
        a.click();
      }

    });
  });

});

</script>



@endsection
