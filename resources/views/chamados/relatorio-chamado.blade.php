@extends('layouts.menu')

@section('content')

<div class="row" style="margin:10px !important">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class ="text-center"><b>Relatorio de Chamados</b></h4>
        <form id="filtro" name="filtro" method="GET" >
          {!! csrf_field() !!}
          <table width="auto" border=0>

            <td style="width:250px; padding:15px;">
              <span class="lbl-date">Data Min:<br></span>
              <input type="date" id="data_min" name="data_min" value="<?php echo isset($data_min)? $data_min: '' ;?>" style=" width:180px; height:35px; border-radius:10px; border: 1px solid gray;"  />
            </td>
            <td style="width:250px; padding:15px;">
              <span class="lbl-date">Data Max:<br></span>
              <input type="date" id="data_max" name="data_max" value="<?php echo isset($data_max)? $data_max: '' ;?>" style=" width:180px; height:35px; border-radius:10px; border: 1px solid gray;"  />
            </td>


            <td  style="width:250px; padding:15px;">
              <span class="lbl-date">Ordernar por:<br></span>
              <select class="" name="filter_order_direction" id="filter_order_direction" style="height:35px; width:150px; border-radius:10px;">
                <option <?php echo $filter_order_direction == 'ASC' ? 'selected':'' ;?> value="ASC">ASC</option>
                <option <?php echo $filter_order_direction == 'DESC' ? 'selected':'' ;?> value="DESC">DESC</option>
              </select>
            </td>

            <td style="padding-top:20px;">
              <a type="button" id="btn-relatorio" type="submit" class="btn btn-primary" style="">Gerar Relatório</a>
            </td>
            <td style="padding-top:20px; padding-left:15px;">
              <a  id="export-relatorio-chamado" type="button" class="btn btn-success">Exportar para Excel</a>
            </td>
          </tr>
        </table>
      </form>
    </div>
    <div class="panel-body">
      <div width="100%"  style="padding-left:43%;">
        <div style="">
          {{ $chamados->render()}}
        </div>
      </div>
      <table class="table">
        <thead>
          <tr>
            <th scope="col">Chamado</th>
            <th scope="col">Atendente</th>
            <th scope="col">Loja</th>
            <th scope="col">Status</th>
            <th scope="col">Título</th>
            <th scope="col">Categoria</th>
            <th scope="col">Data Chamado</th>
            <th scope="col">Data Atribuido</th>
            <th scope="col">Data Resolvido</th>
            <th scope="col">Prioridade</th>
            <th scope="col">Tempo de Atendimento</th>
            <th scope="col">Tempo Resolução</th>
          </tr>
        </thead>
        <tbody>

          <?php foreach($chamados as $chamado){ ?>
            <tr>
              <td><?php echo $chamado->chamado_id; ; ?></td>
              <td><?php echo $chamado->atendente_nome; ; ?></td>
              <td><?php echo $chamado->brazil_store_name ; ?></td>
              <td><?php echo $chamado->status; ?></td>
              <td><?php echo $chamado->titulo ; ?></td>
              <td><?php echo $chamado->categoria ; ?></td>
              <td><?php echo $chamado->data_chamado ; ?></td>
              <td><?php echo $chamado->data_atribuido ; ?></td>
              <td><?php echo $chamado->data_resolvido ; ?></td>
              <td><?php echo $chamado->prioridade ; ?></td>
              <td><?php echo $chamado->tempo_ate_atendimento ; ?></td>
              <td><?php echo $chamado->tempo_aberto ; ?></td>

            </tr>

          <?php } ?>


        </tbody>
      </table>

      <div width="100%"  style="padding-left:43%;">
        <div style="">
          {{ $chamados->render()}}
        </div>
      </div>

    </div>
  </div>
</div>
</div>

<div id="lista-relatorio" class="hidden">
</div>
<script>




$(document).ready(function() {

  $('#btn-relatorio').click(function(){
    swal({
      title: 'Aguarde',
      html: 'Aguarde.',
      showCancelButton: false,
      showConfirmButton: false
    });

    var filter_order_direction = $('#filter_order_direction').val();
    var data_min = $('#data_min').val();
    var data_max = $('#data_max').val();

    var date1 = new Date(data_min);
    var date2 = new Date(data_max);
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));


    if(diffDays > 31){
      swal("Erro!", "Datas devem respeitar limite de 1 mês.", "error");
      return false;
    }

    if(data_min > data_max){
      swal("Erro!", "Data mínima deve ser menor que data máxima.", "error");
      return false;
    }


    $("#filtro").submit();

  });

/*
  $('#btn-relatorio').click(function(){

    swal({
      title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
      text: 'Aguarde...',
      html: true,
      showCancelButton: false,
      showConfirmButton: false,
      closeOnConfirm: false,
      closeOnCancel: false
    });

    var filter_order_direction = $('#filter_order_direction').val();
    var data_min = $('#data_min').val();
    var data_max = $('#data_max').val();

    var date1 = new Date(data_min);
    var date2 = new Date(data_max);
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));


    if(diffDays > 31){
      swal("Erro!", "Datas devem respeitar limite de 1 mês.", "error");
      return false;
    }

    if(data_min > data_max){
      swal("Erro!", "Data mínima deve ser menor que data máxima.", "error");
      return false;
    }


    var url = "{{ URL::to('/relatorio-chamado') }}";

    $.ajax({
      url: url,
      type: 'GET',
      data: {
        filter_order_direction: filter_order_direction,
        data_min: data_min,
        data_max: data_max
      },
      success: function(data) {
        $("#lista-relatorio").html(data);
        swal.close()
      }
    });


  });
*/
  $('#export-relatorio-chamado').click(function(){


    var filter_order_direction = $('#filter_order_direction').val();
    var data_min = $('#data_min').val();
    var data_max = $('#data_max').val();

    var date1 = new Date(data_min);
    var date2 = new Date(data_max);
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));


    if(diffDays > 31){
      swal("Erro!", "Datas devem respeitar limite de 1 mês.", "error");
      return false;
    }

    if(data_min > data_max){
      swal("Erro!", "Data mínima deve ser menor que data máxima.", "error");
      return false;
    }


    var url = "{{ URL::to('/excel-relatorio-chamado/') }}";

    $.ajax({
      url: url,
      type: 'GET',
      data: {
        filter_order_direction: filter_order_direction,
        data_min: data_min,
        data_max: data_max
      },  success: function(data) {
        $("#lista-relatorio").html(data);
        var a = document.createElement('a');
        var data_type = 'data:application/vnd.ms-excel';
        var table_div = document.getElementById('lista-relatorio');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');
        a.href = data_type + ', ' + table_html;
        a.download = 'Relatorio_por_chamado.xls';
        a.click();
      }

    });
  });

});

</script>



@endsection
