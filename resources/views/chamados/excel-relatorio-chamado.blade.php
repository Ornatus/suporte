
<table class="table">
  <thead>
    <tr>
      <th scope="col">Chamado</th>
      <th scope="col">Atendente</th>
      <th scope="col">Loja</th>
      <th scope="col">Status</th>
      <th scope="col">Título</th>
      <th scope="col">Categoria</th>
      <th scope="col">Data Chamado</th>
      <th scope="col">Data Atribuido</th>
      <th scope="col">Data Resolvido</th>
      <th scope="col">Prioridade</th>
      <th scope="col">Tempo de Atendimento</th>
      <th scope="col">Tempo Resolução</th>
    </tr>
  </thead>
  <tbody>

    <?php foreach($chamados as $chamado){ ?>
      <tr>
        <td><?php echo $chamado->chamado_id; ; ?></td>
        <td><?php echo $chamado->atendente_nome; ; ?></td>
        <td><?php echo $chamado->brazil_store_name ; ?></td>
        <td><?php echo $chamado->status; ?></td>
        <td><?php echo $chamado->titulo ; ?></td>
        <td><?php echo $chamado->categoria ; ?></td>
        <td><?php echo $chamado->data_chamado ; ?></td>
        <td><?php echo $chamado->data_atribuido ; ?></td>
        <td><?php echo $chamado->data_resolvido ; ?></td>
        <td><?php echo $chamado->prioridade ; ?></td>
        <td><?php echo $chamado->tempo_ate_atendimento ; ?></td>
        <td><?php echo $chamado->tempo_aberto ; ?></td>

      </tr>

    <?php } ?>


  </tbody>
</table>
