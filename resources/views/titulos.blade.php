@extends('layouts.menu')

@section('content')

<div class="row" style="margin:10px !important">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">

        <form id="filtro" name="filtro" method="GET">
          {!! csrf_field() !!}
          <table width="auto">
            <td style="padding:5px"> Filtrar titulo:<br>
              <input type="text" id="filtro_titulo" name="filtro_titulo" value="<?php echo $filtro_titulo; ?>" style="margin-bottom:10px"  class="form-control"/>
            </td>
            <td style="padding:5px"><br>
              <a type="button" id="btn-atualizar" class="btn btn-primary" style="margin-bottom:10px">Filtrar</a>
            </td>
            <td style="padding:5px"><br>
              <a type="button" id="btn-adicionar" class="btn btn-primary" style="margin-bottom:10px">Adicionar</a>
            </td>
          </tr>
        </table>
      </form>
    </div>
    <div  width="100%"  style="padding-left:43%;">
      <div style="">
        {{ $titulos->render()}}
      </div>
    </div>
    <div class="panel-body">
      <table class="table table-striped table-responsive" style="font-size: 10pt; font-family: Verdana;">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Titulo</th>
            <th scope="col">Categoria</th>
            <th scope="col">Link</th>
            <th scope="col" style="width:10%">Cor</th>
            <th scope="col" style="width:10%">Editar</th>
            <th scope="col" style="width:10%">Inativar</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($titulos as $titulo){ ?>
            <tr>
              <td scope="row"><b><?php echo $titulo->titulo_id; ?></b> </td>
              <td><?php echo $titulo->descricao; ?></td>
              <td><?php echo $titulo->categoria; ?></td>
              <td><?php echo isset($titulo->link) ? substr($titulo->link,0,30).'...' : ''; ?></td>
              <td style="padding: 5px;">
                <div class="rounded" style="width:50% !important;height: 100% !important;background-color:<?php echo $titulo->cor; ?>;color:<?php echo $titulo->cor; ?>;">
                  <img style="<?php echo $titulo->cor; ?>" class="rounded-circle">
                  <h6><?php echo $titulo->cor; ?></h6>
                </div>
              </td>
              <td>
                <a type="button" title="Editar titulo" href="{{ URL::to('/titulo/'.$titulo->titulo_id) }}" class="btn btn-default">
                  <i class="fa fa-bars"></i></a>
                </td>
                <td>
                  <input type="button" class="btn btn-danger inativar-titulo" name="inativar-titulo" id="inativar-titulo" value=" Inativar" data-id="<?php echo $titulo->titulo_id ?>">
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
    <div  width="100%"  style="padding-left:43%;">
      <div style="">
        {{ $titulos->render()}}
      </div>
    </div>
  </div>
</div>


<script>
$(document).ready(function() {

  $('#btn-atualizar').click(function(){
    swal({
      title: 'Aguarde',
      html: 'Aguarde.',
      showCancelButton: false,
      showConfirmButton: false
    });

    $("#filtro").submit();

  });

  $("#btn-adicionar").click(function(){

    window.location = "{{ URL::to('/adiciona-titulo/') }}";
  })


  $(".inativar-titulo").click(function(){

    url =  "{{ URL::to('/inativar-titulo/') }}";

    var titulo_id = $(this).data('id');

    $.ajax({
      url: url,
      type: 'POST',
      data: {
        titulo_id: titulo_id,
        "_token":"{{ csrf_token() }}"
      },
      success: function(dataReturn, textStatus, xhr)
      {

        if(xhr.status == 202)
        {
          swal({
            title: "Atenção",
            text: dataReturn.mensagem,
            html: true,

          },  function () {
            location.reload(true);
            tr.hide();
            window.location = "{{ URL::to('/titulos/') }}";
          });

        }

      }

    });

  })

});

</script>

<style>

.btn-group
{
  margin-bottom:0px !important;
}

</style>

@endsection
