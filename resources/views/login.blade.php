@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Login</div>

        <div class="panel-body">
          <form class="form-horizontal" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}

            <div class="form-group">
              <label for="email" class="col-md-4 control-label">Usuário</label>

              <div class="col-md-6">
                <input id="usuario" type="text" class="form-control" name="usuario" value="">
              </div>
            </div>

            <div class="form-group">
              <label for="password" class="col-md-4 control-label">Senha</label>

              <div class="col-md-6">
                <input id="password" type="password" class="form-control" name="password" required>
              </div>
            </div>


            <div class="form-group">
              <div class="col-md-8 col-md-offset-4">
                <button type="button" class="btn btn-primary btn-login">
                  Login
                </button>
                <h6><span class="badge badge-danger msg-erro" style="background-color:red; display:none">Não foi possível autenticar o usuário.</span></h6>
              </div>

            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script>

$(document).ready(function() {

  $('.btn-login').click(function(){

    $btn = $(this);
    $usuario = $("#usuario").val();
    $pass = $("#password").val();

    $btn.attr('disabled',true);

    $('.msg-erro').hide();

    $url = "{{ URL::to('/autenticar') }}";

    $.ajax({
      url: $url,
      type: 'POST',
      data: {"_token":"{{ csrf_token() }}",
      pass:$pass,
      usuario:$usuario
    },
    datatype : "application/json",
    success: function(dataReturn, textStatus, xhr)
    {

      if(xhr.status == 200)
      {
        window.location.href = "{{ URL::to('chamados') }}";
        return true;
      } else if(xhr.status == 202){
        $('.msg-erro').html('Usuário sem grupo atribuido');
        $('.msg-erro').show();
        $('.msg-erro').attr('display','block');
        return false;
      }
      else
      {
      $('.msg-erro').html('Usuário ou senha incorreta');
      $('.msg-erro').show();
      $('.msg-erro').attr('display','block');
      return false;
    }

    /*  if(xhr.status == 203)
    {
    swal({
    title: "",
    text: "Cadastro não encontrado.",
    type: "info",
    showCancelButton: false,
    confirmButtonColor: "#0073e6",
    confirmButtonText: "Sim",
    closeOnConfirm: false,
    closeOnClickOutside: false,
    closeOnEsc: false

  })

  $btn.attr('disabled',false);
  return false;
}
else
{
window.location.href = "{{ URL::to('/formulario/loja/') }}"+'/'+$loja;
}*/
},
error: function(xhr, status, error) {

  $('.msg-erro').attr('display','block');
  $btn.attr('disabled',false);
  $('.msg-erro').show();

  /* swal('Erro.'+error.text);
  $btn.attr('disabled',false);
  return false; */
}
}).done(function() {
  $btn.attr('disabled',false);

});

})


});



</script>

@endsection
