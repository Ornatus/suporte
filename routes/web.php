<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  // return view('welcome');
  return redirect('logar');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/cadastro', 'HomeController@cadastro');
Route::post('/cadastro/salvar', 'HomeController@salvar');
Route::post('/cadastro/atualizar', 'HomeController@atualizar');
Route::get('/editar/{id}', 'HomeController@editar');
Route::get('/formulario/loja/{loja}', 'FormController@formulario');
Route::get('/formulario/entrar', 'FormController@entrar');
Route::post('/formulario/login', 'FormController@login');
Route::post('/formulario/atualizar', 'FormController@atualizar');
Route::post('/formulario/finalizar', 'FormController@finalizar');
Route::get('/formulario-view/loja/{loja}', 'FormController@formularioView');

//Suporte //chamado
Route::get('/logar', 'LoginController@login');
Route::post('/autenticar', 'LoginController@autenticar');
Route::post('/envia-mensagem', 'SuporteController@enviaMensagem')->middleware(['verificalogin','addlog']);
Route::post('/chamado-mensagem', 'SuporteController@chamadoMensagem');
Route::post('/chamado-arquivo', 'SuporteController@chamadoArquivo');
Route::post('/atender-chamado', 'SuporteController@atenderChamado')->middleware(['verificalogin','addlog']);
Route::any('/usuario-logado', 'SuporteController@isLogged');
Route::any('/logout', 'LoginController@logout');
Route::any('/sessao-expirada', 'LoginController@sessaoExpirada');
Route::any('/chamados', 'SuporteController@chamados')->middleware(['verificalogin','addlog'])->name('chamados');
Route::post('/solicita-analise', 'SuporteController@solicitaAnalise')->middleware(['verificalogin','addlog']);
Route::get('/chamado/{chamado_id}', 'SuporteController@chamado')->middleware(['verificalogin','addlog']);
Route::get('/openFile/{arquivo_id}', 'SuporteController@openFile');
Route::post('/uploadFile', 'SuporteController@uploadFile');
Route::post('/finaliza-chamado', 'SuporteController@finalizaChamado')->middleware(['verificalogin','addlog']);
Route::post('/retorna_bolsao', 'SuporteController@retornaBolsao')->middleware(['verificalogin','addlog'])->name('retorna_bolsao');
Route::post('/negar-analise', 'SuporteController@negarAnalise')->middleware(['verificalogin','addlog']);

//Painel
Route::get('/painel', 'SuporteController@painel');
Route::get('/atualiza-painel', 'SuporteController@atualizaPainel');

//grupo
Route::post('/cadastra-grupo', 'GrupoController@cadastraGrupo')->middleware(['verificalogin','addlog'])->name('cadastra-grupo');
Route::get('/adiciona-grupo', 'GrupoController@adicionaGrupo')->middleware(['verificalogin','addlog'])->name('adiciona-grupo');
Route::post('/remover-categoria', 'GrupoController@dropCategoriaGrupo')->middleware(['verificalogin','addlog'])->name('remover-categoria');
Route::post('/adiciona-categoria', 'GrupoController@setCategoria')->middleware(['verificalogin','addlog'])->name('adiciona-categoria');
Route::get('/lista-categoria-grupo', 'GrupoController@getCategoriaGrupo')->middleware(['verificalogin','addlog'])->name('lista-categoria-grupo');
Route::get('/grupos', 'GrupoController@grupos')->middleware(['verificalogin','addlog'])->name('grupos');
Route::get('/grupo/{grupo_id}', 'GrupoController@grupo')->middleware(['verificalogin','addlog'])->name('grupo');
Route::post('/atualiza-grupo', 'GrupoController@atualizaGrupo')->middleware(['verificalogin','addlog']);

// categoria
Route::post('/cadastra-categoria', 'CategoriaController@cadastraCategoria')->middleware(['verificalogin','addlog'])->name('cadastra-categoria');
Route::get('/adiciona-categoria', 'CategoriaController@adicionaCategoria')->middleware(['verificalogin','addlog'])->name('adiciona-categoria');
Route::get('/categorias', 'CategoriaController@categorias')->middleware(['verificalogin','addlog'])->name('categorias');
Route::get('/categoria/{categoria_id}', 'CategoriaController@categoria')->middleware(['verificalogin','addlog'])->name('categoria');
Route::post('/atualiza-categoria', 'CategoriaController@atualizaCategoria')->middleware(['verificalogin','addlog']);
Route::post('/atualiza-chamado', 'SuporteController@atualizaChamado')->middleware(['verificalogin','addlog']);

// titulo
Route::post('/cadastra-titulo', 'TituloController@cadastraTitulo')->middleware(['verificalogin','addlog'])->name('cadastra-titulo');
Route::get('/adiciona-titulo', 'TituloController@adicionaTitulo')->middleware(['verificalogin','addlog'])->name('adiciona-titulo');
Route::get('/titulos', 'TituloController@titulos')->middleware(['verificalogin','addlog'])->name('titulos');
Route::get('/titulo/{titulo_id}', 'TituloController@titulo')->middleware(['verificalogin','addlog'])->name('titulo');
Route::post('/atualiza-titulo', 'TituloController@atualizaTitulo')->middleware(['verificalogin','addlog']);
Route::post('/inativar-titulo', 'TituloController@inativaTitulo')->middleware(['verificalogin','addlog']);


// usuario
Route::post('/cadastra-usuario', 'UsuarioController@cadastraUsuario')->middleware(['verificalogin','addlog'])->name('cadastra-usuario');
Route::get('/adiciona-usuario', 'UsuarioController@adicionaUsuario')->middleware(['verificalogin','addlog'])->name('adiciona-usuario');
Route::get('/usuarios', 'UsuarioController@usuarios')->middleware(['verificalogin','addlog'])->name('usuarios');
Route::get('/usuario/{usuario_id}', 'UsuarioController@usuario')->middleware(['verificalogin','addlog'])->name('usuario');
Route::post('/atualiza-usuario', 'UsuarioController@atualizaUsuario')->middleware(['verificalogin','addlog']);

//relatório titulo
Route::get('/relatorio-titulo', 'RelatorioController@getCategoriasTitulo')->middleware(['verificalogin','addlog'])->name('relatorio-titulo');
Route::get('/lista-relatorio-titulo', 'RelatorioController@getFilterRelatorioTitulo')->middleware(['verificalogin','addlog'])->name('lista-relatorio-titulo');
//relatorio area
Route::get('/relatorio-area', 'RelatorioController@getLojas')->middleware(['verificalogin','addlog'])->name('relatorio-area');
Route::get('/lista-relatorio-area', 'RelatorioController@getFilterRelatorioArea')->middleware(['verificalogin','addlog'])->name('lista-relatorio-area');
//relatorio atendente
Route::get('/relatorio-usuario', 'RelatorioController@getUsuarios')->middleware(['verificalogin','addlog'])->name('relatorio-usuario');
Route::get('/lista-relatorio-usuario', 'RelatorioController@getFilterRelatorioUsuarios')->middleware(['verificalogin','addlog'])->name('lista-relatorio-usuario');
//relatorio Chamados
Route::get('/relatorio-chamado', 'RelatorioController@getChamados')->middleware(['verificalogin','addlog'])->name('relatorio-chamado');
Route::get('/excel-relatorio-chamado', 'RelatorioController@excelRelatorioChamado')->middleware(['verificalogin','addlog'])->name('excel-relatorio-chamado');
//relatório Backups
Route::get('/relatorio-backup', 'RelatorioController@getBackups')->middleware(['verificalogin','addlog'])->name('relatorio-backup');
Route::get('/excel-relatorio-backup', 'RelatorioController@excelRelatorioBackups')->middleware(['verificalogin','addlog'])->name('excel-relatorio-backup');
