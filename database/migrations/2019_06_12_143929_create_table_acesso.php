<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAcesso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('acesso', function (Blueprint $table) {
             $table->increments('id');
             $table->string('codigo_loja')->nullable();
             $table->string('email',200)->nullable();
             $table->string('acao',20)->nullable();
             $table->string('valor')->nullable();
             $table->timestamp('data_acesso')->useCurrent();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('acesso');
     }
}
