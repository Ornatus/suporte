<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableListaCadastro extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('lista_cadastro', function (Blueprint $table) {
             $table->increments('id');
             $table->string('codigo_loja',11);
             $table->string('data_instalacao',20);
             $table->string('data_limite',20);
             $table->string('email_responsavel',200);
             $table->string('email_contador',200);
             $table->integer('user_id');
             $table->timestamp('data_cadastro')->useCurrent();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('lista_cadastro');
     }
}
