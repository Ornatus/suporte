<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateTableLojas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('loja', function (Blueprint $table) {
             $table->increments('id');
             $table->string('codigo',11);
             $table->string('nome',200);
             $table->string('email',200);
             $table->string('ppe_access',2);
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('loja');
     }
}
