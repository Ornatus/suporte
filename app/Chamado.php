<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Chamado;

class Chamado extends Model
{
  protected $fillable = ['tipo_usuario', 'usuario_id', 'atendente_id', 'descricao', 'categoria_id', 'data_chamado', 'data_atribuido', 'data_resolvido', 'status_id','ativo','solicitante', 'telefone'];
  protected $table = 'chamado';
  public $timestamps = false;


  static function getChamadosPainel()
  {
    $sql = "SELECT
    s.descricao as status,
    s.cor as status_cor,
    c.nome as brazil_store_name,
    c.codigo as brazil_store_id,
    ct.descricao as categoria,
    ct.departamento,
    ch.*,
    su.firstname as atendente_nome
    FROM suporte.chamado ch
    JOIN suporte.vw_usuarios c on c.usuario_id = ch.usuario_id and c.tipo_usuario = ch.tipo_usuario
    JOIN suporte.status s on s.status_id = ch.status_id
    JOIN suporte.categoria ct on ct.categoria_id = ch.categoria_id
    LEFT JOIN suporte.user su ON su.user_id = ch.atendente_id
    WHERE ch.ativo = 1
    AND atendente_id is null
    ORDER BY ch.chamado_id DESC";

    return (array)DB::select($sql);
  }

  static function getUltimoChamado()
  {
    $sql = "SELECT
    max(ch.chamado_id) as ultimo_chamado
    FROM suporte.chamado ch
    JOIN suporte.vw_usuarios c on c.usuario_id = ch.usuario_id and c.tipo_usuario = ch.tipo_usuario
    JOIN suporte.status s on s.status_id = ch.status_id
    JOIN suporte.categoria ct on ct.categoria_id = ch.categoria_id
    LEFT JOIN suporte.user su ON su.user_id = ch.atendente_id
    WHERE ch.ativo = 1 AND atendente_id is null
    ORDER BY ch.chamado_id DESC";

    return (array)DB::select($sql);
  }

  static function getChamadosUsuarioGrupo($data)
  {
    $sql = "
    SELECT * FROM (
      SELECT
      s.descricao as status,
      s.cor as status_cor,
      c.nome as brazil_store_name,
      c.codigo as brazil_store_id,
      ct.descricao as categoria,
      ct.cor as cor_categoria,
      ch.ativo,
      ch.descricao,
      ch.data_chamado,
      ch.resolucao,
      pr.cor as corprioridade,
      pr.descricao as prioridade,
      t.cor as cortitulo,
      t.descricao as titulo,
      ch.status_id,
      ch.retorno,
      ch.categoria_id,
      ch.telefone,
      ch.solicitante,
      ch.chamado_id,
      ch.atendente_id,
      ct.is_semaphore,
      TIMEDIFF(CURRENT_TIMESTAMP(),ch.data_chamado) as dif_time,
      CASE WHEN ch.data_resolvido is not null
      THEN TIMEDIFF(ch.data_resolvido, ch.data_chamado)
      ELSE TIMEDIFF(current_timestamp, ch.data_chamado)
      END AS tempo_aberto,
      su.nome as atendente_nome
      FROM suporte.chamado ch
      JOIN suporte.vw_usuarios c on c.usuario_id = ch.usuario_id and c.tipo_usuario = ch.tipo_usuario
      JOIN suporte.status s on s.status_id = ch.status_id
      LEFT JOIN suporte.prioridade pr ON pr.prioridade_id = ch.prioridade_id
      LEFT JOIN suporte.titulo t ON t.titulo_id = ch.titulo_id
      JOIN suporte.categoria ct on ct.categoria_id = ch.categoria_id
      LEFT JOIN suporte.vw_usuarios su ON su.usuario_id = ch.atendente_id AND su.tipo_usuario = 2
      ) as chamados
      WHERE ativo = 1 ";


      if(isset($data['filtro_status']) )
      {
        if( $data['filtro_status'] == 'AA')
        {
          $sql .="AND status_id in (1,2) ";
        }

        if( (int)$data['filtro_status'] > 0 )
        {
          $sql .="AND status_id = '".$data['filtro_status']."' ";
        }

      }

      if(isset($data['filtro_livre']) && strlen($data['filtro_livre'])>0)
      {
        $sql .=" AND ( descricao like '%".$data['filtro_livre']."%' ";
          $sql .=" OR atendente_nome like '%".$data['filtro_livre']."%' ";
          $sql .=" OR categoria like '%".$data['filtro_livre']."%' ";
          $sql .=" OR prioridade like '%".$data['filtro_livre']."%' ";
          $sql .=" OR titulo like '%".$data['filtro_livre']."%' ";
          $sql .=" OR brazil_store_name like '%".$data['filtro_livre']."%' ) ";

        }

        $sql .="AND categoria_id in (SELECT categoria_id FROM grupo_categoria WHERE grupo_id = '".$data['usuario_configuracao']->grupo_id."' AND ativo = 1 ) ";

        if($data['filtro_chamado'] == 'grupo')
        {
          //  $sql .= "AND  atendente_id is null ";
        }
        else
        {
          $sql .= "AND  atendente_id = '".$data['usuario']->user_id."' ";
        }

        $sql .="ORDER BY ".$data['filtro_coluna']." ".$data['filtro_ordem'].";";

        return (array)DB::select($sql);
      }

      static function getChamadoStatus()
      {
        $sql = "SELECT * FROM status
        WHERE ativo = 1
        ORDER BY status_id;";

        return (array)DB::select($sql);
      }

      static function getChamadoPrioridade()
      {
        $sql = "SELECT * FROM prioridade
        WHERE ativo = 1
        ORDER BY prioridade_id;";

        return (array)DB::select($sql);
      }

      static function getChamadoCategoria()
      {
        $sql = "SELECT c.categoria_id, c.descricao, c.finaliza_chamado FROM categoria c
        JOIN grupo_categoria gc ON gc.categoria_id = c.categoria_id
        WHERE c.ativo = 1
        AND gc.ativo = 1 AND gc.grupo_id = '".$_SESSION['usuario_configuracao']->grupo_id."'
        ORDER BY categoria_id;";

        return (array)DB::select($sql);
      }

      static function getTitulosbyCategoriaid($categoria_id)
      {
        $sql = "SELECT * FROM titulo
        WHERE ativo = 1
        AND categoria_id = '".$categoria_id."'
        ORDER BY descricao;";

        return (array)DB::select($sql);
      }

      static function getChamadoAcao()
      {
        $sql = "SELECT * FROM acao
        WHERE ativo = 1
        ORDER BY acao_id;";

        return (array)DB::select($sql);
      }

      static function getChamadoTipo()
      {
        $sql = "SELECT * FROM tipo
        WHERE ativo = 1
        ORDER BY tipo_id;";

        return (array)DB::select($sql);
      }

      static function getChamadoById($data)
      {
        $sql = "SELECT
        s.descricao as status,
        s.cor as status_cor,
        c.nome as brazil_store_name,
        c.codigo as brazil_store_id,
        t.titulo_id as titulo,
        t.descricao as titulo_descricao,
        t.cor as titulo_cor,
        ct.descricao as categoria,
        ct.cor as cor_categoria,
        tp.descricao as tipo,
        pr.descricao as prioridade,
        pr.cor as corprioridade,
        ch.*,
        ct.finaliza_chamado,
        ct.atualiza_categoria,
        TIMEDIFF(current_timestamp, ch.data_chamado) as tempo_aberto,
        su.firstname as atendente_nome
        FROM suporte.chamado ch
        JOIN suporte.vw_usuarios c on c.usuario_id = ch.usuario_id and ch.tipo_usuario = c.tipo_usuario
        JOIN suporte.status s on s.status_id = ch.status_id
        JOIN suporte.categoria ct on ct.categoria_id = ch.categoria_id
        LEFT JOIN suporte.titulo t on t.titulo_id = ch.titulo_id
        LEFT JOIN suporte.user su ON su.user_id = ch.atendente_id
        LEFT JOIN suporte.usuario_configuracao uc ON uc.usuario_id = ch.atendente_id
        LEFT JOIN suporte.tipo tp ON tp.tipo_id = ch.tipo_id
        LEFT JOIN suporte.prioridade pr ON pr.prioridade_id = ch.prioridade_id
        WHERE ch.ativo = 1
        AND ch.chamado_id = '".$data['chamado_id']."';";


        return (array)DB::select($sql);
      }



      static function getMensagensByChamadoId($data) {

        $sql = "SELECT * FROM suporte.chamado_mensagem cm
        JOIN suporte.vw_usuarios u ON u.usuario_id = cm.usuario_id AND u.tipo_usuario = cm.tipo_usuario_id
        LEFT JOIN suporte.mensagem_arquivo ma ON  cm.chamado_mensagem_id = ma.mensagem_id
        LEFT JOIN suporte.arquivos arq ON ma.arquivo_id = arq.arquivo_id
        WHERE cm.ativo = 1
        AND cm.chamado_id = '".$data['chamado_id']."'
        ORDER BY cm.chamado_id ASC, cm.chamado_mensagem_id DESC;";

        return (array)DB::select($sql);
      }

      static function salvaMensagem($data)
      {
        $sql = "INSERT INTO suporte.chamado_mensagem
        SET chamado_id = '".$data['chamado_id']."',
        mensagem = '".($data['mensagem'])."',
        usuario_id = '".$data['usuario']."',
        tipo_usuario_id = '".$data['tipo_usuario']."',
        ativo = '1' ;";

        $return =  DB::insert($sql);

        $id = DB::getPdo()->lastInsertId();

        $data['mensagem_id'] = $id;

        if(isset($data['arquivos'])){
          if( count($data['arquivos']) > 0)
          {
            $data['arquivo_id'] = $data['arquivos'][0];
            Chamado::saveFileByMessage($data);
            return $return;

          }
        }else{
          return $return;
        }

      }

      static function atribuirChamado($data)
      {
        $sql = "UPDATE suporte.chamado
        SET
        atendente_id = '".$data['usuario']."',
        retorno = 0,
        titulo_id = '".$data['titulo_id']."',
        tipo_id = '".$data['tipo_id']."',
        prioridade_id = '".$data['prioridade_id']."',
        categoria_id = '".$data['categoria_id']."',
        data_atribuido = current_timestamp,
        status_id = 2
        WHERE
        chamado_id = '".$data['chamado_id']."';";

        return DB::update($sql);
      }


      static function finalizaChamado($data)
      {
        $sql = "UPDATE suporte.chamado
        SET
        data_resolvido = current_timestamp,
        resolucao = '".$data['resolucao']."',
        status_id = 3
        WHERE
        chamado_id = '".$data['chamado_id']."';";

        return DB::update($sql);
      }

      static function atualizarChamado($data)
      {
        $sql = "UPDATE suporte.chamado
        SET
        tipo_id = '".$data['tipo_id']."',
        prioridade_id = '".$data['prioridade_id']."',
        titulo_id = '".$data['titulo_id']."',
        categoria_id = '".$data['categoria_id']."'
        WHERE
        chamado_id = '".$data['chamado_id']."';";

        return DB::update($sql);
      }



      static function retornaBolsao($data)
      {
        $sql = "UPDATE suporte.chamado
        SET
        atendente_id = null,
        status_id = 1,
        retorno = 1
        WHERE
        chamado_id = '".$data['chamado_id']."';";

        return DB::update($sql);
      }

      static function getEmailClientbyId($chamado_id){
        $sql =   "SELECT distinct u.email
        from vw_usuarios u
        JOIN chamado c
        ON c.tipo_usuario = u.tipo_usuario
        AND u.usuario_id = c.usuario_id
        WHERE c.chamado_id = '".$chamado_id."' ;";

        return (array)DB::select($sql);
      }

      static function getCategoryNamebyId($categoria_id){
        $sql =   "SELECT  descricao
        from suporte.categoria
        WHERE categoria_id  = '".$categoria_id."';";

        return (array)DB::select($sql);
      }

      static function getCategoryIdByIdChamado($chamado_id){
        $sql =   "SELECT categoria_id
        from suporte.chamado
        WHERE chamado_id = '".$chamado_id."';";

        return (array)DB::select($sql);
      }

      static function getArquivoByChamadoId($chamado_id){
        $sql =   "SELECT  arq.* from chamado_arquivo as ch_a
        JOIN arquivos as arq ON arq.arquivo_id = ch_a.arquivo_id
        JOIN chamado as ch ON ch.chamado_id = ch_a.chamado_id
        where ch_a.chamado_id = ch.chamado_id
        AND ch_a.chamado_id =  '".$chamado_id."';";

        return (array)DB::select($sql);

      }

      static function getFileById($arquivo_id){
        $sql =   "SELECT *
        from arquivos
        where arquivo_id = '".$arquivo_id."';";

        return (array)DB::select($sql);

      }

      static function saveFile($data){
        $sql = "INSERT INTO suporte.arquivos SET
        name = '".$data['file']['name']."',
        type = '".$data['file']['type']."',
        tmp_name = '".$data['file']['tmp_name']."',
        error = '".$data['file']['error']."',
        size = '".$data['file']['size']."',
        extension = '".$data['file']['extension']."',
        base64 = '".$data['file']['base64']."'
        ;";

        DB::insert($sql);
        $id = DB::getPdo()->lastInsertId();
        return $id;

      }

      static function saveFileByMessage($data){

        if( $data['arquivo_id'] > 0)
        {
          $sql = "INSERT INTO
          suporte.mensagem_arquivo SET
          mensagem_id = '".$data['mensagem_id']."',
          arquivo_id = '".$data['arquivo_id']."',
          ativo = '1';";

          return (array)DB::insert($sql);
        }

      }

      static function solicitaAnalise($chamado_id)
      {
        $sql = "UPDATE suporte.chamado
        SET
        status_id = 4
        WHERE
        chamado_id = '".$chamado_id."';";

        return DB::update($sql);
      }

      static function getEmailSuporte(){
        $sql =   "SELECT u.email
        from usuario_configuracao as uc
        JOIN user as u ON u.user_id = uc.usuario_id
        and uc.aprovar_chamado = 1 and uc.ativo = 1;";

        return (array)DB::select($sql);

      }

      static function negarAnalise($chamado_id)
      {
        $sql = "UPDATE suporte.chamado
        SET
        status_id = 2
        WHERE
        chamado_id = '".$chamado_id."';";

        return DB::update($sql);
      }


      static function getChamados($data)
      {
        $sql = "SELECT
          ch.chamado_id,
          s.descricao as status,
          c.nome as brazil_store_name,
          ct.descricao as categoria,
          ch.ativo,
          ch.descricao,
          pr.descricao as prioridade,
          t.cor as cortitulo,
          t.descricao as titulo,
          ch.atendente_id,
          ch.data_chamado,
          ch.data_atribuido,
          ch.data_resolvido,
          CASE WHEN ch.data_resolvido is not null
          THEN TIMEDIFF(ch.data_resolvido, ch.data_chamado)
          ELSE TIMEDIFF(current_timestamp, ch.data_chamado)
          END AS tempo_aberto,
          TIMEDIFF(ch.data_atribuido,ch.data_chamado)
          AS tempo_ate_atendimento,
          su.nome as atendente_nome
          FROM suporte.chamado ch
          JOIN suporte.vw_usuarios c on c.usuario_id = ch.usuario_id and c.tipo_usuario = ch.tipo_usuario
          JOIN suporte.status s on s.status_id = ch.status_id
          LEFT JOIN suporte.prioridade pr ON pr.prioridade_id = ch.prioridade_id
          LEFT JOIN suporte.titulo t ON t.titulo_id = ch.titulo_id
          JOIN suporte.categoria ct on ct.categoria_id = ch.categoria_id
          LEFT JOIN suporte.vw_usuarios su ON su.usuario_id = ch.atendente_id AND su.tipo_usuario = 2
           WHERE 1 ";

          if(isset($data['data_min']) && $data['data_min'] != ''){
          $sql.=" AND ch.data_chamado >= '".$data['data_min']."' ";
          $sql.=" AND ch.data_chamado <= '".$data['data_max']."' ";
          }

          $sql.=" ORDER BY ch.chamado_id ".$data['filter_order_direction']." ";

          return (array)DB::select($sql);
        }

      }
