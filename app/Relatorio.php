<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;


class Relatorio extends Authenticatable
{
  use Notifiable;

  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    'name', 'email', 'password',
  ];

  /**
  * The attributes that should be hidden for arrays.
  *
  * @var array
  */
  protected $hidden = [
    'password', 'remember_token',
  ];

  static function getCategorias()
  {
    $sql = "SELECT cat.categoria_id,cat.descricao
    from suporte.categoria as cat";

    return (array)DB::select($sql);
  }

  static function getFilterRelatorioTitulo($data)
  {

    $sql = "SELECT t.titulo_id,t.descricao,count(t.descricao) as quantidade
    FROM suporte.titulo as t
    JOIN suporte.chamado as ch ON t.titulo_id = ch.titulo_id
    JOIN suporte.categoria as cat ON cat.categoria_id = t.categoria_id
    WHERE 1 ";

    if($data['categoria'] != 0){
      $sql .=" AND t.categoria_id ='".$data['categoria']."' ";
    }

    if(!empty($data['data_min'])){
      $sql.=" AND ch.data_chamado >= '".$data['data_min']."' " ;
    }

    if(!empty($data['data_max'])){
      $sql.=" AND ch.data_chamado <= '".$data['data_max']."' " ;
    }

    $sql .= 'group by 1,2';

    if($data['filter_order'] !=0){
      if($data['filter_order'] == 1){
        $order_by = 't.descricao';
      }else{
        $order_by = 'quantidade';
      }
      if($data['filter_order_direction'] !=0){
        if($data['filter_order_direction'] == 1){
          $sql.=" ORDER BY ".$order_by." ASC " ;
        }else{
          $sql.=" ORDER BY  ".$order_by." DESC " ;
        }
      }
    }

    return (array)DB::select($sql);
  }

  //area

  static function getLojas()
  {
    $sql = "SELECT distinct vw.usuario_id,vw.nome
    FROM suporte.chamado as ch
    JOIN vw_usuarios as vw
    ON ch.usuario_id = vw.usuario_id
    WHERE 1 AND ch.tipo_usuario = vw.tipo_usuario ";;

    return (array)DB::select($sql);
  }

  static function getFilterRelatorioArea($data)
  {

    $sql = "SELECT vw.nome as loja,count(ch.chamado_id) as quantidade_chamados
    FROM suporte.chamado as ch
    JOIN vw_usuarios as vw
    ON ch.usuario_id = vw.usuario_id
    WHERE 1";

    if($data['loja_id'] != 0){
      $sql .=" AND vw.usuario_id ='".$data['loja_id']."' ";
    }

    if(!empty($data['data_min'])){
      $sql.=" AND ch.data_chamado >= '".$data['data_min']."' " ;
    }

    if(!empty($data['data_max'])){
      $sql.=" AND ch.data_chamado <= '".$data['data_max']."' " ;
    }

    $sql .= " AND ch.tipo_usuario = vw.tipo_usuario ";

    $sql .= ' group by 1 ';

    switch ($data['filter_order'] !=0) {
      case $data['filter_order'] ==1:
      $order_by = 'loja';
      break;
      case $data['filter_order'] ==2:
      $order_by = 'quantidade_chamados';
      break;
      default:
      $order_by = ' 1 ';
      break;

    }

    if($data['filter_order_direction'] !=0){
      if($data['filter_order_direction'] == 1){
        $sql.=" ORDER BY ".$order_by." ASC " ;
      }else{
        $sql.=" ORDER BY  ".$order_by." DESC " ;
      }
    }

    return (array)DB::select($sql);
  }

  //usuarios

  static function getUsuarios()
  {
    $sql = "SELECT distinct u.user_id,u.firstname from suporte.chamado as ch
    JOIN suporte.user as u
    ON ch.atendente_id = u.user_id; ";;

    return (array)DB::select($sql);
  }

  static function getFilterRelatorioUsuarios($data)
  {

    $sql = "SELECT
    u.firstname as usuario,
    count(ch.chamado_id) as quantidade_chamado,
    AVG(TIMESTAMPDIFF(MINUTE,ch.data_atribuido,ch.data_resolvido)  / 60) as tempo_medio
    from suporte.chamado as ch
    JOIN suporte.user as u
    ON ch.atendente_id = u.user_id
    WHERE 1";

    if($data['usuario_id'] == 0){
      $sql .=" ";
    }else{
      $sql .=" AND u.user_id ='".$data['usuario_id']."' ";

    }

    if(!empty($data['data_min'])){
      $sql.=" AND ch.data_chamado >= '".$data['data_min']."' " ;
    }

    if(!empty($data['data_max'])){
      $sql.=" AND ch.data_chamado <= '".$data['data_max']."' " ;
    }

    if($data['categoria'] == 0){
      $sql .= " ";
    }else{
      $sql .= " AND ch.categoria_id ='".$data['categoria']."' ";
    }
    $sql .= " AND ch.status_id = 3 ";

    switch ($data['filter_order'] !=0) {
      case $data['filter_order'] ==1:
      $order_by = 'usuario';
      break;
      case $data['filter_order'] ==2:
      $order_by = 'quantidade_chamado';
      break;
      default:
      $order_by = ' 1 ';
      break;

    }

    $sql .= ' group by 1 ';

    if($data['filter_order_direction'] !=0){
      if($data['filter_order_direction'] == 1){
        $sql.=" ORDER BY ".$order_by." ASC " ;
      }else{
        $sql.=" ORDER BY  ".$order_by." DESC " ;
      }
    }

    return (array)DB::select($sql);
  }


  static function getCustomers()
  {

    $sql = "SELECT
    customer_id,
    brazil_store_id,
    brazil_store_name
    FROM customer
    WHERE ppe_access = 1
    ORDER BY brazil_store_name ASC";

    return (array)DB::select($sql);
  }


}
