<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class updateTablesBaloneCustomerSuport extends Command
{
  /**
  * The name and signature of the console command.
  *
  * @var string
  */
  protected $signature = 'command:updateTablesBaloneCustomerSuport';

  /**
  * The console command description.
  *
  * @var string
  */
  protected $description = 'Command to update tables balone_customer in Data Base Suporte';

  /**
  * Create a new command instance.
  *
  * @return void
  */
  public function __construct()
  {
    parent::__construct();
  }

  /**
  * Execute the console command.
  *
  * @return mixed
  */
  public function handle()
  {

    //Query para coletar todos os registro da tabela customers de homologação, mudar para produção
    $sql ="SELECT
    *
    FROM morana2.balone_customer;
    ";

    //Homolog, depois mudar ENV mysql2 para produção.
    $customers_prod = DB::connection('mysql2')->select($sql);

    DB::table('suporte.balone_customer')->truncate();

    $sql = "";

    foreach ($customers_prod as $customer) {

      $customer->group_store_id = isset($customer->group_store_id) ? $customer->group_store_id : 0;
      $customer->notification = isset($customer->notification) ? $customer->notification : 0;
      $customer->cellphone = isset($customer->cellphone) ? $customer->cellphone : 0;
      $customer->supervisor_id = isset($customer->supervisor_id) ? $customer->supervisor_id : 0;
      $customer->customer_profile_id = isset($customer->customer_profile_id) ? $customer->customer_profile_id : 0;

      if($customer->date_added == '0000-00-00 00:00:00'){
        $customer->date_added ='1970-01-01 00:00:00';
      }

      $sql = "INSERT INTO suporte.balone_customer
      SET
      customer_id = '".$customer->customer_id."',
      brazil_store_id = '".$customer->brazil_store_id."',
      brazil_store_name = '".$customer->brazil_store_name."',
      store_id = '".$customer->store_id."',
      type = '".$customer->type."',
      firstname = '".$customer->firstname."',
      lastname = '".$customer->lastname."',
      email = '".$customer->email."',
      telephone = '".$customer->telephone."',
      fax = '".$customer->fax."',
      password = '".$customer->password."',
      salt = '".$customer->salt."',
      cart = '".$customer->cart."',
      wishlist = '".$customer->wishlist."',
      newsletter = '".$customer->newsletter."',
      address_id = '".$customer->address_id."',
      customer_group_id = '".$customer->customer_group_id."',
      ip = '".$customer->ip."',
      status = '".$customer->status."',
      approved = '".$customer->approved."',
      token = '".$customer->token."',
      date_added = '".$customer->date_added."',
      master_customer_id = '".$customer->master_customer_id."',
      supervisor_id = '".$customer->supervisor_id."',
      ppe_access_days = '".$customer->ppe_access_days."',
      customer_profile_id = '".$customer->customer_profile_id."',
      ppe_access = '".$customer->ppe_access."',
      is_debitor = '".$customer->is_debitor."',
      notification = '".$customer->notification."',
      cellphone = '".$customer->cellphone."',
      code = '".$customer->code."',
      group_store_id = '".$customer->group_store_id."',
      manager_password = '".$customer->manager_password."' ;
      ";

      $query = DB::insert($sql);

      if($query <=0 || $query == '' ){
        print_R("Algum erro ocorreu ao inserir customer: '".$customer->brazil_store_id."'\n");die;
      }else{
        print_r("Inserido customer:'".$customer->customer_id."'\n");
      }
    }



  }
}
