<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;


class User extends Authenticatable
{
  use Notifiable;

  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    'name', 'email', 'password',
  ];

  /**
  * The attributes that should be hidden for arrays.
  *
  * @var array
  */
  protected $hidden = [
    'password', 'remember_token',
  ];

  static function getUser($id)
  {
    $sql = "SELECT * FROM users WHERE id = '".$id."'";

    return (array)DB::select($sql);
  }

  static function autenticar($data)
  {
    $response = true;
    $request = new Request;

    $sql = "SELECT * FROM
    user
    WHERE username = '".$data['usuario']."'
    AND password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('".$data['pass']."')))));";

    $query = (array)DB::select($sql);

    return $query;
  }

  static function configuracao($data)
  {
    $sql = "SELECT
    u.username,
    u.email,
    g.descricao as grupo,
    g.grupo_id as grupo_id,
    uc.aprovar_chamado
    FROM user u
    JOIN usuario_grupo ug ON ug.usuario_id = u.user_id AND ug.ativo = 1
    JOIN grupo g ON g.grupo_id = ug.grupo_id
    LEFT JOIN  usuario_configuracao as uc ON uc.usuario_id = u.user_id and uc.ativo = 1
    WHERE u.user_id = '".$data->user_id."';";

    $query = (array)DB::select($sql);

    return $query;
  }


  static function cadastrarUsuario($data)
  {
    $sql = "INSERT INTO suporte.usuario
    SET
    descricao = '".$data['descricao']."',
    cor = '".$data['cor']."',
    categoria_id = '".$data['categoria']."',
    ativo = '1' ;";

    return DB::insert($sql);
  }

  static function getUsuarios($data)
  {
    $sql = "SELECT
    u.user_id,
    u.username,
    u.firstname,
    u.lastname,
    u.email,
    g.descricao,
    ug.usuario_grupo_id
    FROM suporte.user as u
    LEFT JOIN suporte.usuario_grupo as ug
    ON u.user_id = ug.usuario_id AND ug.ativo = 1
    LEFT JOIN suporte.grupo as g
    ON ug.grupo_id = g.grupo_id
    WHERE 1";

    if(isset($data['filtro_usuario'])){
      $sql.=" AND u.username like '%".$data['filtro_usuario']."%'";

    }

    $sql.="ORDER BY u.user_id ASC";

    return (array)DB::select($sql);
  }

  static function getUsuarioById($data)
  {
    $sql = "SELECT
    u.user_id,
    u.username,
    u.firstname,
    u.lastname,
    u.email,
    u.user_group_id,
    g.descricao,
    ug.usuario_grupo_id,
    uc.aprovar_chamado,
    g.grupo_id
    FROM suporte.user as u
    LEFT JOIN suporte.usuario_grupo as ug  ON u.user_id = ug.usuario_id AND ug.ativo = 1
    LEFT JOIN suporte.usuario_configuracao as uc ON uc.usuario_id = u.user_id AND uc.ativo =1
    LEFT JOIN suporte.grupo as g ON ug.grupo_id = g.grupo_id
    WHERE u.user_id = '".$data['usuario_id']."'";

    return (array)DB::select($sql);
  }

  static function atualizarUsuario($data)
  {

    $sql = "UPDATE suporte.usuario_configuracao
    SET
    ativo = 0
    WHERE
    usuario_id = '".$data['usuario_id']."';";

    DB::update($sql);

    $sql = "INSERT INTO suporte.usuario_configuracao
    SET
    ativo = 1,
    usuario_id = '".$data['usuario_id']."',
    aprovar_chamado = '".$data['aprovar_chamado']."';";

    DB::insert($sql);


    $sql = "UPDATE suporte.usuario_grupo
    SET
    ativo = 0
    WHERE
    usuario_id = '".$data['usuario_id']."'";

    DB::update($sql);

    $sql = "INSERT INTO suporte.usuario_grupo
    SET
    ativo = 1,
    usuario_id = '".$data['usuario_id']."',
    grupo_id = '".$data['grupo']."';";

    return DB::insert($sql);
  }


  static function getUsuariosUsuarioGrupo($data)
  {
    $sql = "
    SELECT * FROM (
      SELECT
      s.descricao as status,
      s.cor as status_cor,
      c.nome as brazil_store_name,
      c.codigo as brazil_store_id,
      ct.descricao as usuario,
      ct.cor as cor_usuario,
      ch.ativo,
      ch.descricao,
      ch.data_chamado,
      ch.resolucao,
      ch.status_id,
      ch.usuario_id,
      ch.categoria_id,
      ch.chamado_id,
      ch.atendente_id,
      CASE WHEN ch.data_resolvido is not null
      THEN TIMEDIFF(ch.data_resolvido, ch.data_chamado)
      ELSE TIMEDIFF(current_timestamp, ch.data_chamado)
      END AS tempo_aberto,
      su.nome as atendente_nome
      FROM suporte.chamado ch
      JOIN suporte.vw_usuarios c on c.usuario_id = ch.usuario_id and c.tipo_usuario = ch.tipo_usuario
      JOIN suporte.status s on s.status_id = ch.status_id
      JOIN suporte.usuario ct on ct.usuario_id = ch.usuario_id
      LEFT JOIN suporte.vw_usuarios su ON su.usuario_id = ch.atendente_id AND su.tipo_usuario = 2
      ) as chamados
      WHERE ativo = 1 ";


      if(isset($data['filtro_status']) )
      {
        if( $data['filtro_status'] == 'AA')
        {
          $sql .="AND status_id in (1,2) ";
        }

        if( (int)$data['filtro_status'] > 0 )
        {
          $sql .="AND status_id = '".$data['filtro_status']."' ";
        }

      }

      if(isset($data['filtro_livre']) && strlen($data['filtro_livre'])>0)
      {
        $sql .=" AND ( descricao like '%".$data['filtro_livre']."%' ";
          $sql .=" OR atendente_nome like '%".$data['filtro_livre']."%' ";
          $sql .=" OR usuario like '%".$data['filtro_livre']."%' ";
          $sql .=" OR brazil_store_name like '%".$data['filtro_livre']."%' ) ";
        }

        $sql .="AND usuario_id in (SELECT usuario_id FROM grupo_usuario WHERE grupo_id = '".$data['usuario_configuracao']->grupo_id."') ";

        if($data['filtro_chamado'] == 'grupo')
        {
          //  $sql .= "AND  atendente_id is null ";
        }
        else
        {
          $sql .= "AND  atendente_id = '".$data['usuario']->user_id."' ";
        }

        $sql .="ORDER BY ".$data['filtro_coluna']." ".$data['filtro_ordem'].";";

        return (array)DB::select($sql);
      }

      static function getUsuarioCategoria()
      {
        $sql = "SELECT c.categoria_id, c.descricao FROM categoria c
        JOIN grupo_categoria gc ON gc.categoria_id = c.categoria_id
        WHERE c.ativo = 1
        AND gc.ativo = 1 AND gc.grupo_id = '".$_SESSION['usuario_configuracao']->grupo_id."'
        ORDER BY categoria_id;";

        return (array)DB::select($sql);
      }




    }
