<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Categoria extends Model
{
  protected $fillable = ['categoria_id','descricao','cor','ativo','departamento'];
  protected $table = 'categoria';
  public $timestamps = false;


  static function cadastrarCategoria($data)
  {
    $sql = "INSERT INTO suporte.categoria
    SET
    descricao = '".$data['descricao']."',
    cor = '".$data['cor']."',
    departamento = '".$data['departamento']."',
    is_semaphore = '".$data['is_semaphore']."',
    ativo = '1' ;";

    return DB::insert($sql);
  }

  static function getCategorias($data)
  {
    $sql = "SELECT
    descricao,
    cor,
    categoria_id,
    departamento
    FROM suporte.categoria
    WHERE ativo = 1 ";

    if(isset($data['filtro_categoria'])){
      $sql.=" AND descricao like '%".$data['filtro_categoria']."%'";

    }

    $sql.="ORDER BY departamento,categoria_id ASC";


    return (array)DB::select($sql);
  }

  static function getCategoriaById($data)
  {
    $sql = "SELECT
    descricao,
    cor,
    categoria_id,
    finaliza_chamado,
    departamento,
    is_semaphore
    FROM suporte.categoria
    WHERE ativo = 1
    AND categoria_id = '".$data['categoria_id']."'
    ORDER BY categoria_id DESC";

    return (array)DB::select($sql);
  }

  static function atualizarCategoria($data)
  {
    $sql = "UPDATE suporte.categoria
    SET
      cor = '".$data['cor']."',
      descricao = '".$data['descricao']."',
      finaliza_chamado = '".$data['finaliza_chamado']."',
      departamento = '".$data['departamento']."',
      is_semaphore = '".$data['is_semaphore']."'
    WHERE
      categoria_id = '".$data['categoria_id']."';";

    return DB::update($sql);
  }


  static function getCategoriasUsuarioGrupo($data)
  {
    $sql = "
    SELECT * FROM (
            SELECT
            s.descricao as status,
            s.cor as status_cor,
            c.nome as brazil_store_name,
            c.codigo as brazil_store_id,
            ct.descricao as categoria,
            ct.cor as cor_categoria,
            ch.ativo,
            ch.descricao,
            ch.data_chamado,
            ch.resolucao,
            ch.status_id,
            ch.categoria_id,
            ch.chamado_id,
            ch.atendente_id,
            CASE WHEN ch.data_resolvido is not null
                  THEN TIMEDIFF(ch.data_resolvido, ch.data_chamado)
                  ELSE TIMEDIFF(current_timestamp, ch.data_chamado)
            END AS tempo_aberto,
            su.nome as atendente_nome
            FROM suporte.chamado ch
            JOIN suporte.vw_usuarios c on c.usuario_id = ch.usuario_id and c.tipo_usuario = ch.tipo_usuario
            JOIN suporte.status s on s.status_id = ch.status_id
            JOIN suporte.categoria ct on ct.categoria_id = ch.categoria_id
            LEFT JOIN suporte.vw_usuarios su ON su.usuario_id = ch.atendente_id AND su.tipo_usuario = 2
            ) as chamados
            WHERE ativo = 1 ";


    if(isset($data['filtro_status']) )
    {
      if( $data['filtro_status'] == 'AA')
      {
        $sql .="AND status_id in (1,2) ";
      }

      if( (int)$data['filtro_status'] > 0 )
      {
        $sql .="AND status_id = '".$data['filtro_status']."' ";
      }

    }

    if(isset($data['filtro_livre']) && strlen($data['filtro_livre'])>0)
    {
        $sql .=" AND ( descricao like '%".$data['filtro_livre']."%' ";
        $sql .=" OR atendente_nome like '%".$data['filtro_livre']."%' ";
        $sql .=" OR categoria like '%".$data['filtro_livre']."%' ";
        $sql .=" OR brazil_store_name like '%".$data['filtro_livre']."%' ) ";
    }

    $sql .="AND categoria_id in (SELECT categoria_id FROM grupo_categoria WHERE grupo_id = '".$data['usuario_configuracao']->grupo_id."') ";

    if($data['filtro_chamado'] == 'grupo')
    {
    //  $sql .= "AND  atendente_id is null ";
    }
    else
    {
      $sql .= "AND  atendente_id = '".$data['usuario']->user_id."' ";
    }

    $sql .="ORDER BY ".$data['filtro_coluna']." ".$data['filtro_ordem'].";";

    return (array)DB::select($sql);
  }


}
