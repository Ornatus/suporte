<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Loja extends Model
{

  protected $fillable = ['codigo', 'nome', 'email', 'ppe_access'];
  protected $table = 'table';
  public $timestamps = false;

  static function getListaLojas()
  {
    $sql = "SELECT * FROM loja WHERE ppe_access = 1 ORDER BY codigo ";

    return (array)DB::select($sql);
  }

  static function insertListaCadastro($data)
  {
    $sql = "INSERT INTO lista_cadastro (codigo_loja,data_instalacao,data_limite,email_responsavel, email_contador,user_id)
    VALUES('".$data['loja']."',
      '".$data['data_instalacao']."',
      '".$data['data_limite']."',
      '".$data['email_responsavel']."',
      '".$data['email_contador']."',
      '".$data['user_id']."'
    )";

    return (array)DB::insert($sql);
  }

  static function updateListaCadastro($data)
  {
    $sql = "UPDATE lista_cadastro
    SET
    data_instalacao = '".$data['data_instalacao']."',
    data_limite = '".$data['data_limite']."',
    email_responsavel = '".$data['email_responsavel']."',
    email_contador =  '".$data['email_contador']."',
    user_id =  '".$data['user_id']."'
    WHERE id =  '".$data['id']."';";

    return (array)DB::update($sql);
  }

  static function verificaListaCadastro($data)
  {
    $sql = "SELECT * FROM lista_cadastro WHERE codigo_loja = '".$data['loja']."';";

    return (array)DB::select($sql);
  }

  static function getListaCadastro()
  {
    $sql = "SELECT lc.*,l.*, lc.id as id_lista, dl.STATUS,
    (select id from acesso where codigo_loja = lc.codigo_loja AND email = lc.email_contador LIMIT 1) as acesso_contador,
    (select id from acesso where codigo_loja = lc.codigo_loja AND email = lc.email_responsavel LIMIT 1) as acesso_responsavel
    FROM lista_cadastro lc
    JOIN loja l on l.codigo = lc.codigo_loja
    LEFT JOIN dados_loja dl ON dl.CodArea = lc.codigo_loja
    ORDER BY lc.data_instalacao;";

    return (array)DB::select($sql);
  }

  static function getListaCadastroById($id)
  {
    $sql = "SELECT l.*, lc.* FROM lista_cadastro lc
    JOIN loja l on l.codigo = lc.codigo_loja
    WHERE lc.id = '".$id."';";

    return (array)DB::select($sql);
  }

  static function getLojaPeloCodigo($codigo)
  {
    $sql = "SELECT * FROM loja WHERE ppe_access = 1 AND codigo = '$codigo';";

    return (array)DB::select($sql);
  }

  static function getCadastroPeloCodigoLoja($codigo)
  {
    $sql = "SELECT * FROM dados_loja WHERE CodArea = '$codigo';";

    return (array)DB::select($sql);
  }

  static function verificaListaCadastroLogin($data)
  {
    $sql = "SELECT * FROM lista_cadastro
    WHERE codigo_loja = '".$data['loja']."'
    AND ( email_contador = '".$data['email']."' OR email_responsavel = '".$data['email']."') ;";

    return (array)DB::select($sql);
  }

  static function insertAcesso($loja, $email,$acao,$valor)
  {
    $sql = "INSERT INTO acesso (codigo_loja,email,acao,valor)
    VALUES('".$loja."',
      '".$email."',
      '".$acao."',
      '".$valor."'
    )";

    return (array)DB::insert($sql);
  }

  static function insereDadosFormulario($query)
  {
    $sql = "INSERT INTO dados_loja   SET ".$query." ;";



    return (array)DB::insert($sql);
  }

  static function atualizaDadosFormulario($loja, $query)
  {
    $sql = "UPDATE dados_loja
    SET ".$query.",
    DT_ULTIMA_ATUALIZACAO = current_timestamp
    WHERE CodArea = '".$loja."';";

    try {

      $retorno = (array)DB::insert($sql);

    } catch (Exception $e) {
      $retorno =  'Erro so inserir';
    }

    return $retorno;
  }

  static function atualizaSatatus($loja, $query)
  {
    $sql = "UPDATE dados_loja
              SET  DT_ULTIMA_ATUALIZACAO = current_timestamp, STATUS = 'C'
            WHERE CodArea = '".$loja."';";

            try {

              $retorno = (array)DB::insert($sql);

            } catch (Exception $e) {
              $retorno =  'Erro so inserir';
            }

    return $retorno;
  }

}
