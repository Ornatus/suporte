<?php

namespace App\Http\Middleware;

use Closure;

class verificaLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if(session_id()=='')
      {
        session_start();
      }

      if(! isset($_SESSION['usuario']))
      {
         return redirect('sessao-expirada');
      }

      return $next($request);
    }
}
