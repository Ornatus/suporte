<?php

namespace App\Http\Middleware;

use Closure;
use App\Log;

class addLog
{
  /**
  * Handle an incoming request.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \Closure  $next
  * @return mixed
  */
  public function handle($request, Closure $next)
  {

    if(isset($_SESSION['usuario']))
    {
      Log::setLog();

    }

    return $next($request);
  }
}
