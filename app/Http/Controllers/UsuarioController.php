<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Chamado;
use App\Grupo;
use App\User;
use Session;
use Illuminate\Support\Facades\Response;

class UsuarioController extends Controller
{
  function __construct()
  {
    if(session_id()=='')
    {
      session_start();
    }
  }

  function atualizaUsuario(Request $request)
  {
    $data = $_POST;
    
    $save = User::atualizarUsuario($data);

    return Response::json($save,200);
  }

  function adicionaUsuario(Request $request)
  {
    $data['usuario'] = $_SESSION['usuario'];
    $data['usuario_configuracao'] = $_SESSION['usuario_configuracao'];
    $data['categorias'] = Categoria::getCategorias($data);

    return view('adiciona-usuario', $data);
  }

  function cadastraUsuario(Request $request)
  {
    $data = $_POST;
    $data['usuario'] = $_SESSION['usuario']->user_id;

    $save = User::cadastrarUsuario($data);

    return Response::json($save,200);
  }


  function usuarios(Request $request)
  {
    $data = array();

    $data['filtro_usuario'] = '';

    if(isset($_REQUEST['filtro_usuario']))
    {
      $data['filtro_usuario'] = $_REQUEST['filtro_usuario'];
    }

    $_SESSION['filtro_usuario'] = $data['filtro_usuario'];

    $data['usuario'] = $_SESSION['usuario'];
    $data['usuario_configuracao'] = $_SESSION['usuario_configuracao'];

    $usuarios = User::getUsuarios($data);
    $data['usuarios']  = $this->arrayPaginator($usuarios, $request);

    return view('usuarios',$data);
  }

  public function arrayPaginator($array, $request)
  {

    $currentPage = LengthAwarePaginator::resolveCurrentPage();
    $page = $currentPage;
    $perPage = 25;
    $offset = ($page * $perPage) - $perPage;

    return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,['path' => $request->url(),'query' => $request->query()]);
  }

  function usuario(Request $request, $usuario_id)
  {
    $data = array();
    $data['usuario_id'] = $usuario_id;
    $data['usuario'] = $_SESSION['usuario'];
    $data['usuario_configuracao'] = $_SESSION['usuario_configuracao'];

    $data['user'] = User::getUsuarioById($data);

    $data['grupos'] = Grupo::getGrupos($data);

    return view('usuario',$data);
  }

  function isLogged()
  {
    if( isset($_SESSION['usuario']) && $_SESSION['usuario']->user_id > 0  )
    {
      return true;
    }
    else
    {
      return false;
    };
  }



}
