<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Chamado;
use App\Categoria;
use App\Titulo;
use App\User;
use Session;
use Illuminate\Support\Facades\Response;

class TituloController extends Controller
{
  function __construct()
  {
    if(session_id()=='')
    {
      session_start();
    }
  }

  function atualizaTitulo(Request $request)
  {
    $data = array();
    
    $data['titulo_id'] = isset($_POST['titulo_id']) ? $_POST['titulo_id'] :'' ;
    $data['descricao'] = isset($_POST['descricao']) ? $_POST['descricao'] :'' ;
    $data['categoria'] = isset($_POST['categoria']) ? $_POST['categoria'] : '';
    $data['link'] = isset($_POST['link']) ? $_POST['link'] :'' ;
    $data['cor'] = isset($_POST['cor']) ? $_POST['cor'] : '';
    $data['usuario'] = isset($_SESSION['usuario']->user_id) ? $_SESSION['usuario']->user_id: '';

    $save = Titulo::atualizarTitulo($data);

    return Response::json($save,200);
  }

  function adicionaTitulo(Request $request)
  {
    $data['usuario'] = $_SESSION['usuario'];
    $data['usuario_configuracao'] = $_SESSION['usuario_configuracao'];
    $data['categorias'] = Categoria::getCategorias($data);

    return view('adiciona-titulo', $data);
  }

  function cadastraTitulo(Request $request)
  {
    $data = array();

    $data['descricao'] = isset($_POST['descricao']) ? $_POST['descricao'] :'' ;
    $data['categoria'] = isset($_POST['categoria']) ? $_POST['categoria'] : '';
    $data['link'] = isset($_POST['link']) ? $_POST['link'] :'' ;
    $data['cor'] = isset($_POST['cor']) ? $_POST['cor'] : '';
    $data['usuario'] = isset($_SESSION['usuario']->user_id) ? $_SESSION['usuario']->user_id: '';

    $save = Titulo::cadastrarTitulo($data);

    return Response::json($save,200);
  }


  function titulos(Request $request)
  {
    $data = array();

    $data['filtro_titulo'] = isset($_SESSION['filtro_titulo']) ? $_SESSION['filtro_titulo'] : '';


    if(isset($_REQUEST['filtro_titulo']))
    {
      $data['filtro_titulo'] = $_REQUEST['filtro_titulo'];
    }

    $_SESSION['filtro_titulo'] = $data['filtro_titulo'];

    $data['usuario'] = $_SESSION['usuario'];
    $data['usuario_configuracao'] = $_SESSION['usuario_configuracao'];
    $titulos = Titulo::getTitulos($data);

    $data['titulos']  = $this->arrayPaginator($titulos, $request);

    return view('titulos',$data);
  }

  public function arrayPaginator($array, $request)
  {

    $currentPage = LengthAwarePaginator::resolveCurrentPage();
    $page = $currentPage;
    $perPage = 25;
    $offset = ($page * $perPage) - $perPage;

    return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,['path' => $request->url(),'query' => $request->query()]);
  }

  function titulo(Request $request, $titulo_id)
  {
    $data = array();
    $data['titulo_id'] = $titulo_id;
    $data['usuario'] = $_SESSION['usuario'];
    $data['usuario_configuracao'] = $_SESSION['usuario_configuracao'];

    $data['titulo'] = Titulo::getTituloById($data);
    $data['categorias'] = Categoria::getCategorias($data);

    return view('titulo',$data);
  }

  function isLogged()
  {
    if( isset($_SESSION['usuario']) && $_SESSION['usuario']->user_id > 0  )
    {
      return true;
    }
    else
    {
      return false;
    };
  }


  function inativaTitulo(Request $request)
  {
    $titulo_id = $_POST['titulo_id'];
    // verifica se existem chamados com o titulo, que ira ser inativado
    $chamados = Titulo::consultaTitulobyId($titulo_id);

    $save = array();

    if(count($chamados)  > 0){
      $save['quantidade'] = count($chamados);
      $save['chamados'] = $chamados;
      $save['mensagem'] = "Existem ". $save['quantidade'] ." chamados com este titulo: <br><br>";

      foreach ($save['chamados'] as $chamado) {
        $save['mensagem'] .= " [ ". $chamado->chamado_id . " ] ";
      }

    }else{
      Titulo::inativaTitulobyId($titulo_id);
      $save['mensagem'] = "Titulo Inativado";

    }

    return Response::json($save,202);
  }


}
