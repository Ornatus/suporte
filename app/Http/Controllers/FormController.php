<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Loja;

class FormController extends Controller
{
  public function __construct()
  {
    session_cache_expire(600);
  }

  public function formulario(Request $request, $loja)
  {
    if($loja != $request->session()->get('loja_formulario') || empty($request->session()->get('email_formulario')))
    {
      return redirect('formulario/entrar');
    }

    $view['loja'] = Loja::getLojaPeloCodigo($loja);

    $view['cadastro'] =   Loja::getCadastroPeloCodigoLoja($loja);

    return view('formulario',$view);
  }

  public function entrar(Request $request)
  {
    $request->session()->pull('email_formulario', []);
    $request->session()->pull('loja_formulario', []);

    $view['listaLojas'] = Loja::getListaLojas();

    return view('entrar',$view);
  }

  public function login(Request $request)
  {

    $response = 200;
    $data = $_POST;

    $verifica = Loja::verificaListaCadastroLogin($data);

    if(count($verifica) > 0)
    {
      $request->session()->put('email_formulario',$data['email']);
      $request->session()->put('loja_formulario',$data['loja']);

      Loja::insertAcesso($data['loja'], $data['email'], 'login','');

      $response = 200;
    }
    else
    {
      $response = 203;
    }

    return Response::json(true,$response);

  }

  public function atualizar(Request $request)
  {
    $campos = parse_str($_POST['form'], $form);
    $vazios = array();
    $query = '';

    $obrigatorios = $this->camposObrigatorios($form);

    foreach($form as $campo=>$valor)
    {

      if(trim($valor) == '' || trim($valor) == '0')
      {
        if(in_array($campo, $obrigatorios))
        $vazios[$campo]=$campo;
      }

      $query .= $campo."=NULLIF('".$valor."',''),";

    }

    $query = substr($query,0,-1);

    $cad = Loja::getCadastroPeloCodigoLoja($form['CodArea']);

    if(count($cad)>0)
    {
      Loja::atualizaDadosFormulario($form['CodArea'],$query);
    }
    else
    {
      Loja::insereDadosFormulario($query);
    }

    Loja::insertAcesso($form['CodArea'],   $request->session()->get('email_formulario'), 'atualizar', json_encode($form) );

  }


  public function finalizar(Request $request)
  {
    $campos = parse_str($_POST['form'], $form);
    $vazios = array();
    $query = '';

    $obrigatorios = $this->camposObrigatorios($form);

    foreach($form as $campo=>$valor)
    {

      if(trim($valor) == '')
      {
        if(in_array($campo, $obrigatorios))
        $vazios[$campo]=$campo;
      }

      $query .= $campo."=NULLIF('".$valor."',''),";

    }

    if(count($vazios) > 0)
    {
      return Response::json($vazios,'202');
    }

    $query = substr($query,0,-1);

    $cad = Loja::getCadastroPeloCodigoLoja($form['CodArea']);

    if(count($cad)>0)
    {
      Loja::atualizaDadosFormulario($form['CodArea'],$query);
    }
    else
    {
      Loja::insereDadosFormulario($query);
    }

    Loja::atualizaSatatus($form['CodArea'],$query);
    Loja::insertAcesso($form['CodArea'],   $request->session()->get('email_formulario'), 'atualizar', json_encode($form) );
  }



  function camposObrigatorios($form)
  {
    $obrigatorios = array('CodArea',
                          'IndProcIntel',
                          'EspacoDisco',
                          'MemRam',
                          'IndSO',
                          'IndMySql',
                          'TamBanda',
                          'TvwVersao',
                          'TvwId',
                          'TvwPwd',
                          'CNPJ',
                          'INSCESC',
                          'INSCMUN',
                          'RAZAO',
                          'FANTASIA',
                          'ENDERECO',
                          'NUMERO',
                          'BAIRRO',
                          'CIDADE',
                          'CEP',
                          'ESTADO',
                          'DDD',
                          'FONE1',
                          'FONE2',
                          'EMAIL',
                          'NOMERESP',
                          'CPFRESP',
                          'CONTATO',
                          'NOMECONTADOR',
                          'FONECONTADOR',
                          'EMAILCONTADOR',
                          'DTESTOQUE',
                          'CRTDIGINST',
                          'CRTDIGTIPO',
                          'DTHABNFE',
                          'REGTRIB',
                          'ALIQSN',
                          'INDPDVDOC',
                          'INDTEF',
                          'NFESERIE',
                          'NFENRO',
                          'TIPOEMINFDEV',
                          'TIPOEMINFEST',
                          'NFE_XMLEM_FMENV',
                          'NFE_XMLEM_SRVUSR',
                          'NFE_XMLEM_SRVPWD');


    if($form['IndMySql'] == 1)
    {
      array_push($obrigatorios,'PwdMySql');
    }

    if($form['INDPDVDOC'] == 1)
    {
      array_push($obrigatorios,'MARCAECF', 'MODELOECF', 'ALIQICMSECF');
    }

    if($form['INDPDVDOC'] == 2)
    {
      array_push($obrigatorios,'DTHABNFCE', 'NFCESERIE', 'NFCENRO', 'IDTOKEN', 'CSC', 'NOMEIMPNFISC');
    }

    if($form['INDPDVDOC'] == 3)
    {
      array_push($obrigatorios,'CODATIVACAOSAT','NOMEIMPNFISC');
    }

    if($form['INDPDVDOC'] == 4)
    {
      array_push($obrigatorios,'CODATIVACAOSAT', 'MFE_CHAVACESVLD', 'CNPJADQ', 'CHAVEREQ', 'CODESTADM', 'SERIALPOS','NOMEIMPNFISC');
    }

    if($form['ESTADO'] == 'SC')
    {
      array_push($obrigatorios,'NROAUTUSOECF');
    }

    if($form['NFE_XMLEM_FMENV'] == '1')
    {
      array_push($obrigatorios,'NFE_XMLEM_SRVEML', 'NFE_XMLEM_SRVPRT');
    }

    return $obrigatorios;
  }

  public function formularioView(Request $request, $loja)
  {

    $view['loja'] = Loja::getLojaPeloCodigo($loja);

    $view['cadastro'] =   Loja::getCadastroPeloCodigoLoja($loja);

    return view('formulario_view',$view);
  }


}
