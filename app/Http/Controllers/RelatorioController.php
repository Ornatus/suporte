<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chamado;
use App\Grupo;
use App\User;
use App\Relatorio;
use Session;
use Illuminate\Support\Facades\Response;
use Illuminate\Pagination\LengthAwarePaginator;

class RelatorioController extends Controller
{
  function __construct()
  {
    if(session_id()=='')
    {
      session_start();
    }
  }

  //Titulo

  function getCategoriasTitulo(Request $request)
  {
    $data = array();

    $data['usuario'] = $_SESSION['usuario'];
    $data['usuario_configuracao'] = $_SESSION['usuario_configuracao'];

    $data['categorias'] = Relatorio::getCategorias();

    return view('relatorio-titulo',$data);
  }

  function getFilterRelatorioTitulo(Request $request)
  {
    $data = array();
    $data = $_GET;

    $data['results'] = Relatorio::getFilterRelatorioTitulo($data);

    return view('lista-relatorio-titulo',$data);
  }


  //Area

  function getLojas(Request $request)
  {
    $data = array();

    $data['usuario'] = $_SESSION['usuario'];
    $data['usuario_configuracao'] = $_SESSION['usuario_configuracao'];

    $data['lojas'] = Relatorio::getLojas();

    return view('relatorio-area',$data);
  }

  function getFilterRelatorioArea(Request $request)
  {
    $data = array();
    $data = $_GET;

    $data['results'] = Relatorio::getFilterRelatorioArea($data);

    return view('lista-relatorio-area',$data);
  }


  //Usuarios

  function getUsuarios(Request $request)
  {
    $data = array();

    $data['usuario'] = $_SESSION['usuario'];
    $data['usuario_configuracao'] = $_SESSION['usuario_configuracao'];

    $data['usuarios'] = Relatorio::getUsuarios();
    $data['categorias'] = Relatorio::getCategorias();

    return view('relatorio-usuario',$data);
  }

  function getFilterRelatorioUsuarios(Request $request)
  {
    $data = array();
    $data = $_GET;

    $data['results'] = Relatorio::getFilterRelatorioUsuarios($data);

    return view('lista-relatorio-usuario',$data);
  }

  //Chamados

  function getChamados(Request $request)
  {
    $data = array();

    $data['usuario'] = $_SESSION['usuario'];
    $data['usuario_configuracao'] = $_SESSION['usuario_configuracao'];

    $data['data_min'] = isset($_GET['data_min']) ? $_GET['data_min'] : '';
    $data['data_max'] = isset($_GET['data_max']) ? $_GET['data_max'] : '';
    $data['filter_order_direction'] = isset($_GET['filter_order_direction']) ? $_GET['filter_order_direction'] : '';
    //print_R($data);die;
    $chamados = Chamado::getChamados($data);

    $data['chamados']  = $this->arrayPaginator($chamados, $request);

    return view('chamados/relatorio-chamado',$data);
  }

  function excelRelatorioChamado(Request $request)
  {
    $data = array();
    $data = $_GET;

    $data['chamados'] = Chamado::getChamados($data);

    return view('chamados/excel-relatorio-chamado',$data);
  }


  public function arrayPaginator($array, $request)
  {

    $currentPage = LengthAwarePaginator::resolveCurrentPage();
    $page = $currentPage;
    $perPage = 25;
    $offset = ($page * $perPage) - $perPage;

    return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,['path' => $request->url(),'query' => $request->query()]);
  }


  function getBackups(Request $request)
  {
    $data = array();

    $data['usuario'] = isset($_SESSION['usuario']) ? $_SESSION['usuario'] : array();
    $data['usuario_configuracao'] = isset($_SESSION['usuario_configuracao']) ? $_SESSION['usuario_configuracao'] : array();

    $data['data_min'] = isset($_GET['data_min']) ? $_GET['data_min'] : '';
    $data['data_max'] = isset($_GET['data_max']) ? $_GET['data_max'] : '';
    $data['filter_customer'] = isset($_GET['filter_customer']) ? $_GET['filter_customer'] : '';
    $data['first_access'] = isset($_GET['first_access']) ? $_GET['first_access'] : 0;

    $data['customers'] = Relatorio::getCustomers();

    $data['backups'] = array();

    if($data['first_access'] > 0)
    {
      $files = scandir(env('BACKUP_FILES'));

      if($data['filter_customer'] == 0)
      {

        foreach ($data['customers'] as $customer)
        {
          foreach($files as $file)
          {
            if(preg_match('@(Area'.$customer->brazil_store_id.')@is',$file))
            {
              $date_file = date("Y-m-d", filectime(env('BACKUP_FILES').$file));
              $size = $this->formatSizeUnits(filesize(env('BACKUP_FILES').$file));

              if($data['data_min'] != '')
              {
                if($date_file >= $data['data_min'] && $date_file <= $data['data_max'])
                {
                  $data['backups'][$file]['name'] = $file;
                  $data['backups'][$file]['date'] = $date_file;
                  $data['backups'][$file]['size'] = $size;
                }
              }
              else
              {
                $data['backups'][$file]['name'] = $file;
                $data['backups'][$file]['date'] = $date_file;
                $data['backups'][$file]['size'] = $size;
              }
            }
          }
        }


      }else{

        foreach($files as $file)
        {
          if(preg_match('@(Area'.$data['filter_customer'].')@is',$file))
          {
            $date_file = date("Y-m-d", filectime(env('BACKUP_FILES').$file));
            $size = $this->formatSizeUnits(filesize(env('BACKUP_FILES').$file));

            if($data['data_min'] != '')
            {
              if($date_file >= $data['data_min'] && $date_file <= $data['data_max'])
              {
                $data['backups'][$file]['name'] = $file;
                $data['backups'][$file]['date'] = $date_file;
                $data['backups'][$file]['size'] = $size;
              }
            }
            else
            {
              $data['backups'][$file]['name'] = $file;
              $data['backups'][$file]['date'] = $date_file;
              $data['backups'][$file]['size'] = $size;
            }
          }
        }
      }

    }

    return view('backups/relatorio-backup',$data);
  }


  function excelRelatorioBackups(Request $request)
  {
    $data = array();

    $data['data_min'] = isset($_GET['data_min']) ? $_GET['data_min'] : '';
    $data['data_max'] = isset($_GET['data_max']) ? $_GET['data_max'] : '';
    $data['filter_customer'] = isset($_GET['filter_customer']) ? $_GET['filter_customer'] : '';
    $data['first_access'] = isset($_GET['first_access']) ? $_GET['first_access'] : 0;

    $data['customers'] = Relatorio::getCustomers();

    $data['backups'] = array();

    if($data['first_access'] > 0)
    {
      $files = scandir(env('BACKUP_FILES'));

      if($data['filter_customer'] == 0)
      {

        foreach ($data['customers'] as $customer)
        {
          foreach($files as $file)
          {
            if(preg_match('@(Area'.$customer->brazil_store_id.')@is',$file))
            {
              $date_file = date("Y-m-d", filectime(env('BACKUP_FILES').$file));
              $size = $this->formatSizeUnits(filesize(env('BACKUP_FILES').$file));

              if($data['data_min'] != '')
              {
                if($date_file >= $data['data_min'] && $date_file <= $data['data_max'])
                {
                  $data['backups'][$file]['name'] = $file;
                  $data['backups'][$file]['date'] = $date_file;
                  $data['backups'][$file]['size'] = $size;
                }
              }
              else
              {
                $data['backups'][$file]['name'] = $file;
                $data['backups'][$file]['date'] = $date_file;
                $data['backups'][$file]['size'] = $size;
              }
            }
          }
        }


      }else{

        foreach($files as $file)
        {
          if(preg_match('@(Area'.$data['filter_customer'].')@is',$file))
          {
            $date_file = date("Y-m-d", filectime(env('BACKUP_FILES').$file));
            $size = $this->formatSizeUnits(filesize(env('BACKUP_FILES').$file));

            if($data['data_min'] != '')
            {
              if($date_file >= $data['data_min'] && $date_file <= $data['data_max'])
              {
                $data['backups'][$file]['name'] = $file;
                $data['backups'][$file]['date'] = $date_file;
                $data['backups'][$file]['size'] = $size;
              }
            }
            else
            {
              $data['backups'][$file]['name'] = $file;
              $data['backups'][$file]['date'] = $date_file;
              $data['backups'][$file]['size'] = $size;
            }
          }
        }
      }

    }

    return view('backups/excel-relatorio-backup',$data);
  }


  function formatSizeUnits($bytes)
  {
    if ($bytes >= 1)
    {
      $bytes = number_format($bytes / 1048576, 2) . ' MB';
    }
    else
    {
      $bytes = '0 MB';
    }

    return $bytes;
  }

}
