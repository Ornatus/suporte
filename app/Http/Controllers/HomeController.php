<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Loja;
use App\User;

class HomeController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {

    $view['lista'] = Loja::getListaCadastro();
    $view['user'] = User::getUser( $request->user()->id );

    return view('home',$view);
  }

  public function cadastro()
  {

    $view['listaLojas'] = Loja::getListaLojas();

    return view('cadastro',$view);
  }

  public function editar($id)
  {


    $view['data'] = Loja::getListaCadastroById($id);
    $view['listaLojas'] = Loja::getListaLojas();

    return view('editar',$view);
  }

  public function salvar(Request $request)
  {

    $response = 200;
    $data = $_POST;
    $data['user_id'] = $request->user()->id;

    $verifica = Loja::verificaListaCadastro($data);

    if(count($verifica) > 0)
    {
      $response = 203;
    }
    else
    {
      $inserir = Loja::insertListaCadastro($data);
    }

    return Response::json(true,$response);

  }

  public function atualizar(Request $request)
  {
    $response = 200;
    $data = $_POST;
    $data['user_id'] = $request->user()->id;

    $atualizar = Loja::updateListaCadastro($data);

    return Response::json(true,$response);

  }

}
