<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Chamado;
use App\Categoria;
use App\Grupo;
use App\User;
use Session;
use Illuminate\Support\Facades\Response;

class GrupoController extends Controller
{
  function __construct()
  {
    if(session_id()=='')
    {
      session_start();
    }
  }

  function atualizaGrupo(Request $request)
  {
    $data = $_POST;
    $data['usuario'] = $_SESSION['usuario']->user_id;

    $save = Grupo::atualizarGrupo($data);

    return Response::json($save,200);
  }

  function adicionaGrupo(Request $request)
  {
    $data['usuario'] = $_SESSION['usuario'];
    $data['usuario_configuracao'] = $_SESSION['usuario_configuracao'];

    return view('adiciona-grupo', $data);
  }

  function getCategoriaGrupo(Request $request)
  {
    $data = $_GET;

    $data['usuario'] = $_SESSION['usuario'];
    $data['usuario_configuracao'] = $_SESSION['usuario_configuracao'];
    $data['categorias'] = Grupo::getCategoriasByGrupoId($data);

    return view('lista-categoria-grupo', $data);
  }


  function setCategoria(Request $request)
  {
    $save = array();
    $resposta = 200;
    $data = $_POST;
    $data['usuario'] = $_SESSION['usuario']->user_id;

    $verifica = Grupo::getGrupocategoriaByCategoriaIdGrupoId($data);

    if(count($verifica) > 0){
      $resposta = 201;
    }else{
      $save = Grupo::setGrupocategoria($data);
    }

    return Response::json($save,$resposta);
  }

  function dropCategoriaGrupo(Request $request)
  {
    $save = array();
    $resposta = 200;
    $data = $_POST;
    $data['usuario'] = $_SESSION['usuario']->user_id;

    $save = Grupo::dropGrupocategoriaByGrupoCategoriaId($data);

    return Response::json($save,$resposta);
  }


  function cadastraGrupo(Request $request)
  {
    $data = $_POST;
    $data['usuario'] = $_SESSION['usuario']->user_id;

    $save = Grupo::cadastrarGrupo($data);

    return Response::json($save,200);
  }


  function grupos(Request $request)
  {
    $data = array();

    $data['filtro_grupo'] = isset($_SESSION['filtro_grupo']) ? $_SESSION['filtro_grupo'] : '';


    if(isset($_REQUEST['filtro_grupo']))
    {
      $data['filtro_grupo'] = $_REQUEST['filtro_grupo'];
    }

    $_SESSION['filtro_grupo'] = $data['filtro_grupo'];

    $data['usuario'] = $_SESSION['usuario'];
    $data['usuario_configuracao'] = $_SESSION['usuario_configuracao'];
    $grupos = Grupo::getGrupos($data);

    $data['grupos']  = $this->arrayPaginator($grupos, $request);

    return view('grupos',$data);
  }

  public function arrayPaginator($array, $request)
  {

    $currentPage = LengthAwarePaginator::resolveCurrentPage();
    $page = $currentPage;
    $perPage = 25;
    $offset = ($page * $perPage) - $perPage;

    return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,['path' => $request->url(),'query' => $request->query()]);
  }

  function grupo(Request $request, $grupo_id)
  {
    $data = array();
    $data['grupo_id'] = $grupo_id;
    $data['usuario'] = $_SESSION['usuario'];
    $data['usuario_configuracao'] = $_SESSION['usuario_configuracao'];

    $data['grupo'] = Grupo::getGrupoById($data);
    $data['categorias'] = Categoria::getCategorias($data);
    return view('grupo',$data);
  }

  function isLogged()
  {
    if( isset($_SESSION['usuario']) && $_SESSION['usuario']->user_id > 0  )
    {
      return true;
    }
    else
    {
      return false;
    };
  }



}
