<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\User;

class LoginController extends Controller
{
  function __construct()
  {
    if(session_id()=='')
    {
      session_start();
    }
  }

  function login()
  {

    if(session_id()=='')
    {
      session_start();
    }

    if(isset($_SESSION['usuario']))
    {
      return redirect('chamados');
    }

    $view = array();
    return view('login',$view);
  }

  function autenticar(Request $request)
  {
    $response = false;
    $response_code = 201;
    $data = $_POST;
    $autenticar = User::autenticar($data);

    if(count($autenticar) > 0)
    {
      $configuracao = User::configuracao($autenticar[0]);
      if(count($configuracao) > 0){
        $_SESSION['usuario'] = $autenticar[0];
        $_SESSION['usuario_configuracao'] = $configuracao[0];

        $response = true;
        $response_code = 200;
      }else{
        $response = false;
        $response_code = 202;
    
      }
    }

    return Response::json($response,$response_code);

  }

  function logout(Request $request)
  {
    session_destroy();

    return view('logout');


    // return redirect('logar');
  }

  function sessaoExpirada()
  {
    session_destroy();

    return view('logoutsession');


    // return redirect('logar');
  }
}
