<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Chamado;
use App\Categoria;
use App\User;
use Session;
use Illuminate\Support\Facades\Response;

class CategoriaController extends Controller
{
  function __construct()
  {
    if(session_id()=='')
    {
      session_start();
    }
  }

  function atualizaCategoria(Request $request)
  {
    $data = $_POST;
    $data['usuario'] = $_SESSION['usuario']->user_id;

    $save = Categoria::atualizarCategoria($data);

    return Response::json($save,200);
  }

  function adicionaCategoria(Request $request)
  {
    $data['usuario'] = $_SESSION['usuario'];
    $data['usuario_configuracao'] = $_SESSION['usuario_configuracao'];

    return view('adiciona-categoria', $data);
  }

  function cadastraCategoria(Request $request)
  {
    $data = $_POST;
    $data['usuario'] = $_SESSION['usuario']->user_id;

    $save = Categoria::cadastrarCategoria($data);

    return Response::json($save,200);
  }


  function categorias(Request $request)
  {
    $data = array();

    $data['filtro_categoria'] = isset($_SESSION['filtro_categoria']) ? $_SESSION['filtro_categoria'] : '';


    if(isset($_REQUEST['filtro_categoria']))
    {
      $data['filtro_categoria'] = $_REQUEST['filtro_categoria'];
    }

    $_SESSION['filtro_categoria'] = $data['filtro_categoria'];

    $data['usuario'] = $_SESSION['usuario'];
    $data['usuario_configuracao'] = $_SESSION['usuario_configuracao'];

    $categorias = Categoria::getCategorias($data);
    $data['categorias']  = $this->arrayPaginator($categorias, $request);

    return view('categorias',$data);
  }

  public function arrayPaginator($array, $request)
  {

    $currentPage = LengthAwarePaginator::resolveCurrentPage();
    $page = $currentPage;
    $perPage = 25;
    $offset = ($page * $perPage) - $perPage;

    return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,['path' => $request->url(),'query' => $request->query()]);
  }


  function categoria(Request $request, $categoria_id)
  {
    $data = array();
    $data['categoria_id'] = $categoria_id;
    $data['usuario'] = $_SESSION['usuario'];
    $data['usuario_configuracao'] = $_SESSION['usuario_configuracao'];

    $data['categoria'] = Categoria::getCategoriaById($data);
    
    return view('categoria',$data);
  }

  function isLogged()
  {
    if( isset($_SESSION['usuario']) && $_SESSION['usuario']->user_id > 0  )
    {
      return true;
    }
    else
    {
      return false;
    };
  }



}
