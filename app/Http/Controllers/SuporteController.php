<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Chamado;
use App\User;
use Session;


class SuporteController extends Controller
{
  function __construct()
  {
    if(session_id()=='')
    {
      session_start();
    }
  }

  function painel()
  {
    $view = array();

    $view['chamados'] =  Chamado::getChamadosPainel();
    $view['ultimo_chamado'] =  Chamado::getUltimoChamado();

    return view('painel',$view);
  }

  function atualizaPainel()
  {
    $view = array();

    $view['chamados'] =  Chamado::getChamadosPainel();
    $view['ultimo_chamado'] =  Chamado::getUltimoChamado();

    $view['html'] = view('cards',$view)->render();

    return $view;

  }

  function chamados(Request $request)
  {

    $data = array();

    $data['filtro_chamado'] = isset($_SESSION['filtro_chamado']) ? $_SESSION['filtro_chamado'] : 'grupo';
    $data['filtro_status'] = isset($_SESSION['filtro_status']) ? $_SESSION['filtro_status'] : '';
    $data['filtro_coluna'] = isset($_SESSION['filtro_coluna']) ? $_SESSION['filtro_coluna'] : 'data_chamado';
    $data['filtro_ordem'] = isset($_SESSION['filtro_ordem']) ? $_SESSION['filtro_ordem'] : 'desc';
    $data['filtro_livre'] = isset($_SESSION['filtro_livre']) ? $_SESSION['filtro_livre'] : '';

    if(isset($_REQUEST['filtro_chamado']))
    {
      $data['filtro_chamado'] = $_REQUEST['filtro_chamado'];
    }

    if(isset($_REQUEST['filtro_status']))
    {
      $data['filtro_status'] = $_REQUEST['filtro_status'];
    }

    if(isset($_REQUEST['filtro_coluna']))
    {
      $data['filtro_coluna'] = $_REQUEST['filtro_coluna'];
    }

    if(isset($_REQUEST['filtro_ordem']))
    {
      $data['filtro_ordem'] = $_REQUEST['filtro_ordem'];
    }

    if(isset($_REQUEST['filtro_livre']))
    {
      $data['filtro_livre'] = $_REQUEST['filtro_livre'];
    }

    $_SESSION['filtro_status'] = $data['filtro_status'];
    $_SESSION['filtro_chamado'] = $data['filtro_chamado'];
    $_SESSION['filtro_coluna'] = $data['filtro_coluna'];
    $_SESSION['filtro_ordem'] = $data['filtro_ordem'];
    $_SESSION['filtro_livre'] = $data['filtro_livre'];

    $data['usuario'] = $_SESSION['usuario'];
    $data['usuario_configuracao'] = $_SESSION['usuario_configuracao'];
    $data['status'] = Chamado::getChamadoStatus();

    $chamados = Chamado::getChamadosUsuarioGrupo($data);
    $data['chamados']  = $this->arrayPaginator($chamados, $request);

    return view('home',$data);
  }

  public function arrayPaginator($array, $request)
  {

    $currentPage = LengthAwarePaginator::resolveCurrentPage();
    $page = $currentPage;
    $perPage = 25;
    $offset = ($page * $perPage) - $perPage;

    return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,['path' => $request->url(),'query' => $request->query()]);
  }

  function chamado(Request $request, $chamado_id)
  {

    $data = array();
    $data['chamado_id'] = $chamado_id;
    $data['usuario'] = $_SESSION['usuario'];
    $data['usuario_configuracao'] = $_SESSION['usuario_configuracao'];
    $data['prioridades'] = Chamado::getChamadoPrioridade();
    $data['tipos'] = Chamado::getChamadoTipo();
    $data['acoes'] = Chamado::getChamadoAcao();
    $data['status'] = Chamado::getChamadoStatus();
    $data['categorias'] = Chamado::getChamadoCategoria();
    $data['chamado'] = Chamado::getChamadoById($data);

    $data['titulos'] = Chamado::getTitulosbyCategoriaid($data['chamado'][0]->categoria_id);

    return view('chamado',$data);
  }

  function enviaMensagem(Request $request)
  {
    $data = $_POST;
    $data['usuario'] = $_SESSION['usuario']->user_id;
    $data['tipo_usuario'] = 2;

    $categoria_id = Chamado::getCategoryIdByIdChamado($data['chamado_id']);

    $data['categoria_id']  = $categoria_id[0]->categoria_id;

    $this->mail($data['chamado_id'],$data,2);

    $save = Chamado::salvaMensagem($data);

    return Response::json($save,200);
  }

  function chamadoMensagem(Request $request)
  {
    $data['chamado_id'] = $_POST['chamado_id'];
    $data['mensagens'] = Chamado::getMensagensByChamadoId($data);

    $data['html'] = view('mensagens',$data)->render();

    return Response::json($data,200);
  }


  function atenderChamado(Request $request)
  {
    $data['chamado_id'] = $_POST['chamado_id'];
    $data['tipo_id'] = $_POST['tipo_id'];
    $data['titulo_id'] = $_POST['titulo_id'];
    $data['prioridade_id'] = $_POST['prioridade_id'];
    $data['categoria_id'] = $_POST['categoria_id'];
    $data['usuario'] = $_SESSION['usuario']->user_id;

    $data['chamado'] = Chamado::getChamadoById($data);

    if($data['chamado'][0]->atendente_id > 0)
    {
      $data['response'] = false;
    }else{
      $data['response'] = Chamado::atribuirChamado($data);
      $data['chamado'] = Chamado::getChamadoById($data);
      $this->mail($data['chamado_id'],$data,1);
    }

    return Response::json($data,200);
  }


  function isLogged()
  {
    if( isset($_SESSION['usuario']) && $_SESSION['usuario']->user_id > 0  )
    {
      return true;
    }
    else
    {
      return false;
    };
  }

  function finalizaChamado(Request $request)
  {
    $data = $_POST;

    $data['usuario'] = $_SESSION['usuario']->user_id;
    $data['tipo_usuario'] = 2;

    $save = Chamado::finalizaChamado($data);
    $categoria_id = Chamado::getCategoryIdByIdChamado($data['chamado_id']);

    $data['categoria_id']  = $categoria_id[0]->categoria_id;

    $this->mail($data['chamado_id'],$data,3);

    return Response::json($save,200);
  }

  function retornaBolsao(Request $request)
  {
    $data = $_POST;
    $data['usuario'] = $_SESSION['usuario']->user_id;
    $data['tipo_usuario'] = 2;

    $save = Chamado::retornaBolsao($data);

    return Response::json($save,200);
  }

  function atualizaChamado(Request $request)
  {
    $data = $_POST;
    $data['usuario'] = $_SESSION['usuario']->user_id;
    $data['tipo_usuario'] = 2;

    $save = Chamado::atualizarChamado($data);

    return Response::json($save,200);
  }

  public function mail($chamado_id,$data,$status)
  {

    $emails = array();

    if($status == 4){
      $users = Chamado::getEmailSuporte();
    }else{
      $users = Chamado::getEmailClientbyId($data['chamado_id']);
    }

    $data['atendente_nome'] = $_SESSION['usuario']->firstname;

    foreach ($users as $user) {
      $emails[] = $user->email;
    }

    $data['emails'] = $emails;

    if($status == 1){
      $viewcerta = 'mail/suporte_status_chamado';
    }elseif($status == 2){
      $viewcerta = 'mail/suporte_mensagem_chamado';
    }elseif($status == 3){
      $viewcerta = 'mail/suporte_finaliza_chamado';
    }elseif($status == 4){
      $viewcerta = 'mail/suporte_analise_chamado';
    }elseif($status == 5){
      $viewcerta = 'mail/suporte_resposta_analise_chamado';
    }else{
      print_r('Erro no envio de email');
    };

    Mail::send($viewcerta, $data, function($message) use ($data) {
      $message->to($data['emails'], '')
      ->subject( '[SUPORTE ORNATUS] Chamado: '.$data['chamado_id'] );
      $message->from('dev@grupoornatus.com','[SUPORTE ORNATUS]');
    });

  }

  function chamadoArquivo(Request $request)
  {
    $data['chamado_id'] = $_POST['chamado_id'];
    $data['arquivos'] = Chamado::getArquivoByChamadoId($data['chamado_id']);
    $data['html'] = view('arquivos',$data)->render();

    return Response::json($data,200);
  }

  function openFile(Request $request, $arquivo_id){

    $data = Chamado::getFileById($arquivo_id);

    $content = base64_decode($data[0]->base64);

    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename= '.$data[0]->arquivo_id.'.'.$data[0]->extension);
    echo $content;

  }

  function uploadFile(Request $request){

    $max_size =  '5242880';

    if($_FILES['image']['size'] > $max_size){
      return Response::json('false');
    }

    $data['file'] = $_FILES['image'];

    $data['file']['extension'] = pathinfo($_FILES['image']['name'])['extension'] ;

    $file = file_get_contents($_FILES['image']['tmp_name']);

    $data['file']['base64'] =  base64_encode($file);
    $data['file']['arquivo_id'] = Chamado::saveFile($data);

    return Response::json($data,200);
  }

  function solicitaAnalise(Request $request)
  {
    $chamado_id = $_POST['chamado_id'];

    Chamado::solicitaAnalise($chamado_id);

    $save = array();
    $save['mensagem'] = "Solicitação realizada com sucesso.";

    $data['chamado_id'] =$_POST['chamado_id'];
    $this->mail($chamado_id,$data,4);

    return Response::json($save,200);
  }

  function negarAnalise(Request $request)
  {
    $data = $_POST;

    $data['usuario'] = $_SESSION['usuario']->user_id;
    $data['tipo_usuario'] = 2;

    $categoria_id = Chamado::getCategoryIdByIdChamado($data['chamado_id']);

    $data['categoria_id']  = $categoria_id[0]->categoria_id;

    $date['status_update'] = Chamado::negarAnalise($data['chamado_id']);

    $save = Chamado::salvaMensagem($data);

    $this->mail($data['chamado_id'],$data,5);


    return Response::json($save,200);
  }

}
