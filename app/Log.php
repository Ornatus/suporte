<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Log extends Model
{
  protected $fillable = ['date_added','user_id','remote_addr','http_referer','http_host','request_uri','query_string','route','request_method','request_value'];
  protected $table = 'access_log_suporte';
  public $timestamps = false;


  static function setLog()
  {
    
    $json_request = json_encode($_SERVER);

    $user = isset($_SESSION['usuario']->user_id)?$_SESSION['usuario']->user_id : null;
    $remote_addr = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR']: '';
    $http_referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER']: '';
    $http_host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST']: '';
    $request_uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI']: '';
    $query_string = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING']: '';
    $route = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI']: '';
    $request_method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD']: '';

    $sql = "INSERT INTO suporte.access_log_suporte
    SET
    user_id = '".$user."',
    remote_addr = '".$remote_addr."',
    http_referer = '".$http_referer."',
    http_host = '".$http_host."',
    request_uri = '".$request_uri."',
    query_string = '".$query_string."',
    route = '".$route."',
    request_method = '".$request_method."',
    request_value = '".$json_request."' ;";

    return DB::insert($sql);
  }

}
