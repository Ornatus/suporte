<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Titulo extends Model
{
  protected $fillable = ['titulo_id','descricao','cor','ativo','categoria_id'];
  protected $table = 'titulo';
  public $timestamps = false;


  static function cadastrarTitulo($data)
  {

    $sql = "INSERT INTO suporte.titulo
    SET
    descricao = '".$data['descricao']."',
    cor = '".$data['cor']."',
    categoria_id = '".$data['categoria']."',
    link = '".$data['link']."',
    ativo = '1' ;";

    return DB::insert($sql);
  }

  static function getTitulos($data)
  {
    $sql = "SELECT
    t.descricao,
    t.cor,
    t.titulo_id,
    t.categoria_id,
    t.link,
    c.descricao as categoria
    FROM suporte.titulo as t
    JOIN suporte.categoria as c
    ON c.categoria_id = t.categoria_id
    WHERE t.ativo = 1 ";

    if(isset($data['filtro_titulo'])){
      $sql.=" AND t.descricao like '%".$data['filtro_titulo']."%'";

    }

    $sql.="ORDER BY categoria_id,titulo_id ASC";


    return (array)DB::select($sql);
  }

  static function getTituloById($data)
  {
    $sql = "SELECT
    descricao,
    cor,
    titulo_id,
    link,
    categoria_id
    FROM suporte.titulo
    WHERE ativo = 1
    AND titulo_id = '".$data['titulo_id']."'
    ORDER BY titulo_id DESC";

    return (array)DB::select($sql);
  }

  static function atualizarTitulo($data)
  {
    $sql = "UPDATE suporte.titulo
    SET
    cor = '".$data['cor']."',
    descricao = '".$data['descricao']."',
    categoria_id = '".$data['categoria']."',
    link = '".$data['link']."'
    WHERE
    titulo_id = '".$data['titulo_id']."';";

    return DB::update($sql);
  }


  static function getTitulosUsuarioGrupo($data)
  {
    $sql = "
    SELECT * FROM (
      SELECT
      s.descricao as status,
      s.cor as status_cor,
      c.nome as brazil_store_name,
      c.codigo as brazil_store_id,
      ct.descricao as titulo,
      ct.cor as cor_titulo,
      ch.ativo,
      ch.descricao,
      ch.data_chamado,
      ch.resolucao,
      ch.status_id,
      ch.titulo_id,
      ch.categoria_id,
      ch.chamado_id,
      ch.atendente_id,
      CASE WHEN ch.data_resolvido is not null
      THEN TIMEDIFF(ch.data_resolvido, ch.data_chamado)
      ELSE TIMEDIFF(current_timestamp, ch.data_chamado)
      END AS tempo_aberto,
      su.nome as atendente_nome
      FROM suporte.chamado ch
      JOIN suporte.vw_usuarios c on c.usuario_id = ch.usuario_id and c.tipo_usuario = ch.tipo_usuario
      JOIN suporte.status s on s.status_id = ch.status_id
      JOIN suporte.titulo ct on ct.titulo_id = ch.titulo_id
      LEFT JOIN suporte.vw_usuarios su ON su.usuario_id = ch.atendente_id AND su.tipo_usuario = 2
      ) as chamados
      WHERE ativo = 1 ";


      if(isset($data['filtro_status']) )
      {
        if( $data['filtro_status'] == 'AA')
        {
          $sql .="AND status_id in (1,2) ";
        }

        if( (int)$data['filtro_status'] > 0 )
        {
          $sql .="AND status_id = '".$data['filtro_status']."' ";
        }

      }

      if(isset($data['filtro_livre']) && strlen($data['filtro_livre'])>0)
      {
        $sql .=" AND ( descricao like '%".$data['filtro_livre']."%' ";
          $sql .=" OR atendente_nome like '%".$data['filtro_livre']."%' ";
          $sql .=" OR titulo like '%".$data['filtro_livre']."%' ";
          $sql .=" OR brazil_store_name like '%".$data['filtro_livre']."%' ) ";
        }

        $sql .="AND titulo_id in (SELECT titulo_id FROM grupo_titulo WHERE grupo_id = '".$data['usuario_configuracao']->grupo_id."') ";

        if($data['filtro_chamado'] == 'grupo')
        {
          //  $sql .= "AND  atendente_id is null ";
        }
        else
        {
          $sql .= "AND  atendente_id = '".$data['usuario']->user_id."' ";
        }

        $sql .="ORDER BY ".$data['filtro_coluna']." ".$data['filtro_ordem'].";";

        return (array)DB::select($sql);
      }

      static function getTituloCategoria()
      {
        $sql = "SELECT c.categoria_id, c.descricao FROM categoria c
        JOIN grupo_categoria gc ON gc.categoria_id = c.categoria_id
        WHERE c.ativo = 1
        AND gc.ativo = 1 AND gc.grupo_id = '".$_SESSION['usuario_configuracao']->grupo_id."'
        ORDER BY categoria_id;";

        return (array)DB::select($sql);
      }

      static function consultaTitulobyId($titulo_id)
      {
        $sql = "SELECT ch.chamado_id
        from suporte.chamado as ch
        JOIN suporte.titulo as t ON t.titulo_id = ch.titulo_id
        where t.titulo_id = '".$titulo_id."'; ";

        return (array)DB::select($sql);
      }

      static function inativaTitulobyId($titulo_id)
      {
        $sql = "UPDATE suporte.titulo
        SET
        ativo = 0
        WHERE
        titulo_id = '".$titulo_id."';";

        return DB::update($sql);
      }

    }
