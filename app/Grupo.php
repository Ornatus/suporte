<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Grupo extends Model
{
  protected $fillable = ['grupo_id','descricao','cor','ativo'];
  protected $table = 'grupo';
  public $timestamps = false;



  static function setGrupocategoria($data)
  {
    $sql = "INSERT INTO suporte.grupo_categoria
    SET
    grupo_id = '".$data['grupo_id']."',
    categoria_id = '".$data['categoria']."',
    ativo = '1' ;";

    return DB::insert($sql);
  }

  static function getGrupocategoriaByCategoriaIdGrupoId($data)
  {
    $sql = "SELECT * FROM suporte.grupo_categoria
    WHERE
    grupo_id = '".$data['grupo_id']."' AND
    categoria_id = '".$data['categoria']."' AND
    ativo = '1' ;";

    return DB::select($sql);
  }

  static function dropGrupocategoriaByGrupoCategoriaId($data)
  {
    $sql = "UPDATE suporte.grupo_categoria
    SET ativo = 0 where
    grupo_categoria_id = '".$data['grupo_categoria_id']."' AND
    ativo = '1' ;";

    return DB::update($sql);
  }

  static function getCategoriasByGrupoId($data)
  {
    $sql = "SELECT gc.grupo_categoria_id,c.categoria_id,c.descricao,c.departamento
    FROM suporte.grupo_categoria as gc
    JOIN categoria c on c.categoria_id = gc.categoria_id
    WHERE
    gc.grupo_id = '".$data['grupo_id']."' AND
    gc.ativo = '1' ;";

    return DB::select($sql);
  }

  static function cadastrarGrupo($data)
  {
    $sql = "INSERT INTO suporte.grupo
    SET
    descricao = '".$data['descricao']."',
    cor = '".$data['cor']."',
    ativo = '1' ;";

    return DB::insert($sql);
  }

  static function getGrupos($data)
  {
    $sql = "SELECT
    descricao,
    cor,
    grupo_id
    FROM suporte.grupo
    WHERE ativo = 1 ";

    if(isset($data['filtro_grupo'])){
      $sql.=" AND descricao like '%".$data['filtro_grupo']."%'";
    }

    $sql.="ORDER BY grupo_id ASC";

    return (array)DB::select($sql);
  }

  static function getGrupoById($data)
  {
    $sql = "SELECT
    descricao,
    cor,
    grupo_id
    FROM suporte.grupo
    WHERE ativo = 1
    AND grupo_id = '".$data['grupo_id']."'
    ORDER BY grupo_id DESC";

    return (array)DB::select($sql);
  }

  static function atualizarGrupo($data)
  {
    $sql = "UPDATE suporte.grupo
    SET
    cor = '".$data['cor']."',
    descricao = '".$data['descricao']."'
    WHERE
    grupo_id = '".$data['grupo_id']."';";

    return DB::update($sql);
  }


}
